#include "my_utility_tm.h"

#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

class TraversabilityMapping{

private:

    // ROS Node Handle
    ros::NodeHandle nh, ng;
    // Mutex Memory Lock
    std::mutex mtx, laserCloudElevationMtx;
    // Subscriber
    ros::Subscriber subFilteredGroundCloud1, subFilteredGroundCloud2;
    ros::Publisher pubElevationCloud, pubMaximumCloud;
    // Point Cloud Pointer
    pcl::PointCloud<PointType>::Ptr laserCloud; // save input filtered laser cloud for mapping
    pcl::PointCloud<PointType>::Ptr laserCloudElevation; // a cloud for publishing elevation map

    double radius, voxel, x_pos_cur[5];
    int UDPNUM, sor_kmean;
    double sor_threshold;
    vector<string> UDPs;

    int pubCount;
    
    // Map Arrays
    int mapArrayCount;
    int **mapArrayInd; // it saves the index of this submap in vector mapArray
    int **predictionArrayFlag;
    vector<childMap_t*> mapArray;

    // Local Map Extraction
    PointType robotPoint;

    // Global Variables for Traversability Calculation
    cv::Mat matCov, matEig, matVec;

    // Lists for New Scan
    vector<mapCell_t*> observingList1; // thread 1: save new observed cells
    vector<mapCell_t*> observingList2; // thread 2: calculate traversability of new observed cells

    pcl::PointCloud<PointType>::Ptr LocalMaximumPoints;
    double time_laser1, time_send_udp1, time_laser2, time_send_udp2;

    pcl::StatisticalOutlierRemoval<PointType> sor;

    vector<BoundingBox> obstacles;
    int obstacles_size;
    

public:
    TraversabilityMapping():
        nh("~"),
        pubCount(1),
        mapArrayCount(0){
        ng.getParam("radius", radius);
        ng.getParam("voxel", voxel);
        ng.getParam("sor_kmean", sor_kmean);
        ng.getParam("sor_threshold", sor_threshold);
        ng.getParam("UDPNUM", UDPNUM);
        for(int i = 1; i <= UDPNUM; i++)
        {
            stringstream ss;
            ss << "UDP" << i;
            string tmp;
            ng.getParam(ss.str(), tmp);
            UDPs.push_back(tmp);
        }
        for(int i = 0; i < 5; i ++)
            x_pos_cur[i] = -1.0;
        time_laser1 = time_send_udp1 = time_laser2 = time_send_udp2 = 0.0;

        obstacles.push_back(BoundingBox(275.55, 0, 0, 278.05, 32.0, 10.5));    // 6#库左侧墙
        obstacles.push_back(BoundingBox(200.0, 29.45, 0, 350.0, 32.0, 10.5));  // 下侧墙
        obstacles.push_back(BoundingBox(247.2, 0.0, 0.0, 248.7, 32.0, 10.5));  // 4-5#库隔断墙
        obstacles.push_back(BoundingBox(259.3, 0.0, 0.0, 260.8, 32.0, 10.5));  // 5-6#库隔断墙
        //obstacles.push_back(BoundingBox(253.6, 0.0, 0.0, 272.6, 8.7, 8.4));   // 5-6料仓台
        obstacles.push_back(BoundingBox(270.5, 0.0, 0.0, 272.6, 8.7, 10.5));   // 5-6库料仓台：竖直1
        obstacles.push_back(BoundingBox(264.6, 0.0, 0.0, 267.3, 8.7, 10.5));   // 5-6库料仓台：竖直2
        obstacles.push_back(BoundingBox(258.5, 0.0, 0.0, 261.4, 8.7, 10.5));   // 5-6库料仓台：竖直3
        obstacles.push_back(BoundingBox(253.4, 0.0, 0.0, 255.3, 8.7, 10.5));   // 5-6库料仓台：竖直4
        obstacles.push_back(BoundingBox(253.4, 0.0, 0.0, 272.6, 1.5, 10.5));   // 5-6库料仓台：水平5
        obstacles.push_back(BoundingBox(253.4, 6.3, 0.0, 272.6, 8.7, 10.5));   // 5-6库料仓台：水平6
        obstacles.push_back(BoundingBox(289.1, 0.0, 0.0, 294.1, 4.25, 10.5));   // 7#仓（平板库）左上角
        obstacles.push_back(BoundingBox(293.8, 0.0, 0.0, 350.0, 32.0, 10.5));   // 7#仓（平板库）左侧
        obstacles.push_back(BoundingBox(233.2, 23.8, 0.0, 242.1, 32.0, 10.5));   // 4仓左下角
        obstacles.push_back(BoundingBox(253.6, 26.0, 0.0, 265.8, 32.0, 10.5));   // 5-6仓下侧平台
        //obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 224.6, 8.8, 10.5));   // 4仓料仓台
        obstacles.push_back(BoundingBox(181.6, 0.0, 0.0, 182.8, 32.0, 10.5));  // 3-4#库隔断墙
        obstacles.push_back(BoundingBox(222.6, 0.0, 0.0, 224.8, 8.9, 10.5));   // 4#库料仓台：竖直1
        obstacles.push_back(BoundingBox(216.7, 0.0, 0.0, 218.8, 8.9, 10.5));   // 4#库料仓台：竖直2
        obstacles.push_back(BoundingBox(210.6, 0.0, 0.0, 213.2, 8.9, 10.5));   // 4#库料仓台：竖直3
        obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 207.0, 8.9, 10.5));   // 4#库料仓台：竖直4
        obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 224.8, 1.75, 10.5));   // 4#库料仓台：水平1
        obstacles.push_back(BoundingBox(205.5, 6.4, 0.0, 224.8, 8.9, 10.5));   // 4#库料仓台：水平2
        obstacles.push_back(BoundingBox(144.6, 0.0, 0.0, 146.5, 9.0, 10.5));   // 3#库料仓台：竖直1
        obstacles.push_back(BoundingBox(138.6, 0.0, 0.0, 141.2, 9.0, 10.5));   // 3#库料仓台：竖直2
        obstacles.push_back(BoundingBox(132.6, 0.0, 0.0, 135.2, 9.0, 10.5));   // 3#库料仓台：竖直3
        obstacles.push_back(BoundingBox(127.0, 0.0, 0.0, 129.2, 9.0, 10.5));   // 3#库料仓台：竖直4
        obstacles.push_back(BoundingBox(127.0, 0.0, 0.0, 146.5, 0.86, 10.5));   // 3#库料仓台：水平1
        obstacles.push_back(BoundingBox(127.0, 6.12, 0.0, 146.5, 9.0, 10.5));   // 3#库料仓台：水平2
        obstacles.push_back(BoundingBox(109.5, 0.0, 0.0, 110.5, 32.0, 10.5));   //2-3#库隔断墙
        obstacles.push_back(BoundingBox(45.8, 0.0, 0.0, 68.8, 10.5, 10.5));   //2#库料仓台
        obstacles.push_back(BoundingBox(55.8, 10.5, 0.0, 56.8, 32.0, 10.5));   //1-2#库隔断墙
        obstacles.push_back(BoundingBox(56.7, 26.6, 0.0, 109.7, 32.0, 10.5));   //2#库底部，因为2#库y最大值比其它库小要单独切割
        
        obstacles_size = obstacles.size();

        LocalMaximumPoints.reset(new pcl::PointCloud<PointType>());
        // subscribe to traversability filter
        subFilteredGroundCloud1 = nh.subscribe<sensor_msgs::PointCloud2>("/seg_cloud1", 5, &TraversabilityMapping::cloudHandler1, this);
        subFilteredGroundCloud2 = nh.subscribe<sensor_msgs::PointCloud2>("/seg_cloud2", 5, &TraversabilityMapping::cloudHandler2, this);
        // publish elevation map for visualization
        pubElevationCloud = nh.advertise<sensor_msgs::PointCloud2> ("/elevation_pointcloud", 1);
        pubMaximumCloud = nh.advertise<sensor_msgs::PointCloud2> ("/maximum_cloud", 1);

        allocateMemory(); 
    }

    ~TraversabilityMapping(){}

    

    void allocateMemory(){
        // allocate memory for point cloud
        laserCloud.reset(new pcl::PointCloud<PointType>());
        laserCloudElevation.reset(new pcl::PointCloud<PointType>());
        
        // initialize array for cmap
        mapArrayInd = new int*[mapArrayLength];
        for (int i = 0; i < mapArrayLength; ++i)
            mapArrayInd[i] = new int[mapArrayLength];

        for (int i = 0; i < mapArrayLength; ++i)
            for (int j = 0; j < mapArrayLength; ++j)
                mapArrayInd[i][j] = -1;

        // initialize array for predicting elevation sub-maps
        predictionArrayFlag = new int*[mapArrayLength];
        for (int i = 0; i < mapArrayLength; ++i)
            predictionArrayFlag[i] = new int[mapArrayLength];

        for (int i = 0; i < mapArrayLength; ++i)
            for (int j = 0; j < mapArrayLength; ++j)
                predictionArrayFlag[i][j] = false;

        // Matrix Initialization
        matCov = cv::Mat (3, 3, CV_32F, cv::Scalar::all(0));
        matEig = cv::Mat (1, 3, CV_32F, cv::Scalar::all(0));
        matVec = cv::Mat (3, 3, CV_32F, cv::Scalar::all(0));
    }

    unsigned char getCheckCode(unsigned char *sendBuf, int dataLen)
    {
        unsigned char checkCode = 0;
        for(int i = 0; i < dataLen; i++)
            checkCode += sendBuf[i];
        return checkCode;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////// Register Cloud /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void cloudHandler1(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        // Lock thread
        //std::lock_guard<std::mutex> lock(mtx);
        pcl::fromROSMsg(*laserCloudMsg, *laserCloud);
        PointType pt1, pt2;
        pcl::getMinMax3D(*laserCloud, pt1, pt2);
        mtx.lock();
        x_pos_cur[0] = (pt1.x + pt2.x) / 2.0;
        mtx.unlock();
        robotPoint.x = x_pos_cur[0];
        robotPoint.y = 14.0;
        robotPoint.z = 17.0;
        time_laser1 = ros::Time::now().toSec();
        updateElevationMap();
        //publishMap();
        publishTraversabilityMap();
    }

    void cloudHandler2(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        // Lock thread
        //std::lock_guard<std::mutex> lock(mtx);
        pcl::fromROSMsg(*laserCloudMsg, *laserCloud);
        PointType pt1, pt2;
        pcl::getMinMax3D(*laserCloud, pt1, pt2);
        mtx.lock();
        x_pos_cur[1] = (pt1.x + pt2.x) / 2.0;
        mtx.unlock();
        robotPoint.x = x_pos_cur[1];
        robotPoint.y = 14.0;
        robotPoint.z = 17.0;
        time_laser2 = ros::Time::now().toSec();
        updateElevationMap();
        //publishMap();
        publishTraversabilityMap();
    }

    void updateElevationMap(){
        int cloudSize = laserCloud->points.size();
        for (int i = 0; i < cloudSize; ++i){
            PointType p = laserCloud->points[i];
            p.intensity = 100;
            updateElevationMap(&p);
        }
    }

    void updateElevationMap(PointType *point){
        // Find point index in global map
        grid_t thisGrid;
        if (findPointGridInMap(&thisGrid, point) == false) return;
        // Get current cell pointer
        mapCell_t *thisCell = grid2Cell(&thisGrid);
        // update elevation
        updateCellElevation(thisCell, point);
        // update occupancy
        updateCellOccupancy(thisCell, point);
        // update observation time
        updateCellObservationTime(thisCell);
    }

    void updateCellObservationTime(mapCell_t *thisCell){
        ++thisCell->observeTimes;
        if (thisCell->observeTimes >= traversabilityObserveTimeTh)
            observingList1.push_back(thisCell);
    }

    void updateCellOccupancy(mapCell_t *thisCell, PointType *point){
        // Update log_odds
        float p;  // Probability of being occupied knowing current measurement.
        if (point->intensity == 100)
            p = p_occupied_when_laser;
        else
            p = p_occupied_when_no_laser;
        thisCell->log_odds += std::log(p / (1 - p));

        if (thisCell->log_odds < -large_log_odds)
            thisCell->log_odds = -large_log_odds;
        else if (thisCell->log_odds > large_log_odds)
            thisCell->log_odds = large_log_odds;
        // Update occupancy
        float occupancy;
        if (thisCell->log_odds < -max_log_odds_for_belief)
            occupancy = 0;
        else if (thisCell->log_odds > max_log_odds_for_belief)
            occupancy = 100;
        else
            occupancy = (int)(lround((1 - 1 / (1 + std::exp(thisCell->log_odds))) * 100));
        // update cell
        thisCell->updateOccupancy(occupancy);
    }

    void updateCellElevation(mapCell_t *thisCell, PointType *point){
        // Kalman Filter: update cell elevation using Kalman filter
        // https://www.cs.cornell.edu/courses/cs4758/2012sp/materials/MI63slides.pdf

        // cell is observed for the first time, no need to use Kalman filter
        double time = ros::Time::now().toSec();

        if (thisCell->elevation == -FLT_MAX){
            thisCell->elevation = point->z;
            thisCell->elevationVar = 10;
            thisCell->observeTimeLast = time;
            return;
        }

        if(point->z > thisCell->elevation + 1)
        {
            if(time - thisCell->observeTimeLast < 5)
            {
                thisCell->observeTimeLast = time;
                return;
            }
            // else
            // {
            //     thisCell->updateElevation(point->z, 10);
            //     thisCell->observeTimeLast = time;
            //     return;
            // }
        }

        if(point->z < thisCell->elevation - 1)
        {
            
            thisCell->updateElevation(point->z, 10);
            thisCell->observeTimeLast = time;
            return;
        }


        // Predict:
        float x_pred = thisCell->elevation; // x = F * x + B * u
        float P_pred = thisCell->elevationVar + 0.01; // P = F*P*F + Q
        // Update:
        float R_factor = 1; //(thisCell->observeTimes > 20) ? 10 : 1;
        float R = 10 * R_factor; // measurement noise: R, scale it with dist and observed times
        float K = P_pred / (P_pred + R);// Gain: K  = P * H^T * (HPH + R)^-1
        float y = point->z; // measurement: y
        float x_final = x_pred + K * (y - x_pred); // x_final = x_pred + K * (y - H * x_pred)
        float P_final = (1 - K) * P_pred; // P_final = (I - K * H) * P_pred
        // Update cell
        thisCell->observeTimeLast = time;
        thisCell->updateElevation(x_final, P_final);
    }

    mapCell_t* grid2Cell(grid_t *thisGrid){
        return mapArray[mapArrayInd[thisGrid->cubeX][thisGrid->cubeY]]->cellArray[thisGrid->gridX][thisGrid->gridY];
    }

    bool findPointGridInMap(grid_t *gridOut, PointType *point){
        // Calculate the cube index that this point belongs to. (Array dimension: mapArrayLength * mapArrayLength)
        grid_t thisGrid;
        getPointCubeIndex(&thisGrid.cubeX, &thisGrid.cubeY, point);
        // Decide whether a point is out of pre-allocated map
        if (thisGrid.cubeX >= 0 && thisGrid.cubeX < mapArrayLength && 
            thisGrid.cubeY >= 0 && thisGrid.cubeY < mapArrayLength){
            // Point is in the boundary, but this sub-map is not allocated before
            // Allocate new memory for this sub-map and save it to mapArray
            if (mapArrayInd[thisGrid.cubeX][thisGrid.cubeY] == -1){
                childMap_t *thisChildMap = new childMap_t(mapArrayCount, thisGrid.cubeX, thisGrid.cubeY);
                mapArray.push_back(thisChildMap);
                mapArrayInd[thisGrid.cubeX][thisGrid.cubeY] = mapArrayCount;
                ++mapArrayCount;
            }
        }else{
            // Point is out of pre-allocated boundary, report error (you should increase map size)
            ROS_ERROR("Point cloud is out of elevation map boundary. Change params ->mapArrayLength<-. The program will crash!");
            return false;
        }
        // sub-map id
        thisGrid.mapID = mapArrayInd[thisGrid.cubeX][thisGrid.cubeY];
        // Find the index for this point in this sub-map (grid index)
        thisGrid.gridX = (int)((point->x - mapArray[thisGrid.mapID]->originX) / mapResolution);
        thisGrid.gridY = (int)((point->y - mapArray[thisGrid.mapID]->originY) / mapResolution);
        if (thisGrid.gridX < 0 || thisGrid.gridY < 0 || thisGrid.gridX >= mapCubeArrayLength || thisGrid.gridY >= mapCubeArrayLength)
            return false;

        *gridOut = thisGrid;
        return true;
    }

    void getPointCubeIndex(int *cubeX, int *cubeY, PointType *point){
        *cubeX = int((point->x + mapCubeLength/2.0) / mapCubeLength) + rootCubeIndex;
        *cubeY = int((point->y + mapCubeLength/2.0) / mapCubeLength) + rootCubeIndex;

        if (point->x + mapCubeLength/2.0 < 0)  --*cubeX;
        if (point->y + mapCubeLength/2.0 < 0)  --*cubeY;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////// Traversability Calculation ///////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void TraversabilityThread(){

        ros::Rate rate(10); // Hz
        
        while (ros::ok()){

            traversabilityMapCalculation();

            rate.sleep();
        }
    }

    void traversabilityMapCalculation(){

        // no new scan, return
        if (observingList1.size() == 0)
            return;

        observingList2 = observingList1;
        observingList1.clear();

        int listSize = observingList2.size();

        for (int i = 0; i < listSize; ++i){

            mapCell_t *thisCell = observingList2[i];
            // convert this cell to a point for convenience
            PointType thisPoint;
            thisPoint.x = thisCell->xyz->x;
            thisPoint.y = thisCell->xyz->y;
            thisPoint.z = thisCell->xyz->z;
            // too far, not accurate
            if (pointDistance(thisPoint, robotPoint) >= traversabilityCalculatingDistance)
                continue;
            // Find neighbor cells of this center cell
            vector<float> xyzVector = findNeighborElevations(thisCell);

            if (xyzVector.size() <= 2)
                continue;

            Eigen::MatrixXf matPoints = Eigen::Map<const Eigen::Matrix<float, -1, -1, Eigen::RowMajor>>(xyzVector.data(), xyzVector.size() / 3, 3);

            // min and max elevation
            float minElevation = matPoints.col(2).minCoeff();
            float maxElevation = matPoints.col(2).maxCoeff();
            float maxDifference = maxElevation - minElevation;

            if (maxDifference > filterHeightLimit){
                thisPoint.intensity = 100;
                updateCellOccupancy(thisCell, &thisPoint);
                continue;
            }

            // find slope
            Eigen::MatrixXf centered = matPoints.rowwise() - matPoints.colwise().mean();
            Eigen::MatrixXf cov = (centered.adjoint() * centered);
            cv::eigen2cv(cov, matCov); // copy data from eigen to cv::Mat
            cv::eigen(matCov, matEig, matVec); // find eigenvalues and eigenvectors for the covariance matrix

            // float slopeAngle = std::acos(std::abs(matVec.at<float>(2, 2))) / M_PI * 180;
            // // float occupancy = 1.0f / (1.0f + exp(-(slopeAngle - filterAngleLimit)));

            // float occupancy = 0.5 * (slopeAngle / filterAngleLimit)
            //                 + 0.5 * (maxDifference / filterHeightLimit);

            // if (slopeAngle > filterAngleLimit || maxDifference > filterHeightLimit)
            //     thisPoint.intensity = 100;
            thisPoint.intensity = 100;
            updateCellOccupancy(thisCell, &thisPoint);
        }
    }

    vector<float> findNeighborElevations(mapCell_t *centerCell){

        vector<float> xyzVector;

        grid_t centerGrid = centerCell->grid;
        grid_t thisGrid;

        int footprintRadiusLength = int(robotRadius / mapResolution);

        for (int k = -footprintRadiusLength; k <= footprintRadiusLength; ++k){
            for (int l = -footprintRadiusLength; l <= footprintRadiusLength; ++l){
                // skip grids too far
                if (std::sqrt(float(k*k + l*l)) * mapResolution > robotRadius)
                    continue;
                // the neighbor grid
                thisGrid.cubeX = centerGrid.cubeX;
                thisGrid.cubeY = centerGrid.cubeY;
                thisGrid.gridX = centerGrid.gridX + k;
                thisGrid.gridY = centerGrid.gridY + l;
                // If the checked grid is in another sub-map, update it's indexes
                if(thisGrid.gridX < 0){ --thisGrid.cubeX; thisGrid.gridX = thisGrid.gridX + mapCubeArrayLength;
                }else if(thisGrid.gridX >= mapCubeArrayLength){ ++thisGrid.cubeX; thisGrid.gridX = thisGrid.gridX - mapCubeArrayLength; }
                if(thisGrid.gridY < 0){ --thisGrid.cubeY; thisGrid.gridY = thisGrid.gridY + mapCubeArrayLength;
                }else if(thisGrid.gridY >= mapCubeArrayLength){ ++thisGrid.cubeY; thisGrid.gridY = thisGrid.gridY - mapCubeArrayLength; }
                // If the sub-map that the checked grid belongs to is empty or not
                int mapInd = mapArrayInd[thisGrid.cubeX][thisGrid.cubeY];
                if (mapInd == -1) continue;
                // the neighbor cell
                mapCell_t *thisCell = grid2Cell(&thisGrid);
                // save neighbor cell for calculating traversability
                if (thisCell->elevation != -FLT_MAX){
                    xyzVector.push_back(thisCell->xyz->x);
                    xyzVector.push_back(thisCell->xyz->y);
                    xyzVector.push_back(thisCell->xyz->z);
                }
            }
        }

        return xyzVector;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////// Occupancy Map (local) //////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void publishMap(){
        // Publish Occupancy Grid Map and Elevation Map
        pubCount++;
        if (pubCount > visualizationFrequency){
            pubCount = 1;
            //publishLocalMap();
            publishTraversabilityMap();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////// Point Cloud /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void publishTraversabilityMap(){

        // 1. Find robot current cube index
        pcl::PointCloud<PointType>::Ptr laserCloudElevationTemp;
        laserCloudElevationTemp.reset(new pcl::PointCloud<PointType>());
        int currentCubeX, currentCubeY;
        getPointCubeIndex(&currentCubeX, &currentCubeY, &robotPoint);
        // 2. Loop through all the sub-maps that are nearby
        int visualLength = int(visualizationRadius / mapCubeLength);
        for (int i = -visualLength; i <= visualLength; ++i){
            for (int j = -visualLength; j <= visualLength; ++j){

                if (sqrt(float(i*i+j*j)) >= visualLength) continue;

                int idx = i + currentCubeX;
                int idy = j + currentCubeY;

                if (idx < 0 || idx >= mapArrayLength ||  idy < 0 || idy >= mapArrayLength) continue;

                if (mapArrayInd[idx][idy] == -1) continue;

                *laserCloudElevationTemp += mapArray[mapArrayInd[idx][idy]]->cloud;
            }
        }

        

        laserCloudElevationMtx.lock();
        laserCloudElevation.reset(new pcl::PointCloud<PointType>());
        *laserCloudElevation += *laserCloudElevationTemp;
        laserCloudElevationMtx.unlock();

        
        // sensor_msgs::PointCloud2 laserCloudTemp;
        // pcl::toROSMsg(*laserCloudElevation, laserCloudTemp);
        // laserCloudTemp.header.frame_id = "/map";
        // laserCloudTemp.header.stamp = ros::Time::now();
        // pubElevationCloud.publish(laserCloudTemp);



    }

    // void publishMaximumThread()
    // {
    //     ros::Rate rate(10);
    //     pcl::VoxelGrid<PointType> voxelGrid;
    //     voxelGrid.setLeafSize(voxel, voxel, voxel);
    //     while(ros::ok())
    //     {
    //         if(pubMaximumCloud.getNumSubscribers() == 0)
    //         {
    //             rate.sleep();
    //             continue;
    //         }
            
    //         pcl::PointCloud<PointType>::Ptr laserCloudTemp;
    //         laserCloudTemp.reset(new pcl::PointCloud<PointType>());
    //         laserCloudElevationMtx.lock();
    //         *laserCloudTemp += *laserCloudElevation;
    //         laserCloudElevationMtx.unlock();

    //         if(laserCloudTemp->empty())
    //         {
    //             rate.sleep();
    //             continue;
    //         }


    //         LocalMaximumPoints.reset(new pcl::PointCloud<PointType>);

    //         double time_begin = ros::Time::now().toSec();

    //         voxelGrid.setInputCloud(laserCloudTemp);
    //         voxelGrid.filter(*laserCloudTemp);

    //         pcl::LocalMaximum<PointType> lm;
    //         lm.setInputCloud(laserCloudTemp);
    //         lm.setRadius(radius);
    //         lm.setNegative(true);
    //         lm.filter(*LocalMaximumPoints);

    //         double time_end = ros::Time::now().toSec();
    //         ROS_INFO("LocalMaximumPoints: %d points, time: %lf", LocalMaximumPoints->size(), time_end - time_begin);

    //         sensor_msgs::PointCloud2 msg;
    //         pcl::toROSMsg(*LocalMaximumPoints, msg);
    //         msg.header.frame_id = "/map";
    //         msg.header.stamp = ros::Time::now();
    //         pubMaximumCloud.publish(msg);
    //         rate.sleep();
    //     }

    //     pcl::io::savePCDFileBinary("/home/jj/kk-mfm-master/LocalMaximum.pcd", *LocalMaximumPoints);

        
    // }

    pcl::PointCloud<PointType>::Ptr segPointCloud(pcl::PointCloud<PointType>::Ptr cloudIn)
    {
        
        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());

        int obstacles_size = obstacles.size();

        for(auto p:cloudIn->points)
        {
            bool IsIn = false;
            for(int i = 0; i < obstacles_size; i++)
            {
                IsIn = obstacles[i].IsPointInBBox(p);
                if(IsIn)
                    break;
            }
            if(IsIn)
                continue;
            else
                cloudOut->points.push_back(PointType(p));
        }
        return cloudOut;
    }

    void sendUDPThread()
    {
        int sock_fd;
        sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
        if(sock_fd < 0)
        {
            ROS_INFO("SOCKET ERROR");
            return;
        }
        else
            ROS_INFO("SOCKET SUCCESS");
        fcntl(sock_fd, F_SETFL, fcntl(sock_fd, F_GETFL, 0) | O_NONBLOCK);

        vector<struct sockaddr_in> addr_serv_vec;
        int len;
        for(int i = 0; i < UDPNUM; i++)
        {
            string ip_port = UDPs[i];
            string ip = ip_port.substr(0, ip_port.find(":"));
            int port = atoi(ip_port.substr(ip_port.find(":")+1).c_str());
            ROS_INFO("ip: %s, port: %d", ip.c_str(), port);
            struct sockaddr_in addr_serv;
            memset(&addr_serv, 0, sizeof(addr_serv));
            addr_serv.sin_family = AF_INET;
            addr_serv.sin_addr.s_addr = inet_addr(ip.c_str());
            addr_serv.sin_port = htons(port);
            addr_serv_vec.push_back(addr_serv);
        }
        
        len = sizeof(addr_serv_vec[0]);

        pcl::PointCloud<PointType>::Ptr tmpElevationCloud;
        int flag = 0;
        ros::Rate rate(2);
        while(ros::ok())
        {
            flag = (flag + 1) % 5;
            if(flag > 1)
            {
                rate.sleep();
                continue;
            }

            int send_num;
            unsigned char *send_buf = (unsigned char *)malloc(17938);
            memset(send_buf, 0, sizeof(send_buf));

            tmpElevationCloud.reset(new pcl::PointCloud<PointType>());
            laserCloudElevationMtx.lock();
            *tmpElevationCloud += *laserCloudElevation;
            laserCloudElevationMtx.unlock();

            if(tmpElevationCloud->empty())
            {
                rate.sleep();
                continue;
            }

            mtx.lock();
            double x_pos_cur_tmp = round(x_pos_cur[flag]);
            mtx.unlock();

            double time_sor_begin = ros::Time::now().toSec();
            pcl::PointCloud<PointType>::Ptr laserCloudSegObstacles;
            laserCloudSegObstacles.reset(new pcl::PointCloud<PointType>());
            *laserCloudSegObstacles += *segPointCloud(tmpElevationCloud);

            pcl::PointCloud<PointType>::Ptr tmpElevationCloudPassX;
            tmpElevationCloudPassX.reset(new pcl::PointCloud<PointType>());
            pcl::PassThrough<PointType> pass;
            pass.setInputCloud(laserCloudSegObstacles);
            pass.setFilterFieldName("x");
            pass.setFilterLimits(x_pos_cur_tmp - 15, x_pos_cur_tmp + 15);
            pass.filter(*tmpElevationCloudPassX);

            if(tmpElevationCloudPassX->size() < 10)
            {
                rate.sleep();
                continue;
            }


            
            pcl::PointCloud<PointType>::Ptr laserCloudElevationTempSOR;
            laserCloudElevationTempSOR.reset(new pcl::PointCloud<PointType>());
            sor.setInputCloud(tmpElevationCloudPassX);
            sor.setMeanK(sor_kmean);
            sor.setStddevMulThresh(sor_threshold);
            sor.filter(*laserCloudElevationTempSOR);
            double time_sor_end = ros::Time::now().toSec();
            ROS_INFO("SOR: %f", time_sor_end - time_sor_begin);
            int cloudSize = laserCloudElevationTempSOR->size();

            vector<MyVoxel> voxel_vec;
            for(double x = x_pos_cur_tmp - 10.0; x < x_pos_cur_tmp + 10.0; x += voxel)
                for(double y = 0.0; y < 32.0; y += voxel)
                    voxel_vec.push_back(MyVoxel(x, y, voxel));
            
            for(int i = 0; i < cloudSize; i++)
            {
                PointType p = laserCloudElevationTempSOR->points[i];
                if(p.x > x_pos_cur_tmp - 10.0 && p.x < x_pos_cur_tmp + 10.0 && p.y > 0 && p.y < 32.0 && p.z > 0.0 && p.z < 12.0)
                {
                    int idx = floor((p.x - x_pos_cur_tmp + 10.0) / voxel), idy = floor(p.y / voxel);
                    int index = idx * round(32 / (int)voxel) + idy;
                    voxel_vec[index].insertPoint(p);
                }
                
            }

            send_buf[0] = 0xDD;
            send_buf[1] = 0x46;
            send_buf[2] = 0x0E;
            send_buf[3] = 0x00;
            send_buf[4] = 0x00;
            send_buf[5] = 0x00;
            send_buf[6] = 0x00;
            send_buf[7] = 0x00;
            send_buf[8] = 0x00;
            send_buf[9] = 0x01;
            int count = 0;
            pcl::PointCloud<PointType>::Ptr blockCloud;
            blockCloud.reset(new pcl::PointCloud<PointType>());
            for(int i = 0; i < 640; i++)
            {
                MyVoxel v = voxel_vec[i];
                if(v.size == 0)
                    continue;
                // {
                //     blockCloud->push_back(v.pt_center);
                //     blockCloud->points[i].intensity = 0;
                //     continue;
                // }

                blockCloud->push_back(v.pt_highest);
                blockCloud->points[count].intensity = v.volumn * 10.0;
                PointType p1 = v.pt_center, p2 = v.pt_highest;
                uint32_t x1 = (uint32_t)round(p1.x * 100), y1 = (uint32_t)round(p1.y * 100), z1 = (uint32_t)round(p1.z * 100);
                uint32_t x2 = (uint32_t)round(p2.x * 100), y2 = (uint32_t)round(p2.y * 100), z2 = (uint32_t)round(p2.z * 100);
                uint32_t volumn = round(v.volumn * 10000);
                send_buf[12 + count * 28] = (unsigned char)(x1 / 16777216);
                send_buf[13 + count * 28] = (unsigned char)(x1 % 16777216 / 65536);
                send_buf[14 + count * 28] = (unsigned char)(x1 % 65536 / 256);
                send_buf[15 + count * 28] = (unsigned char)(x1 % 256);
                send_buf[16 + count * 28] = (unsigned char)(y1 / 16777216);
                send_buf[17 + count * 28] = (unsigned char)(y1 % 16777216 / 65536);
                send_buf[18 + count * 28] = (unsigned char)(y1 % 65536 / 256);
                send_buf[19 + count * 28] = (unsigned char)(y1 % 256);
                send_buf[20 + count * 28] = (unsigned char)(z1 / 16777216);
                send_buf[21 + count * 28] = (unsigned char)(z1 % 16777216 / 65536);
                send_buf[22 + count * 28] = (unsigned char)(z1 % 65536 / 256);
                send_buf[23 + count * 28] = (unsigned char)(z1 % 256);
                send_buf[24 + count * 28] = (unsigned char)(x2 / 16777216);
                send_buf[25 + count * 28] = (unsigned char)(x2 % 16777216 / 65536);
                send_buf[26 + count * 28] = (unsigned char)(x2 % 65536 / 256);
                send_buf[27 + count * 28] = (unsigned char)(x2 % 256);
                send_buf[28 + count * 28] = (unsigned char)(y2 / 16777216);
                send_buf[29 + count * 28] = (unsigned char)(y2 % 16777216 / 65536);
                send_buf[30 + count * 28] = (unsigned char)(y2 % 65536 / 256);
                send_buf[31 + count * 28] = (unsigned char)(y2 % 256);
                send_buf[32 + count * 28] = (unsigned char)(z2 / 16777216);
                send_buf[33 + count * 28] = (unsigned char)(z2 % 16777216 / 65536);
                send_buf[34 + count * 28] = (unsigned char)(z2 % 65536 / 256);
                send_buf[35 + count * 28] = (unsigned char)(z2 % 256);
                send_buf[36 + count * 28] = (unsigned char)(volumn / 16777216);
                send_buf[37 + count * 28] = (unsigned char)(volumn % 16777216 / 65536);
                send_buf[38 + count * 28] = (unsigned char)(volumn % 65536 / 256);
                send_buf[39 + count * 28] = (unsigned char)(volumn % 256);
                count++;
            }
            send_buf[10] = (unsigned char)(count / 256);
            send_buf[11] = (unsigned char)(count % 256);
            uint32_t x_pos = x_pos_cur[flag] == -1.0 ? 0 : (uint32_t)round((x_pos_cur[flag] -2.38) * 100);
            send_buf[17932] = (unsigned char)(x_pos / 16777216);
            send_buf[17933] = (unsigned char)(x_pos % 16777216 / 65536);
            send_buf[17934] = (unsigned char)(x_pos % 65536 / 256);
            send_buf[17935] = (unsigned char)(x_pos % 256);

            time_send_udp1 = time_send_udp2 = ros::Time::now().toSec();
            if(time_send_udp1 * time_laser1 != 0 && abs(time_send_udp1 - time_laser1) < 5.0)
                send_buf[17936] += 1;
            if(time_send_udp2 * time_laser2 != 0 && abs(time_send_udp2 - time_laser2) < 5.0)
                send_buf[17936] += 16;
            send_buf[17937] = 0xBB; //getCheckCode(send_buf, 17937);

            

            for(int i = 0; i < UDPNUM; i++)
            {
                unsigned char *send_buf_tmp = (unsigned char *)malloc(17938);
                memcpy(send_buf_tmp, send_buf, 17938);
                send_num = sendto(sock_fd, send_buf_tmp, 17938, 0, (struct sockaddr *)&addr_serv_vec[i], sizeof(addr_serv_vec[i]));
                free(send_buf_tmp);
                //ROS_INFO("send_num: %d, valid count: %d", send_num, count);
            }

            free(send_buf);

            sensor_msgs::PointCloud2 laserCloudTemp;
            pcl::toROSMsg(*laserCloudSegObstacles, laserCloudTemp);
            laserCloudTemp.header.frame_id = "/map";
            laserCloudTemp.header.stamp = ros::Time::now();
            pubElevationCloud.publish(laserCloudTemp);

            sensor_msgs::PointCloud2 msg;
            pcl::toROSMsg(*blockCloud, msg);
            msg.header.frame_id = "/map";
            msg.header.stamp = ros::Time::now();
            pubMaximumCloud.publish(msg);

            rate.sleep();
        }
    }
};




int main(int argc, char** argv){

    ros::init(argc, argv, "traversability_mapping");
    
    TraversabilityMapping tMapping;

    std::thread predictionThread(&TraversabilityMapping::TraversabilityThread, &tMapping);
    std::thread SendUDPThread(&TraversabilityMapping::sendUDPThread, &tMapping);
    //std::thread PublishMaximumThread(&TraversabilityMapping::publishMaximumThread, &tMapping);

    ROS_INFO("\033[1;32m---->\033[0m Traversability Mapping Started.");

    ros::spin();

    predictionThread.join();
    SendUDPThread.join();
    //PublishMaximumThread.join();

    return 0;
}