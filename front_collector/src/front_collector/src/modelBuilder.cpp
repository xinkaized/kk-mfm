#include <my_utility.h>

#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

class ModelBuilder{
    private:
    ros::NodeHandle nh, ng;
    ros::Subscriber subLaserCloud, subPoseCloud;
    ros::Subscriber subImu;

    ros::Publisher pubModelCloud, pubSegCloud, pubPoseCloud;

    pcl::PointCloud<PointType>::Ptr laserCloudIn, laserCloudSeg, modelCloud;
    pcl::PointCloud<PointTypePose>::Ptr poseCloud, poseCloudIn;
    pcl::PassThrough<PointType> xFilter, zFilter, yFilter;
    vector<BoundingBox> obstacles;

    tf::StampedTransform imuOdometryTrans;  // map与激光雷达的tf变换
    tf::TransformBroadcaster tfBroadcaster_imuOdometryTrans;

    mutex mtx, poseMtx;

    double timeLaserCloudIn;
    int laserCloudInPointerLast;
    double laserCloudInTimeArray[imuQueLength / 2];
    bool laserCloudInProcessed[imuQueLength / 2];
    pcl::PointCloud<PointType>::Ptr laserCloudInArray[imuQueLength / 2];

    int laserCloudSegPointerLast;
    vector<pcl::PointCloud<PointType>::Ptr> laserCloudSegArray;

    int imuPointerLast;
    double latestImuTime;
    double imuTime[imuQueLength];
    float imuShiftX[imuQueLength];
    float imuShiftY[imuQueLength];
    float imuShiftZ[imuQueLength];

    PointTypePose CurPose, LastPose, nanPoint;
    pcl::VoxelGrid<PointType> downSizeFilter, segDownSizeFilter;

    string IPOUT;
    int PORTIN, PORTOUT;
    int IsSim, IsBack, IsPosCorrect, IsSeg, IsModelBuild, map_size, sor_kmean;
    double resolution, threshold_z, threshold_x, map_gap, x_offset, sor_threshold;
    double min_y, max_y, min_z, max_z;





    public:
    ModelBuilder():
    nh("~"){
        timeLaserCloudIn = latestImuTime = -1.0;

        
        IsSim = 0;

        laserCloudInPointerLast = -1;
        for(int i = 0; i < imuQueLength / 2; i++)
        {
            laserCloudInArray[i].reset(new pcl::PointCloud<PointType>());
            laserCloudInTimeArray[i] = -1.0;
            laserCloudInProcessed[i] = true;
        }

        imuPointerLast = -1;
        for (int i = 0; i < imuQueLength; ++i)
        {
            imuTime[i] = -1.0;
            imuShiftX[i] = 0; imuShiftY[i] = 0; imuShiftZ[i] = 0;
        }

        imuOdometryTrans.frame_id_ = "/map";
        imuOdometryTrans.child_frame_id_ = "/hesai_frame";
        downSizeFilter.setLeafSize(0.1, 0.1, 0.1);
        segDownSizeFilter.setLeafSize(0.1, 0.1, 0.1);

        ng.getParam("IPOUT", IPOUT);
        ng.getParam("PORTIN", PORTIN);
        ng.getParam("PORTOUT", PORTOUT);
        ng.getParam("IsSim", IsSim);
        ng.getParam("resolution", resolution);
        ng.getParam("threshold_z", threshold_z);
        ng.getParam("threshold_x", threshold_x);
        ng.getParam("min_z", min_z);
        ng.getParam("max_z", max_z);
        ng.getParam("min_y", min_y);
        ng.getParam("max_y", max_y);
        ng.getParam("x_offset", x_offset);
        ng.getParam("map_gap", map_gap);
        ng.getParam("map_size", map_size);
        ng.getParam("sor_kmean", sor_kmean);
        ng.getParam("sor_threshold", sor_threshold);
        ng.getParam("IsBack", IsBack);
        ng.getParam("IsPosCorrect", IsPosCorrect);
        ng.getParam("IsSeg", IsSeg);
        ng.getParam("IsModelBuild", IsModelBuild);
        
        

        laserCloudSegPointerLast = -1;
        for(int i = 0; i < map_size; i++)
        {
            laserCloudSegArray.push_back(pcl::PointCloud<PointType>::Ptr(new pcl::PointCloud<PointType>()));
            laserCloudSegArray[i].reset(new pcl::PointCloud<PointType>());
        }
        
        xFilter.setFilterFieldName("x");
        xFilter.setFilterLimits(-threshold_x, threshold_x);
        xFilter.setFilterLimitsNegative(true);

        zFilter.setFilterFieldName("z");
        zFilter.setFilterLimits(min_z, max_z);  //-1.0, 8.40

        yFilter.setFilterFieldName("y");
        yFilter.setFilterLimits(min_y, max_y);  //0.0, 31.45

        // string foldername = string("/home/oem/kk-mfm/front_collector/seg/");
        // FILE *fp = fopen((foldername + "bbox.txt").c_str(), "w");
        // for(int i = 1; i < 13; i++)
        // {
        //     pcl::PointCloud<PointType>::Ptr bboxPCD;
        //     bboxPCD.reset(new pcl::PointCloud<PointType>());
        //     pcl::io::loadPCDFile(foldername + to_string(i) + ".pcd", *bboxPCD);
        //     PointType pt1, pt2;
        //     pcl::getMinMax3D(*bboxPCD, pt1, pt2);
        //     fprintf(fp, "obstacles.push_back(BoundingBox(%lf, %lf, %lf, %lf, %lf, %lf));\n", i, pt1.x, pt1.y, pt1.z, pt2.x, pt2.y, pt2.z);
        // }
        // fclose(fp);


        obstacles.push_back(BoundingBox(275.55, 0, 0, 278.05, 32.0, 10.5));    // 6#库左侧墙
        obstacles.push_back(BoundingBox(200.0, 29.45, 0, 350.0, 32.0, 10.5));  // 下侧墙
        obstacles.push_back(BoundingBox(247.2, 0.0, 0.0, 248.7, 32.0, 10.5));  // 4-5#库隔断墙
        obstacles.push_back(BoundingBox(259.3, 0.0, 0.0, 260.8, 32.0, 10.5));  // 5-6#库隔断墙
        //obstacles.push_back(BoundingBox(253.6, 0.0, 0.0, 272.6, 8.7, 8.4));   // 5-6料仓台
        obstacles.push_back(BoundingBox(270.5, 0.0, 0.0, 272.6, 8.7, 10.5));   // 5-6库料仓台：竖直1
        obstacles.push_back(BoundingBox(264.6, 0.0, 0.0, 267.3, 8.7, 10.5));   // 5-6库料仓台：竖直2
        obstacles.push_back(BoundingBox(258.5, 0.0, 0.0, 261.4, 8.7, 10.5));   // 5-6库料仓台：竖直3
        obstacles.push_back(BoundingBox(253.4, 0.0, 0.0, 255.3, 8.7, 10.5));   // 5-6库料仓台：竖直4
        obstacles.push_back(BoundingBox(253.4, 0.0, 0.0, 272.6, 1.5, 10.5));   // 5-6库料仓台：水平5
        obstacles.push_back(BoundingBox(253.4, 6.3, 0.0, 272.6, 8.7, 10.5));   // 5-6库料仓台：水平6
        obstacles.push_back(BoundingBox(289.1, 0.0, 0.0, 294.1, 4.25, 10.5));   // 7#仓（平板库）左上角
        obstacles.push_back(BoundingBox(293.8, 0.0, 0.0, 350.0, 32.0, 10.5));   // 7#仓（平板库）左侧
        obstacles.push_back(BoundingBox(233.2, 23.8, 0.0, 242.1, 32.0, 10.5));   // 4仓左下角
        obstacles.push_back(BoundingBox(253.6, 26.0, 0.0, 265.8, 32.0, 10.5));   // 5-6仓下侧平台
        //obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 224.6, 8.8, 10.5));   // 4仓料仓台
        obstacles.push_back(BoundingBox(181.6, 0.0, 0.0, 182.8, 32.0, 10.5));  // 3-4#库隔断墙
        obstacles.push_back(BoundingBox(222.6, 0.0, 0.0, 224.8, 8.9, 10.5));   // 4#库料仓台：竖直1
        obstacles.push_back(BoundingBox(216.7, 0.0, 0.0, 218.8, 8.9, 10.5));   // 4#库料仓台：竖直2
        obstacles.push_back(BoundingBox(210.6, 0.0, 0.0, 213.2, 8.9, 10.5));   // 4#库料仓台：竖直3
        obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 207.0, 8.9, 10.5));   // 4#库料仓台：竖直4
        obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 224.8, 1.75, 10.5));   // 4#库料仓台：水平1
        obstacles.push_back(BoundingBox(205.5, 6.4, 0.0, 224.8, 8.9, 10.5));   // 4#库料仓台：水平2
        obstacles.push_back(BoundingBox(144.6, 0.0, 0.0, 146.5, 9.0, 10.5));   // 3#库料仓台：竖直1
        obstacles.push_back(BoundingBox(138.6, 0.0, 0.0, 141.2, 9.0, 10.5));   // 3#库料仓台：竖直2
        obstacles.push_back(BoundingBox(132.6, 0.0, 0.0, 135.2, 9.0, 10.5));   // 3#库料仓台：竖直3
        obstacles.push_back(BoundingBox(127.0, 0.0, 0.0, 129.2, 9.0, 10.5));   // 3#库料仓台：竖直4
        obstacles.push_back(BoundingBox(127.0, 0.0, 0.0, 146.5, 0.86, 10.5));   // 3#库料仓台：水平1
        obstacles.push_back(BoundingBox(127.0, 6.12, 0.0, 146.5, 9.0, 10.5));   // 3#库料仓台：水平2
        obstacles.push_back(BoundingBox(109.5, 0.0, 0.0, 110.5, 32.0, 10.5));   //2-3#库隔断墙
        obstacles.push_back(BoundingBox(45.8, 0.0, 0.0, 68.8, 10.5, 10.5));   //2#库料仓台
        obstacles.push_back(BoundingBox(55.8, 10.5, 0.0, 56.8, 32.0, 10.5));   //1-2#库隔断墙
        obstacles.push_back(BoundingBox(56.7, 26.6, 0.0, 109.7, 32.0, 10.5));   //2#库底部，因为2#库y最大值比其它库小要单独切割

        

        laserCloudIn.reset(new pcl::PointCloud<PointType>());
        laserCloudSeg.reset(new pcl::PointCloud<PointType>());
        modelCloud.reset(new pcl::PointCloud<PointType>());
        poseCloud.reset(new pcl::PointCloud<PointTypePose>());
        poseCloudIn.reset(new pcl::PointCloud<PointTypePose>());
        nanPoint.x = std::numeric_limits<float>::quiet_NaN();
        nanPoint.y = std::numeric_limits<float>::quiet_NaN();
        nanPoint.z = std::numeric_limits<float>::quiet_NaN();
        nanPoint.intensity = -1;
        nanPoint.time = -1.0;
        poseCloud->points.resize(imuQueLength);
        std::fill(poseCloud->points.begin(), poseCloud->points.end(), nanPoint);

        subLaserCloud = nh.subscribe<sensor_msgs::PointCloud2>("/full_cloud", 2, &ModelBuilder::laserCloudHandler, this);
        pubSegCloud = nh.advertise<sensor_msgs::PointCloud2>("/seg_cloud", 2);
        pubModelCloud = nh.advertise<sensor_msgs::PointCloud2>("/model_cloud", 2);
        if(IsSim == 1)
            subPoseCloud = nh.subscribe<sensor_msgs::PointCloud2>("/pose_cloud", 2, &ModelBuilder::poseCloudHandler, this);
        else
            pubPoseCloud = nh.advertise<sensor_msgs::PointCloud2>("/pose_cloud", 2);
    }

    void laserCloudHandler(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        laserCloudIn.reset(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(*laserCloudMsg, *laserCloudIn);
        timeLaserCloudIn = laserCloudMsg->header.stamp.toSec();
        laserCloudInPointerLast = (laserCloudInPointerLast + 1) % (imuQueLength / 2);
        laserCloudInArray[laserCloudInPointerLast].reset(new pcl::PointCloud<PointType>());
        *laserCloudInArray[laserCloudInPointerLast] += *laserCloudIn;
        laserCloudInTimeArray[laserCloudInPointerLast] = timeLaserCloudIn;
        laserCloudInProcessed[laserCloudInPointerLast] = false;
        // ROS_INFO("laserIn Time:%lf", timeLaserCloudIn);
    }

    static bool cmp(pair<double, double> a, pair<double, double> b)
    {
        return a.first < b.first;
    }

    void poseCloudHandler(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        poseCloudIn.reset(new pcl::PointCloud<PointTypePose>());
        pcl::fromROSMsg(*laserCloudMsg, *poseCloudIn);
        vector<pair<double, double>> TimeShiftXTemp;
        double latestImuTimeTemp = latestImuTime;
        for(int i = 0; i < imuQueLength; i++)
        {
            PointTypePose p = poseCloudIn->points[i];
            double ShiftXTemp = IsBack == 1 ? 250 - p.x : p.x;   // TO BE CORRECTED
            double TimeTemp = p.time;
            TimeShiftXTemp.push_back(make_pair(TimeTemp, ShiftXTemp));
            if(latestImuTime < TimeTemp)
            {
                latestImuTimeTemp = TimeTemp;
                //ROS_INFO("imuTime:%lf, imuShiftX:%lf", TimeTemp, ShiftXTemp);
            }
        }
        latestImuTime = latestImuTimeTemp;
        sort(TimeShiftXTemp.begin(), TimeShiftXTemp.end(), ModelBuilder::cmp);

        // interpolate
        double var = TimeShiftXTemp[0].second, time = 0.0;
        for(int i = 1; i < imuQueLength; i++)
        {
            if(abs(TimeShiftXTemp[i].second - var) > 2.0)
                TimeShiftXTemp[i].first = -1.0;
            else if(TimeShiftXTemp[i].second == var)
                TimeShiftXTemp[i].first =  IsPosCorrect == 1 ? TimeShiftXTemp[i].first : -1.0;
            else
            {
                var = TimeShiftXTemp[i].second;
                time = TimeShiftXTemp[i].first;
            }
        }

        poseMtx.lock();
        int error_count = 0;
        for(int i = 0; i < imuQueLength; i++)
        {
            imuTime[i] = TimeShiftXTemp[i].first;
            imuShiftX[i] = TimeShiftXTemp[i].second;
        }
        imuPointerLast = imuQueLength - 1;
        poseMtx.unlock();


        CurPose.x = var + x_offset;
        CurPose.y = 23.68;
        CurPose.time = time;
    }

    
    unsigned char getCheckCode(unsigned char *sendBuf, int dataLen)
    {
        unsigned char checkCode = 0;
        for(int i = 0; i < dataLen; i++)
            checkCode += sendBuf[i];
        return checkCode;
    }

    void listenUDPThread()
    {
        if(IsSim == 1)
            return;
        int sock_fd;
        sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
        if(sock_fd < 0)
        {
            ROS_INFO("SOCKET ERROR");
            return;
        }
        else
            ROS_INFO("SOCKET SUCCESS");
        fcntl(sock_fd, F_SETFL, fcntl(sock_fd, F_GETFL, 0) | O_NONBLOCK);

        struct sockaddr_in addr_serv;
        int len;
        memset(&addr_serv, 0, sizeof(addr_serv));
        addr_serv.sin_family = AF_INET;
        addr_serv.sin_addr.s_addr = htonl(INADDR_ANY);
        addr_serv.sin_port = htons(PORTIN);
        len = sizeof(addr_serv);

        if(bind(sock_fd, (struct sockaddr *)&addr_serv, sizeof(addr_serv)) < 0)  
        {  
            ROS_INFO("BIND ERROR");
            return; 
        }  


        int recv_num;
        unsigned char recv_buf[85];
        ros::Rate rate(20);
        ROS_INFO("BEGIN LISTEN");
        while(ros::ok())
        {
            recv_num = recvfrom(sock_fd, recv_buf, 85, 0, (struct sockaddr *)&addr_serv, (socklen_t *)&len);
            // if(recv_num < 0)
            // {
            //     ROS_INFO("RECVFROM ERROR");
            // }
            // else
            //ROS_INFO("recv_num: %d", recv_num);

            if(recv_buf[0] == 0xA1 || recv_buf[0] == 0xA2 || recv_buf[0] == 0xA3 || recv_buf[0] == 0xA4)
            {
                if(getCheckCode(recv_buf, 84) != recv_buf[84])
                {
                    rate.sleep();
                    continue;
                }
                
                double CurTime = ros::Time::now().toSec();
                uint32_t position;
                position = recv_buf[11] << 24 | recv_buf[12] << 16 | recv_buf[13] << 8 | recv_buf[14];
                double ShiftXTemp = position * 0.01;

                if(abs(ShiftXTemp - imuShiftX[imuPointerLast]) > 2.0 && imuPointerLast != -1)
                {
                    ROS_ERROR("imuTime:%lf, imuShiftX:%lf", CurTime, ShiftXTemp);
                    rate.sleep();
                    continue;
                }    

                poseMtx.lock();
                int imuPointerLastTemp = (imuPointerLast + 1) % imuQueLength;
                imuShiftX[imuPointerLastTemp] = ShiftXTemp;
                imuTime[imuPointerLastTemp] = CurTime;
                imuPointerLast = imuPointerLastTemp;
                poseMtx.unlock();

                //ROS_INFO("imuTime:%lf, imuShiftX:%lf", CurTime, ShiftXTemp);

                CurPose.x = ShiftXTemp + x_offset;
                CurPose.y = 23.68;
                CurPose.time = CurTime;
            }
            // else
            //     ROS_INFO("RECVFROM DATA ERROR");
            


            
            rate.sleep();
        }
    }

    void modelThread()
    {
        ros::Rate rate(10);
        double laserCloudInProcessedTimeLast = 0.0;
        while(ros::ok())
        {
            if(laserCloudInPointerLast == -1 || imuPointerLast == -1)
            {
                rate.sleep();
                continue;
            }

            double time_begin = ros::Time::now().toSec();

            double imuTimeTempArray[imuQueLength], imuShiftXTempArray[imuQueLength];
            poseMtx.lock();
            for(int i = 0; i < imuQueLength; i++)
            {
                imuShiftXTempArray[i] = imuShiftX[i];
                imuTimeTempArray[i] = imuTime[i];
            }
            poseMtx.unlock();
            
            
            int laserCloudInProcessingPointer = -1;
            int imuProcessingPointer = -1;
            double minTimeDiff = 0.10;
            for(int i = 0; i < imuQueLength / 2; i++)
            {
                bool laserCloudInProcessedTemp = laserCloudInProcessed[i];
                double laserCloudInTimeTemp = laserCloudInTimeArray[i];

                if(laserCloudInProcessedTemp == true || laserCloudInTimeTemp == -1.0 || laserCloudInTimeTemp < laserCloudInProcessedTimeLast)
                    continue;
                
                
                for(int j = 0; j < imuQueLength; j++)
                {
                    double imuTimeTemp = imuTimeTempArray[j];
                    if(imuTimeTemp == -1.0)
                        continue;
                    double timeDiffTemp = abs(laserCloudInTimeTemp - imuTimeTemp);
                    if(timeDiffTemp < minTimeDiff)
                    {
                        minTimeDiff = timeDiffTemp;
                        laserCloudInProcessingPointer = i;
                        imuProcessingPointer = j;
                    }
                }
            }

            //ROS_INFO("find laser-imu pair at index: %d - %d", laserCloudInProcessingPointer, imuProcessingPointer);
            if(laserCloudInProcessingPointer == -1 || imuProcessingPointer == -1 || minTimeDiff > 0.02)
            {
                rate.sleep();
                continue;
            }
            laserCloudInProcessed[laserCloudInProcessingPointer] = true;
            laserCloudInProcessedTimeLast = laserCloudInTimeArray[laserCloudInProcessingPointer];

            PointTypePose pose;
            pose.x = imuShiftXTempArray[imuProcessingPointer] + x_offset;
            pose.y = 23.68;
            pose.z = 0;
            pose.roll = 0;
            pose.pitch = 0;
            pose.yaw = PI;

            double ori = pose.x - LastPose.x;

            if(abs(ori) < map_gap)
            {
                rate.sleep();
                continue;
            }
            LastPose = pose;

            double laserCloudInTimeProcessing = laserCloudInTimeArray[laserCloudInProcessingPointer];

            //ROS_INFO("Laser:%lf, IMU:%lf, %lf", 
            //laserCloudInTimeProcessing, imuTimeTempArray[imuProcessingPointer], imuShiftXTempArray[imuProcessingPointer]);

            
            pcl::PointCloud<PointType>::Ptr  fullCloudTempPassX, fullCloudTempPassY, fullCloudTempPassZ, fullCloudTrans, laserCloudSegTemp, laserCloudSegTempSOR, laserCloudSegTempDS;
            fullCloudTempPassX.reset(new pcl::PointCloud<PointType>());
            fullCloudTempPassY.reset(new pcl::PointCloud<PointType>());
            fullCloudTempPassZ.reset(new pcl::PointCloud<PointType>());
            fullCloudTrans.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTemp.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTempSOR.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTempDS.reset(new pcl::PointCloud<PointType>());

            xFilter.setInputCloud(laserCloudInArray[laserCloudInProcessingPointer]);
            xFilter.filter(*fullCloudTempPassX);

            *fullCloudTrans +=  *transformPointCloud(fullCloudTempPassX, &pose) ; 
            zFilter.setInputCloud(fullCloudTrans);
            zFilter.filter(*fullCloudTempPassZ);
            yFilter.setInputCloud(fullCloudTempPassZ);
            yFilter.filter(*fullCloudTempPassY);

            
            *laserCloudSegTemp += * lowestCloudFilter ( segPointCloud( fullCloudTempPassY ) ) ;//*lowestCloudFilter( segPointCloud( fullCloudTempPassY ) );

            pcl::StatisticalOutlierRemoval<PointType> sor;
            sor.setInputCloud(laserCloudSegTemp);
            sor.setMeanK(sor_kmean);
            sor.setStddevMulThresh(sor_threshold);
            sor.filter(*laserCloudSegTempSOR);

            size_t cloudSize = laserCloudSegTempSOR->size();
            for(int i = 0; i < cloudSize; i++)
            {
                PointType *p = & laserCloudSegTempSOR->points[i];
                p->intensity = 100;
            } 

            segDownSizeFilter.setInputCloud(laserCloudSegTempSOR);
            segDownSizeFilter.filter(*laserCloudSegTempDS);


            double time_seg = ros::Time::now().toSec();
            //ROS_INFO("seg time: %lf", time_seg - time_begin);

            laserCloudSegPointerLast = (laserCloudSegPointerLast + 1) % map_size;
            laserCloudSegArray[laserCloudSegPointerLast].reset(new pcl::PointCloud<PointType>());
            *laserCloudSegArray[laserCloudSegPointerLast] += IsSeg == 1 ? *laserCloudSegTempDS : *fullCloudTempPassY;

            pcl::PointCloud<PointType>::Ptr modelCloudTemp;
            modelCloudTemp.reset(new pcl::PointCloud<PointType>());
            if(IsModelBuild == 1)
            {
                for(int i = 0; i < map_size; i++)
                {
                    pcl::PointCloud<PointType>::Ptr laser = laserCloudSegArray[i];
                    
                    if(!laser->empty())
                        *modelCloudTemp += *laser;
                    
                }
            }
            

            double time_finish = ros::Time::now().toSec();


            mtx.lock();
            laserCloudSeg.reset(new pcl::PointCloud<PointType>());
            *laserCloudSeg += *laserCloudSegTempSOR;
            modelCloud.reset(new pcl::PointCloud<PointType>());
            *modelCloud += *modelCloudTemp;
            mtx.unlock();

            rate.sleep();
        }
    }

    void publishThread()
    {
        ros::Rate rate(10);
        while(ros::ok())
        {
            geometry_msgs::Quaternion geoQuat = tf::createQuaternionMsgFromRollPitchYaw(0.0, 0.0, PI);
            imuOdometryTrans.stamp_ = ros::Time().fromSec(CurPose.time); //ros::Time::now();  //
            imuOdometryTrans.setRotation(tf::Quaternion(geoQuat.x, geoQuat.y, geoQuat.z, geoQuat.w));
            imuOdometryTrans.setOrigin(tf::Vector3(CurPose.x, CurPose.y, 0.0));
            tfBroadcaster_imuOdometryTrans.sendTransform(imuOdometryTrans);
            
            if(IsModelBuild == 1 && pubModelCloud.getNumSubscribers() != 0)
            {
                sensor_msgs::PointCloud2 laserCloudTemp;
                pcl::PointCloud<PointType>::Ptr modelCloudDS;
                modelCloudDS.reset(new pcl::PointCloud<PointType>());
                mtx.lock();
                downSizeFilter.setInputCloud(modelCloud);
                downSizeFilter.filter(*modelCloudDS);
                mtx.unlock();
                pcl::toROSMsg(*modelCloudDS, laserCloudTemp);
                laserCloudTemp.header.stamp = ros::Time::now();
                laserCloudTemp.header.frame_id = "/map";
                pubModelCloud.publish(laserCloudTemp);
            }

            if(pubSegCloud.getNumSubscribers() != 0)
            {
                sensor_msgs::PointCloud2 laserCloudTemp2;
                mtx.lock();
                pcl::toROSMsg(*laserCloudSeg, laserCloudTemp2);
                mtx.unlock();
                laserCloudTemp2.header.stamp = ros::Time::now();
                laserCloudTemp2.header.frame_id = "/map";
                pubSegCloud.publish(laserCloudTemp2);
            }

            if(IsSim != 1)
            {
                for(int i = 0; i < imuQueLength; i++)
                {
                    PointTypePose pose;
                    pose.x = imuShiftX[i];
                    pose.y = 0;
                    pose.z = 0;
                    pose.time = imuTime[i];
                    poseCloud->points[i] = pose;
                }
                sensor_msgs::PointCloud2 laserCloudTemp2;
                pcl::toROSMsg(*poseCloud, laserCloudTemp2);
                laserCloudTemp2.header.stamp = ros::Time::now();
                laserCloudTemp2.header.frame_id = "/map";
                pubPoseCloud.publish(laserCloudTemp2);
            }
            


            rate.sleep();

        }

        pcl::PointCloud<PointType>::Ptr modelCloudDS;
        modelCloudDS.reset(new pcl::PointCloud<PointType>());
        downSizeFilter.setInputCloud(modelCloud);
        downSizeFilter.filter(*modelCloudDS);
        pcl::io::savePCDFileBinary("/home/jj/kk-mfm-master/modelCloud.pcd", *modelCloudDS);
    }

    pcl::PointCloud<PointType>::Ptr transformPointCloud(pcl::PointCloud<PointType>::Ptr cloudIn, PointTypePose* transformIn){

        pcl::PointCloud<PointType>::Ptr cloudOut(new pcl::PointCloud<PointType>());

        PointType *pointFrom;
        PointType pointTo;

        int cloudSize = cloudIn->points.size();
        cloudOut->resize(cloudSize);
        
        for (int i = 0; i < cloudSize; ++i){

            pointFrom = &cloudIn->points[i];
            float x1 = cos(transformIn->yaw) * pointFrom->x - sin(transformIn->yaw) * pointFrom->y;
            float y1 = sin(transformIn->yaw) * pointFrom->x + cos(transformIn->yaw)* pointFrom->y;
            float z1 = pointFrom->z;

            float x2 = x1;
            float y2 = cos(transformIn->roll) * y1 - sin(transformIn->roll) * z1;
            float z2 = sin(transformIn->roll) * y1 + cos(transformIn->roll)* z1;

            pointTo.x = cos(transformIn->pitch) * x2 + sin(transformIn->pitch) * z2 + transformIn->x;
            pointTo.y = y2 + transformIn->y;
            pointTo.z = -sin(transformIn->pitch) * x2 + cos(transformIn->pitch) * z2 + transformIn->z;
            pointTo.intensity = pointFrom->intensity;

            cloudOut->points[i] = pointTo;
        }
        return cloudOut;
    }

    pcl::PointCloud<PointType>::Ptr segPointCloud(pcl::PointCloud<PointType>::Ptr cloudIn)
    {

        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());

        int obstacles_size = obstacles.size();

        for(auto p:cloudIn->points)
        {
            bool IsIn = false;
            for(int i = 0; i < obstacles_size; i++)
            {
                IsIn = obstacles[i].IsPointInBBox(p);
                if(IsIn)
                    break;
            }
            if(IsIn)
                continue;
            else
                cloudOut->points.push_back(PointType(p));
        }
        return cloudOut;
    }

    pcl::PointCloud<PointType>::Ptr lowestCloudFilter(pcl::PointCloud<PointType>::Ptr cloudIn)
    {
        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());

        PointType pt1, pt2;
        pcl::getMinMax3D(*cloudIn, pt1, pt2);
        double res = resolution;
        double minx = round(pt1.x / res) * res - res, maxx = round(pt2.x / res) * res + res;
        double miny = round(pt1.y / res) * res - res, maxy = round(pt2.y / res) * res + res;
        int xsize = round((maxx - minx) / res), ysize = round((maxy - miny) / res);
        vector<MyVoxel> voxels;
        for(double x = minx; x < maxx; x += res)
            for(double y = miny; y < maxy; y += res)
                voxels.push_back(MyVoxel(threshold_z));
            
        for(auto p:cloudIn->points)
        {
             int x = round((p.x - minx) / res), y = round((p.y - miny) / res);
             voxels[x * ysize + y].insertPoint(p);
        }

        for(auto v:voxels)
            *cloudOut += *v.getLowestPoints();
        

        return cloudOut;
    }


    

    



    

};

int main(int argc, char** argv){

    ros::init(argc, argv, "model_builder");
    
    ModelBuilder MB;

    ROS_INFO("\033[1;32m---->\033[0m Model Builder Started.");

    std::thread PublishThread(&ModelBuilder::publishThread, &MB);
    std::thread ModelThread(&ModelBuilder::modelThread, &MB);
    std::thread UDPThread(&ModelBuilder::listenUDPThread, &MB);

    ros::Rate rate(20);

    while(ros::ok())
    {
        ros::spinOnce();
        rate.sleep();
    }
    PublishThread.join();
    ModelThread.join();
    UDPThread.join();
    return 0;
}