#include <my_utility.h>

#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

class ModelBuilder{
    private:
    ros::NodeHandle nh, ng;
    ros::Subscriber subLaserCloud1, subPoseCloud1;
    ros::Subscriber subLaserCloud2, subPoseCloud2;

    ros::Publisher pubModelCloud, pubSegCloud1, pubFullCloudTransSeg1, pubSegCloud2, pubFullCloudTransSeg2, pubPoseCloud1, pubPoseCloud2;

    pcl::PointCloud<PointType>::Ptr laserCloudIn1, laserCloudIn2, modelCloud;
    pcl::PointCloud<PointTypePose>::Ptr poseCloud1, poseCloudIn1;
    pcl::PointCloud<PointTypePose>::Ptr poseCloud2, poseCloudIn2;
    pcl::PassThrough<PointType> xFilter1, zFilter1, yFilter1;
    pcl::PassThrough<PointType> xFilter2, zFilter2, yFilter2;
    vector<BoundingBox> obstacles;

    tf::StampedTransform imuOdometryTrans1;  // map与激光雷达的tf变换
    tf::TransformBroadcaster tfBroadcaster_imuOdometryTrans1;
    tf::StampedTransform imuOdometryTrans2;  // map与激光雷达的tf变换
    tf::TransformBroadcaster tfBroadcaster_imuOdometryTrans2;

    mutex poseMtx1, poseMtx2;

    double timeLaserCloudIn1;
    int laserCloudInPointerLast1;
    double laserCloudInTimeArray1[imuQueLength / 2];
    bool laserCloudInProcessed1[imuQueLength / 2];
    pcl::PointCloud<PointType>::Ptr laserCloudInArray1[imuQueLength / 2];

    int laserCloudSegPointerLast1;
    vector<pcl::PointCloud<PointType>::Ptr> laserCloudSegArray1;
    double laserCloudProcessedTime1;

    int imuPointerLast1;
    double latestImuTime1;
    double imuTime1[imuQueLength];
    float imuShiftX1[imuQueLength];
    float imuShiftY1[imuQueLength];
    float imuShiftZ1[imuQueLength];

    double timeLaserCloudIn2;
    int laserCloudInPointerLast2;
    double laserCloudInTimeArray2[imuQueLength / 2];
    bool laserCloudInProcessed2[imuQueLength / 2];
    pcl::PointCloud<PointType>::Ptr laserCloudInArray2[imuQueLength / 2];

    int laserCloudSegPointerLast2;
    vector<pcl::PointCloud<PointType>::Ptr> laserCloudSegArray2;
    double laserCloudProcessedTime2;

    int imuPointerLast2;
    double latestImuTime2;
    double imuTime2[imuQueLength];
    float imuShiftX2[imuQueLength];
    float imuShiftY2[imuQueLength];
    float imuShiftZ2[imuQueLength];

    PointTypePose CurPose1, CurPose2, LastPose1, LastPose2, nanPoint;
    pcl::VoxelGrid<PointType> downSizeFilter, segDownSizeFilter1, segDownSizeFilter2;

    string IPOUT;
    int PORTIN, PORTOUT;
    int IsSim, IsBack, IsPosCorrect, IsSeg, IsModelBuild, map_size, sor_kmean;
    double resolution, threshold_z, threshold_x, map_gap, x_offset1, x_offset2, sor_threshold;
    double min_y, max_y, min_z, max_z;





    public:
    ModelBuilder():
    nh("~"){
        timeLaserCloudIn1 = timeLaserCloudIn2 = latestImuTime1 = latestImuTime2 = -1.0;

        
        IsSim = 0;

        laserCloudInPointerLast1 = laserCloudInPointerLast2 = -1;
        for(int i = 0; i < imuQueLength / 2; i++)
        {
            laserCloudInArray1[i].reset(new pcl::PointCloud<PointType>());
            laserCloudInTimeArray1[i] = -1.0;
            laserCloudInProcessed1[i] = true;

            laserCloudInArray2[i].reset(new pcl::PointCloud<PointType>());
            laserCloudInTimeArray2[i] = -1.0;
            laserCloudInProcessed2[i] = true;
        }

        imuPointerLast1 = imuPointerLast2 = -1;
        for (int i = 0; i < imuQueLength; ++i)
        {
            imuTime1[i] = -1.0;
            imuShiftX1[i] = 0; imuShiftY1[i] = 0; imuShiftZ1[i] = 0;

            imuTime2[i] = -1.0;
            imuShiftX2[i] = 0; imuShiftY2[i] = 0; imuShiftZ2[i] = 0;
        }

        imuOdometryTrans1.frame_id_ = "/map";
        imuOdometryTrans1.child_frame_id_ = "/hesai_frame1";
        imuOdometryTrans2.frame_id_ = "/map";
        imuOdometryTrans2.child_frame_id_ = "/hesai_frame2";
        downSizeFilter.setLeafSize(0.1, 0.1, 0.1);
        segDownSizeFilter1.setLeafSize(0.1, 0.1, 0.1);
        segDownSizeFilter2.setLeafSize(0.1, 0.1, 0.1);

        ng.getParam("IPOUT", IPOUT);
        ng.getParam("PORTIN", PORTIN);
        ng.getParam("PORTOUT", PORTOUT);
        ng.getParam("IsSim", IsSim);
        ng.getParam("resolution", resolution);
        ng.getParam("threshold_z", threshold_z);
        ng.getParam("threshold_x", threshold_x);
        ng.getParam("min_z", min_z);
        ng.getParam("max_z", max_z);
        ng.getParam("min_y", min_y);
        ng.getParam("max_y", max_y);
        ng.getParam("x_offset1", x_offset1);
        ng.getParam("x_offset2", x_offset2);
        ng.getParam("map_gap", map_gap);
        ng.getParam("map_size", map_size);
        ng.getParam("sor_kmean", sor_kmean);
        ng.getParam("sor_threshold", sor_threshold);
        ng.getParam("IsBack", IsBack);
        ng.getParam("IsPosCorrect", IsPosCorrect);
        ng.getParam("IsSeg", IsSeg);
        ng.getParam("IsModelBuild", IsModelBuild);
        
        

        laserCloudSegPointerLast1 = laserCloudSegPointerLast2 = -1;
        for(int i = 0; i < map_size; i++)
        {
            laserCloudSegArray1.push_back(pcl::PointCloud<PointType>::Ptr(new pcl::PointCloud<PointType>()));
            laserCloudSegArray1[i].reset(new pcl::PointCloud<PointType>());

            laserCloudSegArray2.push_back(pcl::PointCloud<PointType>::Ptr(new pcl::PointCloud<PointType>()));
            laserCloudSegArray2[i].reset(new pcl::PointCloud<PointType>());
        }
        
        xFilter1.setFilterFieldName("x");
        xFilter1.setFilterLimits(-threshold_x, threshold_x);
        xFilter1.setFilterLimitsNegative(true);

        zFilter1.setFilterFieldName("z");
        zFilter1.setFilterLimits(min_z, max_z);  //-1.0, 8.40

        yFilter1.setFilterFieldName("y");
        yFilter1.setFilterLimits(min_y, max_y);  //0.0, 31.45

        xFilter2.setFilterFieldName("x");
        xFilter2.setFilterLimits(-threshold_x, threshold_x);
        xFilter2.setFilterLimitsNegative(true);

        zFilter2.setFilterFieldName("z");
        zFilter2.setFilterLimits(min_z, max_z);  //-1.0, 8.40

        yFilter2.setFilterFieldName("y");
        yFilter2.setFilterLimits(min_y, max_y);  //0.0, 31.45

        // string foldername = string("/home/oem/kk-mfm/front_collector/seg/");
        // FILE *fp = fopen((foldername + "bbox.txt").c_str(), "w");
        // for(int i = 1; i < 13; i++)
        // {
        //     pcl::PointCloud<PointType>::Ptr bboxPCD;
        //     bboxPCD.reset(new pcl::PointCloud<PointType>());
        //     pcl::io::loadPCDFile(foldername + to_string(i) + ".pcd", *bboxPCD);
        //     PointType pt1, pt2;
        //     pcl::getMinMax3D(*bboxPCD, pt1, pt2);
        //     fprintf(fp, "obstacles.push_back(BoundingBox(%lf, %lf, %lf, %lf, %lf, %lf));\n", i, pt1.x, pt1.y, pt1.z, pt2.x, pt2.y, pt2.z);
        // }
        // fclose(fp);


        obstacles.push_back(BoundingBox(275.55, 0, 0, 278.05, 32.0, 10.5));    // 6#库左侧墙
        obstacles.push_back(BoundingBox(200.0, 29.45, 0, 350.0, 32.0, 10.5));  // 下侧墙
        obstacles.push_back(BoundingBox(247.2, 0.0, 0.0, 248.7, 32.0, 10.5));  // 4-5#库隔断墙
        obstacles.push_back(BoundingBox(259.3, 0.0, 0.0, 260.8, 32.0, 10.5));  // 5-6#库隔断墙
        //obstacles.push_back(BoundingBox(253.6, 0.0, 0.0, 272.6, 8.7, 8.4));   // 5-6料仓台
        obstacles.push_back(BoundingBox(270.5, 0.0, 0.0, 272.6, 8.7, 10.5));   // 5-6库料仓台：竖直1
        obstacles.push_back(BoundingBox(264.6, 0.0, 0.0, 267.3, 8.7, 10.5));   // 5-6库料仓台：竖直2
        obstacles.push_back(BoundingBox(258.5, 0.0, 0.0, 261.4, 8.7, 10.5));   // 5-6库料仓台：竖直3
        obstacles.push_back(BoundingBox(253.4, 0.0, 0.0, 255.3, 8.7, 10.5));   // 5-6库料仓台：竖直4
        obstacles.push_back(BoundingBox(253.4, 0.0, 0.0, 272.6, 1.5, 10.5));   // 5-6库料仓台：水平5
        obstacles.push_back(BoundingBox(253.4, 6.3, 0.0, 272.6, 8.7, 10.5));   // 5-6库料仓台：水平6
        obstacles.push_back(BoundingBox(289.1, 0.0, 0.0, 294.1, 4.25, 10.5));   // 7#仓（平板库）左上角
        obstacles.push_back(BoundingBox(293.8, 0.0, 0.0, 350.0, 32.0, 10.5));   // 7#仓（平板库）左侧
        obstacles.push_back(BoundingBox(233.2, 23.8, 0.0, 242.1, 32.0, 10.5));   // 4仓左下角
        obstacles.push_back(BoundingBox(253.6, 26.0, 0.0, 265.8, 32.0, 10.5));   // 5-6仓下侧平台
        //obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 224.6, 8.8, 10.5));   // 4仓料仓台
        obstacles.push_back(BoundingBox(181.6, 0.0, 0.0, 182.8, 32.0, 10.5));  // 3-4#库隔断墙
        obstacles.push_back(BoundingBox(222.6, 0.0, 0.0, 224.8, 8.9, 10.5));   // 4#库料仓台：竖直1
        obstacles.push_back(BoundingBox(216.7, 0.0, 0.0, 218.8, 8.9, 10.5));   // 4#库料仓台：竖直2
        obstacles.push_back(BoundingBox(210.6, 0.0, 0.0, 213.2, 8.9, 10.5));   // 4#库料仓台：竖直3
        obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 207.0, 8.9, 10.5));   // 4#库料仓台：竖直4
        obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 224.8, 1.75, 10.5));   // 4#库料仓台：水平1
        obstacles.push_back(BoundingBox(205.5, 6.4, 0.0, 224.8, 8.9, 10.5));   // 4#库料仓台：水平2
        obstacles.push_back(BoundingBox(144.6, 0.0, 0.0, 146.5, 9.0, 10.5));   // 3#库料仓台：竖直1
        obstacles.push_back(BoundingBox(138.6, 0.0, 0.0, 141.2, 9.0, 10.5));   // 3#库料仓台：竖直2
        obstacles.push_back(BoundingBox(132.6, 0.0, 0.0, 135.2, 9.0, 10.5));   // 3#库料仓台：竖直3
        obstacles.push_back(BoundingBox(127.0, 0.0, 0.0, 129.2, 9.0, 10.5));   // 3#库料仓台：竖直4
        obstacles.push_back(BoundingBox(127.0, 0.0, 0.0, 146.5, 0.86, 10.5));   // 3#库料仓台：水平1
        obstacles.push_back(BoundingBox(127.0, 6.12, 0.0, 146.5, 9.0, 10.5));   // 3#库料仓台：水平2
        obstacles.push_back(BoundingBox(109.5, 0.0, 0.0, 110.5, 32.0, 10.5));   //2-3#库隔断墙
        obstacles.push_back(BoundingBox(45.8, 0.0, 0.0, 68.8, 10.5, 10.5));   //2#库料仓台
        obstacles.push_back(BoundingBox(55.8, 10.5, 0.0, 56.8, 32.0, 10.5));   //1-2#库隔断墙
        obstacles.push_back(BoundingBox(56.7, 26.6, 0.0, 109.7, 32.0, 10.5));   //2#库底部，因为2#库y最大值比其它库小要单独切割

        

        laserCloudIn1.reset(new pcl::PointCloud<PointType>());
        poseCloud1.reset(new pcl::PointCloud<PointTypePose>());
        poseCloudIn1.reset(new pcl::PointCloud<PointTypePose>());

        laserCloudIn2.reset(new pcl::PointCloud<PointType>());
        poseCloud2.reset(new pcl::PointCloud<PointTypePose>());
        poseCloudIn2.reset(new pcl::PointCloud<PointTypePose>());


        modelCloud.reset(new pcl::PointCloud<PointType>());


        nanPoint.x = std::numeric_limits<float>::quiet_NaN();
        nanPoint.y = std::numeric_limits<float>::quiet_NaN();
        nanPoint.z = std::numeric_limits<float>::quiet_NaN();
        nanPoint.intensity = -1;
        nanPoint.time = -1.0;

        poseCloud1->points.resize(imuQueLength);
        std::fill(poseCloud1->points.begin(), poseCloud1->points.end(), nanPoint);
        poseCloud2->points.resize(imuQueLength);
        std::fill(poseCloud2->points.begin(), poseCloud2->points.end(), nanPoint);
        

        subLaserCloud1 = nh.subscribe<sensor_msgs::PointCloud2>("/full_cloud1", 2, &ModelBuilder::laserCloudHandler1, this);
        pubSegCloud1 = nh.advertise<sensor_msgs::PointCloud2>("/seg_cloud1", 2);
        pubFullCloudTransSeg1 = nh.advertise<sensor_msgs::PointCloud2>("/full_seg_cloud1", 2);

        subLaserCloud2 = nh.subscribe<sensor_msgs::PointCloud2>("/full_cloud2", 2, &ModelBuilder::laserCloudHandler2, this);
        pubSegCloud2 = nh.advertise<sensor_msgs::PointCloud2>("/seg_cloud2", 2);
        pubFullCloudTransSeg2 = nh.advertise<sensor_msgs::PointCloud2>("/full_seg_cloud2", 2);

        pubModelCloud = nh.advertise<sensor_msgs::PointCloud2>("/model_cloud", 2);

        if(IsSim == 1)
        {
            subPoseCloud1 = nh.subscribe<sensor_msgs::PointCloud2>("/pose_cloud1", 2, &ModelBuilder::poseCloudHandler1, this);
            subPoseCloud2 = nh.subscribe<sensor_msgs::PointCloud2>("/pose_cloud2", 2, &ModelBuilder::poseCloudHandler2, this);
        }
        else
        {
            pubPoseCloud1 = nh.advertise<sensor_msgs::PointCloud2>("/pose_cloud1", 2);
            pubPoseCloud2 = nh.advertise<sensor_msgs::PointCloud2>("/pose_cloud2", 2);
        }
        
    }

    void laserCloudHandler1(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        laserCloudIn1.reset(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(*laserCloudMsg, *laserCloudIn1);
        timeLaserCloudIn1 = laserCloudMsg->header.stamp.toSec();
        laserCloudInPointerLast1 = (laserCloudInPointerLast1 + 1) % (imuQueLength / 2);
        laserCloudInArray1[laserCloudInPointerLast1].reset(new pcl::PointCloud<PointType>());
        *laserCloudInArray1[laserCloudInPointerLast1] += *laserCloudIn1;
        laserCloudInTimeArray1[laserCloudInPointerLast1] = timeLaserCloudIn1;
        laserCloudInProcessed1[laserCloudInPointerLast1] = false;
        // ROS_INFO("laserIn Time:%lf", timeLaserCloudIn);
    }

    void laserCloudHandler2(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        laserCloudIn2.reset(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(*laserCloudMsg, *laserCloudIn2);
        timeLaserCloudIn2 = laserCloudMsg->header.stamp.toSec();
        laserCloudInPointerLast2 = (laserCloudInPointerLast2 + 1) % (imuQueLength / 2);
        laserCloudInArray2[laserCloudInPointerLast2].reset(new pcl::PointCloud<PointType>());
        *laserCloudInArray2[laserCloudInPointerLast2] += *laserCloudIn2;
        laserCloudInTimeArray2[laserCloudInPointerLast2] = timeLaserCloudIn2;
        laserCloudInProcessed2[laserCloudInPointerLast2] = false;
        // ROS_INFO("laserIn Time:%lf", timeLaserCloudIn);
    }

    static bool cmp(pair<double, double> a, pair<double, double> b)
    {
        return a.first < b.first;
    }

    void poseCloudHandler1(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        poseCloudIn1.reset(new pcl::PointCloud<PointTypePose>());
        pcl::fromROSMsg(*laserCloudMsg, *poseCloudIn1);
        vector<pair<double, double>> TimeShiftXTemp;
        double latestImuTimeTemp = latestImuTime1;
        for(int i = 0; i < imuQueLength; i++)
        {
            PointTypePose p = poseCloudIn1->points[i];
            if(p.time <= 0.0)
                continue;
            double ShiftXTemp = IsBack == 1 ? 250 - p.x : p.x;   // TO BE CORRECTED
            double TimeTemp = p.time;
            TimeShiftXTemp.push_back(make_pair(TimeTemp, ShiftXTemp));
            if(latestImuTime1 < TimeTemp)
            {
                latestImuTimeTemp = TimeTemp;
                ROS_INFO("imu1: Time:%lf, ShiftX:%lf", TimeTemp, ShiftXTemp);
            }
        }
        latestImuTime1 = latestImuTimeTemp;
        sort(TimeShiftXTemp.begin(), TimeShiftXTemp.end(), ModelBuilder::cmp);

        // interpolate
        double var = TimeShiftXTemp[0].second, time = 0.0;
        for(int i = 1; i < TimeShiftXTemp.size(); i++)
        {
            if(abs(TimeShiftXTemp[i].second - var) > 2.0)
                TimeShiftXTemp[i].first = -1.0;
            // else if(TimeShiftXTemp[i].second == var)
            //     TimeShiftXTemp[i].first =  IsPosCorrect == 1 ? TimeShiftXTemp[i].first : -1.0;
            else
            {
                var = TimeShiftXTemp[i].second;
                time = TimeShiftXTemp[i].first;
            }
        }

        poseMtx1.lock();
        int error_count = 0;
        for(int i = 0; i < TimeShiftXTemp.size(); i++)
        {
            imuTime1[i] = TimeShiftXTemp[i].first;
            imuShiftX1[i] = TimeShiftXTemp[i].second;
        }
        imuPointerLast1 = TimeShiftXTemp.size() - 1;
        poseMtx1.unlock();


        CurPose1.x = var + x_offset1;
        CurPose1.y = 23.68;
        CurPose1.time = time;
        ROS_INFO("CurPose1:%lf, %lf", CurPose1.x, CurPose1.time);

        geometry_msgs::Quaternion geoQuat = tf::createQuaternionMsgFromRollPitchYaw(0.0, 0.0, PI);
        imuOdometryTrans1.stamp_ = ros::Time().fromSec(CurPose1.time); //ros::Time::now();  //
        imuOdometryTrans1.setRotation(tf::Quaternion(geoQuat.x, geoQuat.y, geoQuat.z, geoQuat.w));
        imuOdometryTrans1.setOrigin(tf::Vector3(CurPose1.x, CurPose1.y, 0.0));
        tfBroadcaster_imuOdometryTrans1.sendTransform(imuOdometryTrans1);

    }

    void poseCloudHandler2(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        poseCloudIn2.reset(new pcl::PointCloud<PointTypePose>());
        pcl::fromROSMsg(*laserCloudMsg, *poseCloudIn2);
        vector<pair<double, double>> TimeShiftXTemp;
        double latestImuTimeTemp = latestImuTime2;
        for(int i = 0; i < imuQueLength; i++)
        {
            PointTypePose p = poseCloudIn2->points[i];
            if(p.time <= 0.0)
                continue;
            double ShiftXTemp = IsBack == 1 ? 250 - p.x : p.x;   // TO BE CORRECTED
            double TimeTemp = p.time;
            TimeShiftXTemp.push_back(make_pair(TimeTemp, ShiftXTemp));
            if(latestImuTime2 < TimeTemp)
            {
                latestImuTimeTemp = TimeTemp;
                //ROS_INFO("imuTime:%lf, imuShiftX:%lf", TimeTemp, ShiftXTemp);
            }
        }
        latestImuTime2 = latestImuTimeTemp;
        sort(TimeShiftXTemp.begin(), TimeShiftXTemp.end(), ModelBuilder::cmp);

        // interpolate
        double var = TimeShiftXTemp[0].second, time = 0.0;
        for(int i = 1; i < TimeShiftXTemp.size(); i++)
        {
            if(abs(TimeShiftXTemp[i].second - var) > 2.0)
                TimeShiftXTemp[i].first = -1.0;
            // else if(TimeShiftXTemp[i].second == var)
            //     TimeShiftXTemp[i].first =  IsPosCorrect == 1 ? TimeShiftXTemp[i].first : -1.0;
            else
            {
                var = TimeShiftXTemp[i].second;
                time = TimeShiftXTemp[i].first;
            }
        }

        poseMtx2.lock();
        int error_count = 0;
        for(int i = 0; i < TimeShiftXTemp.size(); i++)
        {
            imuTime2[i] = TimeShiftXTemp[i].first;
            imuShiftX2[i] = TimeShiftXTemp[i].second;
        }
        imuPointerLast2 = TimeShiftXTemp.size() - 1;
        poseMtx2.unlock();


        CurPose2.x = var + x_offset2;
        CurPose2.y = 23.68;
        CurPose2.time = time;
        ROS_INFO("CurPose2:%lf, %lf", CurPose2.x, CurPose2.time);

        geometry_msgs::Quaternion geoQuat = tf::createQuaternionMsgFromRollPitchYaw(0.0, 0.0, PI);
        imuOdometryTrans2.stamp_ = ros::Time().fromSec(CurPose2.time); //ros::Time::now();  //
        imuOdometryTrans2.setRotation(tf::Quaternion(geoQuat.x, geoQuat.y, geoQuat.z, geoQuat.w));
        imuOdometryTrans2.setOrigin(tf::Vector3(CurPose2.x, CurPose2.y, 0.0));
        tfBroadcaster_imuOdometryTrans2.sendTransform(imuOdometryTrans2);
    }

    
    unsigned char getCheckCode(unsigned char *sendBuf, int dataLen)
    {
        unsigned char checkCode = 0;
        for(int i = 0; i < dataLen; i++)
            checkCode += sendBuf[i];
        return checkCode;
    }

    void listenUDPThread()
    {
        if(IsSim == 1)
            return;
        int sock_fd;
        sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
        if(sock_fd < 0)
        {
            ROS_INFO("SOCKET ERROR");
            return;
        }
        else
            ROS_INFO("SOCKET SUCCESS");
        fcntl(sock_fd, F_SETFL, fcntl(sock_fd, F_GETFL, 0) | O_NONBLOCK);

        struct sockaddr_in addr_serv;
        int len;
        memset(&addr_serv, 0, sizeof(addr_serv));
        addr_serv.sin_family = AF_INET;
        addr_serv.sin_addr.s_addr = htonl(INADDR_ANY);
        addr_serv.sin_port = htons(PORTIN);
        len = sizeof(addr_serv);

        if(bind(sock_fd, (struct sockaddr *)&addr_serv, sizeof(addr_serv)) < 0)  
        {  
            ROS_INFO("BIND ERROR");
            return; 
        }  


        int recv_num;
        unsigned char recv_buf[85];
        ros::Rate rate(20);
        ROS_INFO("BEGIN LISTEN");
        while(ros::ok())
        {
            recv_num = recvfrom(sock_fd, recv_buf, 85, 0, (struct sockaddr *)&addr_serv, (socklen_t *)&len);
            // if(recv_num < 0)
            // {
            //     ROS_INFO("RECVFROM ERROR");
            // }
            // else
            //ROS_INFO("recv_num: %d", recv_num);

            if(recv_buf[0] == 0xA4)
            {
                if(getCheckCode(recv_buf, 84) != recv_buf[84])
                {
                    rate.sleep();
                    continue;
                }
                
                double CurTime = ros::Time::now().toSec();
                uint32_t position;
                position = recv_buf[11] << 24 | recv_buf[12] << 16 | recv_buf[13] << 8 | recv_buf[14];
                double ShiftXTemp = position * 0.01;

                if((ShiftXTemp > 330.0 || ShiftXTemp < 0.0) && imuPointerLast1 != -1)  //abs(ShiftXTemp - imuShiftX1[imuPointerLast1]) > 2.0
                {
                    ROS_ERROR("IMU1: Time:%lf, ShiftX:%lf", CurTime, ShiftXTemp);
                    rate.sleep();
                    continue;
                }

                poseMtx1.lock();
                int imuPointerLastTemp = (imuPointerLast1 + 1) % imuQueLength;
                imuShiftX1[imuPointerLastTemp] = ShiftXTemp;
                imuTime1[imuPointerLastTemp] = CurTime;
                imuPointerLast1 = imuPointerLastTemp;
                poseMtx1.unlock();

                ROS_INFO("IMU1: Time:%lf, ShiftX:%lf", CurTime, ShiftXTemp);

                CurPose1.x = ShiftXTemp + x_offset1;
                CurPose1.y = 23.68;
                CurPose1.time = CurTime;

                geometry_msgs::Quaternion geoQuat = tf::createQuaternionMsgFromRollPitchYaw(0.0, 0.0, PI);
                imuOdometryTrans1.stamp_ = ros::Time().fromSec(CurPose1.time); //ros::Time::now();  //
                imuOdometryTrans1.setRotation(tf::Quaternion(geoQuat.x, geoQuat.y, geoQuat.z, geoQuat.w));
                imuOdometryTrans1.setOrigin(tf::Vector3(CurPose1.x, CurPose1.y, 0.0));
                tfBroadcaster_imuOdometryTrans1.sendTransform(imuOdometryTrans1);

                if(IsSim != 1)
                {
                    for(int i = 0; i < imuQueLength; i++)
                    {
                        PointTypePose pose1;
                        pose1.x = imuShiftX1[i];
                        pose1.y = 0;
                        pose1.z = 0;
                        pose1.time = imuTime1[i];
                        poseCloud1->points[i] = pose1;
                    }

                    sensor_msgs::PointCloud2 laserCloudTemp3;
                    pcl::toROSMsg(*poseCloud1, laserCloudTemp3);
                    laserCloudTemp3.header.stamp = ros::Time::now();
                    laserCloudTemp3.header.frame_id = "/map";
                    pubPoseCloud1.publish(laserCloudTemp3);
                }
            }
            // else
            //     ROS_INFO("RECVFROM DATA ERROR");
            if(recv_buf[0] == 0xA3)
            {
                if(getCheckCode(recv_buf, 84) != recv_buf[84])
                {
                    rate.sleep();
                    continue;
                }
                
                double CurTime = ros::Time::now().toSec();
                uint32_t position;
                position = recv_buf[11] << 24 | recv_buf[12] << 16 | recv_buf[13] << 8 | recv_buf[14];
                double ShiftXTemp = position * 0.01;

                if((ShiftXTemp > 330.0 || ShiftXTemp < 0.0) && imuPointerLast2 != -1)  //abs(ShiftXTemp - imuShiftX1[imuPointerLast2]) > 2.0
                {
                    ROS_ERROR("IMU2: Time:%lf, ShiftX:%lf", CurTime, ShiftXTemp);
                    rate.sleep();
                    continue;
                }

                poseMtx2.lock();
                int imuPointerLastTemp = (imuPointerLast2 + 1) % imuQueLength;
                imuShiftX2[imuPointerLastTemp] = ShiftXTemp;
                imuTime2[imuPointerLastTemp] = CurTime;
                imuPointerLast2 = imuPointerLastTemp;
                poseMtx2.unlock();

                ROS_INFO("IMU2: Time:%lf, ShiftX:%lf", CurTime, ShiftXTemp);

                CurPose2.x = ShiftXTemp + x_offset2;
                CurPose2.y = 23.68;
                CurPose2.time = CurTime;

                geometry_msgs::Quaternion geoQuat = tf::createQuaternionMsgFromRollPitchYaw(0.0, 0.0, PI);
                imuOdometryTrans2.stamp_ = ros::Time().fromSec(CurPose2.time); //ros::Time::now();  //
                imuOdometryTrans2.setRotation(tf::Quaternion(geoQuat.x, geoQuat.y, geoQuat.z, geoQuat.w));
                imuOdometryTrans2.setOrigin(tf::Vector3(CurPose2.x, CurPose2.y, 0.0));
                tfBroadcaster_imuOdometryTrans2.sendTransform(imuOdometryTrans2);

                if(IsSim != 1)
                {
                    for(int i = 0; i < imuQueLength; i++)
                    {
                        PointTypePose pose2;

                        pose2.x = imuShiftX2[i];
                        pose2.y = 0;
                        pose2.z = 0;
                        pose2.time = imuTime2[i];
                        poseCloud2->points[i] = pose2;
                    }

                    sensor_msgs::PointCloud2 laserCloudTemp4;
                    pcl::toROSMsg(*poseCloud2, laserCloudTemp4);
                    laserCloudTemp4.header.stamp = ros::Time::now();
                    laserCloudTemp4.header.frame_id = "/map";
                    pubPoseCloud2.publish(laserCloudTemp4);
                }
            }
            


            
            rate.sleep();
        }
    }

    void modelThread1()
    {
        ros::Rate rate(10);
        double laserCloudInProcessedTimeLast =0.0;
        while(ros::ok())
        {
            if(laserCloudInPointerLast1 == -1 || imuPointerLast1 == -1 )
            {
                rate.sleep();
                continue;
            }

            double time_begin = ros::Time::now().toSec();

            double imuTimeTempArray[imuQueLength], imuShiftXTempArray[imuQueLength];
            poseMtx1.lock();
            for(int i = 0; i < imuQueLength; i++)
            {
                imuShiftXTempArray[i] = imuShiftX1[i];
                imuTimeTempArray[i] = imuTime1[i];
            }
            poseMtx1.unlock();
            
            
            int laserCloudInProcessingPointer = -1;
            int imuProcessingPointer = -1;
            double minTimeDiff = 0.10;
            for(int i = 0; i < imuQueLength / 2; i++)
            {
                bool laserCloudInProcessedTemp = laserCloudInProcessed1[i];
                double laserCloudInTimeTemp = laserCloudInTimeArray1[i];

                if(laserCloudInProcessedTemp == true || laserCloudInTimeTemp == -1.0 || laserCloudInTimeTemp < laserCloudInProcessedTimeLast)
                    continue;
                
                
                for(int j = 0; j < imuQueLength; j++)
                {
                    double imuTimeTemp = imuTimeTempArray[j];
                    if(imuTimeTemp == -1.0)
                        continue;
                    double timeDiffTemp = abs(laserCloudInTimeTemp - imuTimeTemp);
                    if(timeDiffTemp < minTimeDiff)
                    {
                        minTimeDiff = timeDiffTemp;
                        laserCloudInProcessingPointer = i;
                        imuProcessingPointer = j;
                    }
                }
            }

            //ROS_INFO("find laser-imu pair at index: %d - %d", laserCloudInProcessingPointer, imuProcessingPointer);
            if(laserCloudInProcessingPointer == -1 || imuProcessingPointer == -1 || minTimeDiff > 0.06)
            {
                rate.sleep();
                continue;
            }
            laserCloudInProcessed1[laserCloudInProcessingPointer] = true;
            laserCloudInProcessedTimeLast = laserCloudInTimeArray1[laserCloudInProcessingPointer];

            PointTypePose pose;
            pose.x = imuShiftXTempArray[imuProcessingPointer] + x_offset1;
            pose.y = 23.68;
            pose.z = 0;
            pose.roll = 0;
            pose.pitch = 0;
            pose.yaw = PI;

            double ori = pose.x - LastPose1.x;
            double laserCloudInTimeProcessing = laserCloudInTimeArray1[laserCloudInProcessingPointer];
            double time_pass = laserCloudInTimeProcessing - laserCloudProcessedTime1;

            if(abs(ori) < map_gap && abs(time_pass) < 0.2)
            {
                rate.sleep();
                continue;
            }
            LastPose1 = pose;
            laserCloudProcessedTime1 = laserCloudInTimeProcessing;


            ROS_INFO("Laser1:%lf, IMU1:%lf, %lf", 
            laserCloudInTimeProcessing, imuTimeTempArray[imuProcessingPointer], imuShiftXTempArray[imuProcessingPointer]);

            
            pcl::PointCloud<PointType>::Ptr  fullCloudTempPassX, fullCloudTempPassY, fullCloudTempPassZ, fullCloudTrans, fullCloudTransSegOut, laserCloudSegTemp, laserCloudSegTempSOR, laserCloudSegTempDS;
            fullCloudTempPassX.reset(new pcl::PointCloud<PointType>());
            fullCloudTempPassY.reset(new pcl::PointCloud<PointType>());
            fullCloudTempPassZ.reset(new pcl::PointCloud<PointType>());
            fullCloudTrans.reset(new pcl::PointCloud<PointType>());
            fullCloudTransSegOut.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTemp.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTempSOR.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTempDS.reset(new pcl::PointCloud<PointType>());

            xFilter1.setInputCloud(laserCloudInArray1[laserCloudInProcessingPointer]);
            xFilter1.filter(*fullCloudTempPassX);

            *fullCloudTrans +=  *transformPointCloud(fullCloudTempPassX, &pose) ; 
            zFilter1.setInputCloud(fullCloudTrans);
            zFilter1.filter(*fullCloudTempPassZ);
            yFilter1.setInputCloud(fullCloudTempPassZ);
            yFilter1.filter(*fullCloudTempPassY);

            //*fullCloudTransSegOut += *segPointCloud( fullCloudTempPassY );
            *laserCloudSegTemp += * lowestCloudFilter ( segPointCloud( fullCloudTempPassY ) ) ;//*lowestCloudFilter( segPointCloud( fullCloudTempPassY ) );
            
            pcl::StatisticalOutlierRemoval<PointType> sor;
            sor.setInputCloud(laserCloudSegTemp);
            sor.setMeanK(sor_kmean);
            sor.setStddevMulThresh(sor_threshold);
            sor.filter(*laserCloudSegTempSOR);

            size_t cloudSize = laserCloudSegTempSOR->size();
            for(int i = 0; i < cloudSize; i++)
            {
                PointType *p = & laserCloudSegTempSOR->points[i];
                p->intensity = 100;
            } 

            segDownSizeFilter1.setInputCloud(laserCloudSegTempSOR);
            segDownSizeFilter1.filter(*laserCloudSegTempDS);


            laserCloudSegPointerLast1 = (laserCloudSegPointerLast1 + 1) % map_size;
            laserCloudSegArray1[laserCloudSegPointerLast1].reset(new pcl::PointCloud<PointType>());
            *laserCloudSegArray1[laserCloudSegPointerLast1] += IsSeg == 1 ? *laserCloudSegTempDS : *fullCloudTempPassY;


            sensor_msgs::PointCloud2 laserCloudTemp1;
            pcl::toROSMsg(*laserCloudSegTempSOR, laserCloudTemp1);
            laserCloudTemp1.header.stamp = ros::Time::now();
            laserCloudTemp1.header.frame_id = "/map";
            pubSegCloud1.publish(laserCloudTemp1);

            sensor_msgs::PointCloud2 fullCloudTransSeg;
            pcl::toROSMsg(*fullCloudTempPassY, fullCloudTransSeg);
            fullCloudTransSeg.header.stamp = ros::Time::now();
            fullCloudTransSeg.header.frame_id = "/map";
            pubFullCloudTransSeg1.publish(fullCloudTransSeg);

            rate.sleep();
        }
    }

    void modelThread2()
    {
        ros::Rate rate(10);
        double laserCloudInProcessedTimeLast =0.0;
        while(ros::ok())
        {
            if(laserCloudInPointerLast2 == -1 || imuPointerLast2 == -1 )
            {
                rate.sleep();
                continue;
            }

            double time_begin = ros::Time::now().toSec();

            double imuTimeTempArray[imuQueLength], imuShiftXTempArray[imuQueLength];
            poseMtx2.lock();
            for(int i = 0; i < imuQueLength; i++)
            {
                imuShiftXTempArray[i] = imuShiftX2[i];
                imuTimeTempArray[i] = imuTime2[i];
            }
            poseMtx2.unlock();
            
            
            int laserCloudInProcessingPointer = -1;
            int imuProcessingPointer = -1;
            double minTimeDiff = 0.10;
            for(int i = 0; i < imuQueLength / 2; i++)
            {
                bool laserCloudInProcessedTemp = laserCloudInProcessed2[i];
                double laserCloudInTimeTemp = laserCloudInTimeArray2[i];

                if(laserCloudInProcessedTemp == true || laserCloudInTimeTemp == -1.0 || laserCloudInTimeTemp < laserCloudInProcessedTimeLast)
                    continue;
                
                
                for(int j = 0; j < imuQueLength; j++)
                {
                    double imuTimeTemp = imuTimeTempArray[j];
                    if(imuTimeTemp == -1.0)
                        continue;
                    double timeDiffTemp = abs(laserCloudInTimeTemp - imuTimeTemp);
                    if(timeDiffTemp < minTimeDiff)
                    {
                        minTimeDiff = timeDiffTemp;
                        laserCloudInProcessingPointer = i;
                        imuProcessingPointer = j;
                    }
                }
            }

            //ROS_INFO("find laser-imu pair at index: %d - %d", laserCloudInProcessingPointer, imuProcessingPointer);
            if(laserCloudInProcessingPointer == -1 || imuProcessingPointer == -1 || minTimeDiff > 0.06)
            {
                rate.sleep();
                continue;
            }
            laserCloudInProcessed2[laserCloudInProcessingPointer] = true;
            laserCloudInProcessedTimeLast = laserCloudInTimeArray2[laserCloudInProcessingPointer];

            PointTypePose pose;
            pose.x = imuShiftXTempArray[imuProcessingPointer] + x_offset2;
            pose.y = 23.68;
            pose.z = 0;
            pose.roll = 0;
            pose.pitch = 0;
            pose.yaw = PI;

            double ori = pose.x - LastPose2.x;
            double laserCloudInTimeProcessing = laserCloudInTimeArray2[laserCloudInProcessingPointer];
            double time_pass = laserCloudInTimeProcessing - laserCloudProcessedTime2;

            if(abs(ori) < map_gap && abs(time_pass) < 0.2)
            {
                rate.sleep();
                continue;
            }
            LastPose2 = pose;
            laserCloudProcessedTime2 = laserCloudInTimeProcessing;


            ROS_INFO("Laser2:%lf, IMU2:%lf, %lf", 
            laserCloudInTimeProcessing, imuTimeTempArray[imuProcessingPointer], imuShiftXTempArray[imuProcessingPointer]);

            
            pcl::PointCloud<PointType>::Ptr  fullCloudTempPassX, fullCloudTempPassY, fullCloudTempPassZ, fullCloudTrans, fullCloudTransSegOut, laserCloudSegTemp, laserCloudSegTempSOR, laserCloudSegTempDS;
            fullCloudTempPassX.reset(new pcl::PointCloud<PointType>());
            fullCloudTempPassY.reset(new pcl::PointCloud<PointType>());
            fullCloudTempPassZ.reset(new pcl::PointCloud<PointType>());
            fullCloudTrans.reset(new pcl::PointCloud<PointType>());
            fullCloudTransSegOut.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTemp.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTempSOR.reset(new pcl::PointCloud<PointType>());
            laserCloudSegTempDS.reset(new pcl::PointCloud<PointType>());

            xFilter2.setInputCloud(laserCloudInArray2[laserCloudInProcessingPointer]);
            xFilter2.filter(*fullCloudTempPassX);

            *fullCloudTrans +=  *transformPointCloud(fullCloudTempPassX, &pose) ; 
            zFilter2.setInputCloud(fullCloudTrans);
            zFilter2.filter(*fullCloudTempPassZ);
            yFilter2.setInputCloud(fullCloudTempPassZ);
            yFilter2.filter(*fullCloudTempPassY);

            
            //*fullCloudTransSegOut += *segPointCloud( fullCloudTempPassY );
            *laserCloudSegTemp += * lowestCloudFilter ( segPointCloud( fullCloudTempPassY ) ) ;//*lowestCloudFilter( segPointCloud( fullCloudTempPassY ) );
            //*laserCloudSegTemp += *fullCloudTrans;
            pcl::StatisticalOutlierRemoval<PointType> sor;
            sor.setInputCloud(laserCloudSegTemp);
            sor.setMeanK(sor_kmean);
            sor.setStddevMulThresh(sor_threshold);
            sor.filter(*laserCloudSegTempSOR);

            size_t cloudSize = laserCloudSegTempSOR->size();
            for(int i = 0; i < cloudSize; i++)
            {
                PointType *p = & laserCloudSegTempSOR->points[i];
                p->intensity = 100;
            } 

            segDownSizeFilter2.setInputCloud(laserCloudSegTempSOR);
            segDownSizeFilter2.filter(*laserCloudSegTempDS);


            laserCloudSegPointerLast2 = (laserCloudSegPointerLast2 + 1) % map_size;
            laserCloudSegArray2[laserCloudSegPointerLast2].reset(new pcl::PointCloud<PointType>());
            *laserCloudSegArray2[laserCloudSegPointerLast2] += IsSeg == 1 ? *laserCloudSegTempDS : *fullCloudTempPassY;


            sensor_msgs::PointCloud2 laserCloudTemp2;
            pcl::toROSMsg(*laserCloudSegTempSOR, laserCloudTemp2);
            laserCloudTemp2.header.stamp = ros::Time::now();
            laserCloudTemp2.header.frame_id = "/map";
            pubSegCloud2.publish(laserCloudTemp2);

            sensor_msgs::PointCloud2 fullCloudTransSeg;
            pcl::toROSMsg(*fullCloudTempPassY, fullCloudTransSeg);
            fullCloudTransSeg.header.stamp = ros::Time::now();
            fullCloudTransSeg.header.frame_id = "/map";
            pubFullCloudTransSeg2.publish(fullCloudTransSeg);

            rate.sleep();
        }
    }

    void publishThread()
    {
        ros::Rate rate(10);
        while(ros::ok())
        {
            
            rate.sleep();

        }
        modelCloud.reset(new pcl::PointCloud<PointType>());
        if(IsModelBuild == 1)
        {
            for(int i = 0; i < map_size; i++)
            {
                pcl::PointCloud<PointType>::Ptr laser1 = laserCloudSegArray1[i];
                pcl::PointCloud<PointType>::Ptr laser2 = laserCloudSegArray2[i];
                
                if(!laser1->empty())
                    *modelCloud += *laser1;
                if(!laser2->empty())
                    *modelCloud += *laser2;
            }
        }
        
        // pcl::PointCloud<PointType>::Ptr modelCloudDS;
        // modelCloudDS.reset(new pcl::PointCloud<PointType>());
        // downSizeFilter.setInputCloud(modelCloud);
        // downSizeFilter.filter(*modelCloudDS);
        //pcl::io::savePCDFileBinary("/home/zxk/kk-mfm/modelCloud.pcd", *modelCloud);
        pcl::io::savePCDFileBinary("/home/jj/kk-mfm-master/modelCloud.pcd", *modelCloud);
    }
    

    pcl::PointCloud<PointType>::Ptr transformPointCloud(pcl::PointCloud<PointType>::Ptr cloudIn, PointTypePose* transformIn){

        pcl::PointCloud<PointType>::Ptr cloudOut(new pcl::PointCloud<PointType>());

        PointType *pointFrom;
        PointType pointTo;

        int cloudSize = cloudIn->points.size();
        cloudOut->resize(cloudSize);
        
        for (int i = 0; i < cloudSize; ++i){

            pointFrom = &cloudIn->points[i];
            float x1 = cos(transformIn->yaw) * pointFrom->x - sin(transformIn->yaw) * pointFrom->y;
            float y1 = sin(transformIn->yaw) * pointFrom->x + cos(transformIn->yaw)* pointFrom->y;
            float z1 = pointFrom->z;

            float x2 = x1;
            float y2 = cos(transformIn->roll) * y1 - sin(transformIn->roll) * z1;
            float z2 = sin(transformIn->roll) * y1 + cos(transformIn->roll)* z1;

            pointTo.x = cos(transformIn->pitch) * x2 + sin(transformIn->pitch) * z2 + transformIn->x;
            pointTo.y = y2 + transformIn->y;
            pointTo.z = -sin(transformIn->pitch) * x2 + cos(transformIn->pitch) * z2 + transformIn->z;
            pointTo.intensity = pointFrom->intensity;

            cloudOut->points[i] = pointTo;
        }
        return cloudOut;
    }

    pcl::PointCloud<PointType>::Ptr segPointCloud(pcl::PointCloud<PointType>::Ptr cloudIn)
    {
        if(IsSeg == 0)
            return cloudIn;
        
        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());

        int obstacles_size = obstacles.size();

        for(auto p:cloudIn->points)
        {
            bool IsIn = false;
            for(int i = 0; i < obstacles_size; i++)
            {
                IsIn = obstacles[i].IsPointInBBox(p);
                if(IsIn)
                    break;
            }
            if(IsIn)
                continue;
            else
                cloudOut->points.push_back(PointType(p));
        }
        return cloudOut;
    }

    pcl::PointCloud<PointType>::Ptr lowestCloudFilter(pcl::PointCloud<PointType>::Ptr cloudIn)
    {
        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());

        PointType pt1, pt2;
        pcl::getMinMax3D(*cloudIn, pt1, pt2);
        double res = resolution;
        double minx = round(pt1.x / res) * res - res, maxx = round(pt2.x / res) * res + res;
        double miny = round(pt1.y / res) * res - res, maxy = round(pt2.y / res) * res + res;
        int xsize = round((maxx - minx) / res), ysize = round((maxy - miny) / res);
        vector<MyVoxel> voxels;
        for(double x = minx; x < maxx; x += res)
            for(double y = miny; y < maxy; y += res)
                voxels.push_back(MyVoxel(threshold_z));
            
        for(auto p:cloudIn->points)
        {
             int x = round((p.x - minx) / res), y = round((p.y - miny) / res);
             voxels[x * ysize + y].insertPoint(p);
        }

        for(auto v:voxels)
            *cloudOut += *v.getLowestPoints();
        

        return cloudOut;
    }


    

    



    

};

int main(int argc, char** argv){

    ros::init(argc, argv, "model_builder");
    
    ModelBuilder MB;

    ROS_INFO("\033[1;32m---->\033[0m Model Builder Started.");

    std::thread UDPThread(&ModelBuilder::listenUDPThread, &MB);
    std::thread ModelThread1(&ModelBuilder::modelThread1, &MB);
    std::thread ModelThread2(&ModelBuilder::modelThread2, &MB);
    std::thread PublishThread(&ModelBuilder::publishThread, &MB);

    ros::Rate rate(20);

    while(ros::ok())
    {
        ros::spinOnce();
        rate.sleep();
    }
    PublishThread.join();
    ModelThread1.join();
    ModelThread2.join();
    UDPThread.join();
    return 0;
}