#include <my_utility.h>

#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

class ObjectEntity{
    public:
    int type;
    PointType center;
    PointType range;
    pcl::PointCloud<PointType>::Ptr points;
    ObjectEntity(int type) { this-> type = type; points.reset(new pcl::PointCloud<PointType>()); };
    ObjectEntity(const ObjectEntity &oe) {
        type = oe.type;
        center = PointType(oe.center);
        range = PointType(oe.range);
        points.reset(new pcl::PointCloud<PointType>()); 
    };

    bool IsBetweenTwoValue(double tmp, double min, double max)
    {
        return tmp > min && tmp < max;
    }

    bool IsPointNear(PointType pt)
    {
        for(auto p:points->points)
        {
            if(IsBetweenTwoValue(p.x - pt.x, -0.2, 0.2)
            && IsBetweenTwoValue(p.y - pt.y, -0.2, 0.2)
            && IsBetweenTwoValue(p.z - pt.z, -0.2, 0.2))
            return true;
        }

        if(IsBetweenTwoValue(center.x - pt.x, -0.8 * range.x, 0.8 * range.x)
        && IsBetweenTwoValue(center.y - pt.y, -0.8 * range.y, 0.8 * range.y)
        && IsBetweenTwoValue(center.z - pt.z, -0.8 * range.z, 0.8 * range.z))
            return true;
        
        return false;
    };
};

class ObjectDetect{
    private:
    ros::NodeHandle nh, ng;
    ros::Subscriber subLaserCloud1, subLaserCloud2;

    std::mutex machine_pose_mtx[5], object_pose_mtx[5];

    ros::Publisher  pubSegCloud[5];

    ros::Publisher pubNoGroundForPerson[5], pubNoGroundForVehicle[5];

    pcl::PointCloud<PointType>::Ptr laserCloudIn[5];

    vector<BoundingBox> obstacles;


    
    double Detector_To_Obstacle_Dist; // for cut obstacles
    
    double DetectorPersonThresholdZ, DetectorPersonResolution;  // for voxel filter
    int DetectorPerson_ECE_Min; // ECE cluster min num
    double DetectorPerson_ECE_Tolerance; // ECE cluster tolerance
    double DetectorPerson_MinXY, DetectorPerson_MaxXY;  // for distinguish person or vehicle or others

    double DetectorVehicleThresholdZ, DetectorVehicleResolution;  // for voxel filter
    int DetectorVehicle_ECE_Min; // ECE cluster min num
    double DetectorVehicle_ECE_Tolerance; // ECE cluster tolerance
    double DetectorVehicle_MinXY, DetectorVehicle_MaxXY;  // for distinguish person or vehicle or others


    int UDPNUM;
    vector<string> UDPs;
    double time_send_udp;
    vector<vector<ObjectEntity>*> objects;
    double x_pos_cur[5];

    
    public:
    ObjectDetect():
    nh("~"){

        
        ng.getParam("Detector_To_Obstacle_Dist", Detector_To_Obstacle_Dist);

        ng.getParam("DetectorPersonThresholdZ", DetectorPersonThresholdZ);
        ng.getParam("DetectorPersonResolution", DetectorPersonResolution);
        ng.getParam("DetectorPerson_ECE_Tolerance", DetectorPerson_ECE_Tolerance);
        ng.getParam("DetectorPerson_ECE_Min", DetectorPerson_ECE_Min);
        ng.getParam("DetectorPerson_MinXY", DetectorPerson_MinXY);
        ng.getParam("DetectorPerson_MaxXY", DetectorPerson_MaxXY);

        ng.getParam("DetectorVehicleThresholdZ", DetectorVehicleThresholdZ);
        ng.getParam("DetectorVehicleResolution", DetectorVehicleResolution);
        ng.getParam("DetectorVehicle_ECE_Tolerance", DetectorVehicle_ECE_Tolerance);
        ng.getParam("DetectorVehicle_ECE_Min", DetectorVehicle_ECE_Min);
        ng.getParam("DetectorVehicle_MinXY", DetectorVehicle_MinXY);
        ng.getParam("DetectorVehicle_MaxXY", DetectorVehicle_MaxXY);

        ng.getParam("UDPNUM", UDPNUM);
        for(int i = 1; i <= UDPNUM; i++)
        {
            stringstream ss;
            ss << "UDP" << i;
            string tmp;
            ng.getParam(ss.str(), tmp);
            UDPs.push_back(tmp);
        }
        for(int i = 0; i < 5; i ++)
        {
            x_pos_cur[i] = -1.0;
            vector<ObjectEntity>* tmp = new vector<ObjectEntity>();
            objects.push_back(tmp);
        }
        time_send_udp = 0.0;
        

        obstacles.push_back(BoundingBox(275.55, 0, 0, 278.05, 32.0, 10.5));    // 6仓左侧墙
        obstacles.push_back(BoundingBox(200.0, 29.45, 0, 350.0, 32.0, 10.5));  // 下侧墙
        obstacles.push_back(BoundingBox(247.2, 0.0, 0.0, 248.7, 32.0, 10.5));  // 4-5隔断墙
        obstacles.push_back(BoundingBox(259.0, 0.0, 0.0, 260.7, 32.0, 10.5));  // 5-6隔断墙
        //obstacles.push_back(BoundingBox(253.6, 0.0, 0.0, 272.6, 8.7, 8.4));   // 5-6料仓台
        obstacles.push_back(BoundingBox(255.3, 1.5, 0.0, 258.5, 6.3, 10.5));   // 5-6料仓台：投料口11
        obstacles.push_back(BoundingBox(261.4, 1.5, 0.0, 264.6, 6.3, 10.5));   // 5-6料仓台：投料口12
        obstacles.push_back(BoundingBox(267.3, 1.5, 0.0, 270.5, 6.3, 10.5));   // 5-6料仓台：投料口13
        obstacles.push_back(BoundingBox(271.7, 0.0, 0.0, 272.6, 8.7, 10.5));   // 5-6料仓台：左栏杆
        obstacles.push_back(BoundingBox(253.4, 8.05, 0.0, 272.6, 8.7, 10.5));   // 5-6料仓台：下栏杆
        obstacles.push_back(BoundingBox(253.4, 0.0, 0.0, 254.4, 8.7, 10.5));   // 5-6料仓台：右栏杆
        obstacles.push_back(BoundingBox(289.1, 0.0, 0.0, 294.1, 4.25, 10.5));   // 7仓左上角
        obstacles.push_back(BoundingBox(293.8, 0.0, 0.0, 350.0, 32.0, 10.5));   // 7仓左侧
        //obstacles.push_back(BoundingBox(253.6, 26.1, 0.0, 265.8, 32.0, 10.5));   // 5-6仓卸料口
        obstacles.push_back(BoundingBox(253.7, 26.0, 0.0, 266.2, 26.5, 2.2));   // 5-6仓卸料口边缘1
        obstacles.push_back(BoundingBox(265.8, 26.0, 0.0, 266.0, 30.0, 2.2));   // 5-6仓卸料口边缘2
        obstacles.push_back(BoundingBox(259.8, 26.2, 0.0, 260.2, 30.0, 2.2));   // 5-6仓卸料口边缘3
        obstacles.push_back(BoundingBox(254.0, 26.2, 0.0, 254.6, 30.0, 2.2));   // 5-6仓卸料口边缘4
        obstacles.push_back(BoundingBox(248.0, 0.0, 0.0, 276.7, 11.6, 12.0));   //5-6#库料堆1
        obstacles.push_back(BoundingBox(268.4, 17.5, 0.0, 276.7, 30.0, 12.0));   //5-6#库料堆2
        obstacles.push_back(BoundingBox(248.0, 11.6, 0.0, 273.6, 19.8, 12.0));   //5-6#库料堆3
        obstacles.push_back(BoundingBox(240.0, 0.0, 0.0, 248.0, 13.0, 12.0));   //4#库左上角料堆
        obstacles.push_back(BoundingBox(205.5, 0.0, 0.0, 224.6, 8.9, 10.5));   // 4#库料仓台
        obstacles.push_back(BoundingBox(233.2, 23.8, 0.0, 248.0, 32.0, 10.5));   // 4仓左下角雾炮机附近
        obstacles.push_back(BoundingBox(181.3, 0.0, 0.0, 182.5, 32.0, 10.5));  // 3-4#库隔断墙
        obstacles.push_back(BoundingBox(127.0, 0.0, 0.0, 146.5, 9.0, 10.5));   // 3#库料仓台


        for(int i = 0; i < 5; i++)
        {
            laserCloudIn[i].reset(new pcl::PointCloud<PointType>());
            string str = "/object_detect_seg_cloud" + to_string(i+1);
            pubSegCloud[i] = nh.advertise<sensor_msgs::PointCloud2>(str.c_str(), 2);
            str = "/no_ground_for_person" + to_string(i+1);
            pubNoGroundForPerson[i] = nh.advertise<sensor_msgs::PointCloud2>(str.c_str(), 2);
            str = "/no_ground_for_vehicle" + to_string(i+1);
            pubNoGroundForVehicle[i] = nh.advertise<sensor_msgs::PointCloud2>(str.c_str(), 2);
        }
        


        subLaserCloud1 = nh.subscribe<sensor_msgs::PointCloud2>("/full_seg_cloud1", 2, &ObjectDetect::laserCloudHandler1, this);
        subLaserCloud2 = nh.subscribe<sensor_msgs::PointCloud2>("/full_seg_cloud2", 2, &ObjectDetect::laserCloudHandler2, this);

    }

    void laserCloudHandler1(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        laserCloudIn[0].reset(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(*laserCloudMsg, *laserCloudIn[0]);
        detect(0);
    }

    void laserCloudHandler2(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        laserCloudIn[1].reset(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(*laserCloudMsg, *laserCloudIn[1]);
        detect(1);
    }

    bool IsBetweenTwoValue(double tmp, double min, double max)
    {
        return tmp > min && tmp < max;
    }

    void detect(int index)
    {
        int flag = index;
        pcl::PointCloud<PointType>::Ptr laserCloudInTemp;
        laserCloudInTemp.reset(new pcl::PointCloud<PointType>());
        *laserCloudInTemp += *laserCloudIn[flag];
        pcl::PointCloud<PointType>::Ptr object_cloud(new pcl::PointCloud<PointType>);
        vector<ObjectEntity>* objects_tmp = new vector<ObjectEntity>();

        PointType ptmin, ptmax;
        pcl::getMinMax3D(*laserCloudInTemp, ptmin, ptmax);
        machine_pose_mtx[flag].lock();
        x_pos_cur[flag] = (ptmin.x + ptmax.x) / 2.0;
        machine_pose_mtx[flag].unlock();

        // 5-6仓下侧平台1, 泄料车
        //obstacles.push_back(BoundingBox(253.6, 26.1, 0.0, 265.8, 32.0, 10.5));   // 5-6仓下侧平台
        pcl::PointCloud<PointType>::Ptr laserCloudInTemp561;
        laserCloudInTemp561.reset(new pcl::PointCloud<PointType>());
        for(auto pt:laserCloudInTemp->points)
            if(IsBetweenTwoValue(pt.x, 254.8, 259.6) && IsBetweenTwoValue(pt.y, 25.0, 32.0) && IsBetweenTwoValue(pt.z, 3.0, 6.0))
                laserCloudInTemp561->push_back(PointType(pt));
        if(laserCloudInTemp561->size()>20)
        {
            
            ROS_INFO("\033[1;32m ######## 5-6-1 vehicle detector ######## \033[0m");
            PointType pt1, pt2;
            pcl::getMinMax3D(*laserCloudInTemp561, pt1, pt2);
            double dx = pt2.x - pt1.x;
            double dy = pt2.y - pt1.y;
            double dz = pt2.z - pt1.z;
            ROS_INFO("\033[1;32m---->\033[0m dx:%lf, dy:%lf, dz:%lf, minz:%lf", dx, dy, dz, pt1.z);
            if(IsBetweenTwoValue(dx, 1.5, 4.0) && IsBetweenTwoValue(dy, 1.5, 10.0))
            {
                for(int j = 0; j < laserCloudInTemp561->size(); j++)
                        laserCloudInTemp561->points[j].intensity = 240;
                    *object_cloud += *laserCloudInTemp561;
                    ObjectEntity oet(2);
                    oet.center.x = pt1.x + 0.5 * dx;
                    oet.center.y = pt1.y; //min y
                    oet.center.z = pt1.z + 0.5 * dz;
                    oet.center.intensity = 2;
                    oet.range.x = dx;
                    oet.range.y = dy;
                    oet.range.z = dz;
                    *oet.points += *laserCloudInTemp561;
                    objects_tmp->push_back(ObjectEntity(oet));
                    ROS_INFO("\033[1;31m---->\033[0m detect vehicle at %lf, %lf, %lf", oet.center.x, oet.center.y, oet.center.z);
            }
        }

        // 5-6仓下侧平台2, 泄料车
        //obstacles.push_back(BoundingBox(253.6, 26.1, 0.0, 265.8, 32.0, 10.5));   // 5-6仓下侧平台
        pcl::PointCloud<PointType>::Ptr laserCloudInTemp562;
        laserCloudInTemp562.reset(new pcl::PointCloud<PointType>());
        for(auto pt:laserCloudInTemp->points)
            if(IsBetweenTwoValue(pt.x, 260.7, 265.7) && IsBetweenTwoValue(pt.y, 25.0, 32.0) && IsBetweenTwoValue(pt.z, 3.0, 6.0))
                laserCloudInTemp562->push_back(PointType(pt));
        if(laserCloudInTemp562->size()>20)
        {
            
            ROS_INFO("\033[1;32m ######## 5-6-2 vehicle detector ######## \033[0m");
            PointType pt1, pt2;
            pcl::getMinMax3D(*laserCloudInTemp562, pt1, pt2);
            double dx = pt2.x - pt1.x;
            double dy = pt2.y - pt1.y;
            double dz = pt2.z - pt1.z;
            ROS_INFO("\033[1;32m---->\033[0m dx:%lf, dy:%lf, dz:%lf, minz:%lf", dx, dy, dz, pt1.z);
            if(IsBetweenTwoValue(dx, 1.5, 4.0) && IsBetweenTwoValue(dy, 1.5, 10.0))
            {
                for(int j = 0; j < laserCloudInTemp562->size(); j++)
                        laserCloudInTemp562->points[j].intensity = 240;
                    *object_cloud += *laserCloudInTemp562;
                    ObjectEntity oet(2);
                    oet.center.x = pt1.x + 0.5 * dx;
                    oet.center.y = pt1.y; //min y
                    oet.center.z = pt1.z + 0.5 * dz;
                    oet.center.intensity = 2;
                    oet.range.x = dx;
                    oet.range.y = dy;
                    oet.range.z = dz;
                    *oet.points += *laserCloudInTemp562;
                    objects_tmp->push_back(ObjectEntity(oet));
                    ROS_INFO("\033[1;31m---->\033[0m detect vehicle at %lf, %lf, %lf", oet.center.x, oet.center.y, oet.center.z);
            }
        }
        
        // seg out obstacles
        pcl::PointCloud<PointType>::Ptr SegCloud;
        SegCloud.reset(new pcl::PointCloud<PointType>());
        *SegCloud += *segPointCloudNearObstacles(laserCloudInTemp);
        
        // remove planar point cloud
        pcl::PointCloud<PointType>::Ptr Non_Planar_cloud;
        Non_Planar_cloud.reset(new pcl::PointCloud<PointType>());
        Non_Planar_cloud = PlaneExtractionOut(SegCloud);

        
        // euclidean cluster for vehicle
        pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>());
        pcl::EuclideanClusterExtraction<PointType> ece;
        ece.setSearchMethod(tree);

        // for vehicle detection
        // seg out 5-6仓下侧平台
        //obstacles.push_back(BoundingBox(253.6, 26.1, 0.0, 265.8, 32.0, 10.5));   // 5-6仓下侧平台
        pcl::PointCloud<PointType>::Ptr laserCloudInTemp56Out;
        laserCloudInTemp56Out.reset(new pcl::PointCloud<PointType>());
        for(auto pt:Non_Planar_cloud->points)
            if(IsBetweenTwoValue(pt.x, 253.6, 265.8) && IsBetweenTwoValue(pt.y, 26.1, 32.0) && IsBetweenTwoValue(pt.z, 0.0, 10.5))
                continue;
            else
                laserCloudInTemp56Out->push_back(PointType(pt));

        // remove lowest point cloud
        pcl::PointCloud<PointType>::Ptr NoGroundForVehicle;
        NoGroundForVehicle.reset(new pcl::PointCloud<PointType>());
        NoGroundForVehicle = revemoLowestCloudFilterForVehicle56(laserCloudInTemp56Out);

        if(NoGroundForVehicle->size() > 3)
        {
            // euclidean cluster for vehicle

            vector<pcl::PointIndices> ece_inlier_vehicle;
            ece.setInputCloud(NoGroundForVehicle);
            ece.setClusterTolerance(DetectorVehicle_ECE_Tolerance);
            ece.setMinClusterSize(DetectorVehicle_ECE_Min);
            ece.setMaxClusterSize(10000);
            ece.extract(ece_inlier_vehicle);

            if(ece_inlier_vehicle.size() > 0)
                ROS_INFO("\033[1;32m ######## vehicle detector ######## \033[0m");
            for(int i = 0; i < ece_inlier_vehicle.size(); i++)
            {
                pcl::PointCloud<PointType>::Ptr tmp(new pcl::PointCloud<PointType>);
                pcl::copyPointCloud(*NoGroundForVehicle, ece_inlier_vehicle[i].indices, *tmp);
                PointType pt1, pt2;
                pcl::getMinMax3D(*tmp, pt1, pt2);
                double dx = pt2.x - pt1.x;
                double dy = pt2.y - pt1.y;
                double dz = pt2.z - pt1.z;

                ROS_INFO("\033[1;32m---->\033[0m dx:%lf, dy:%lf, dz:%lf, minz:%lf", dx, dy, dz, pt1.z);
                
                if( IsBetweenTwoValue(dx/dy, 0.33, 3.0)
                && IsBetweenTwoValue(dx, DetectorVehicle_MinXY, DetectorVehicle_MaxXY) 
                && IsBetweenTwoValue(dy, DetectorVehicle_MinXY, DetectorVehicle_MaxXY) 
                && IsBetweenTwoValue(dz, 2.8, 4.0)
                && pt1.z < 3.0)
                {
                    for(int j = 0; j < tmp->size(); j++)
                        tmp->points[j].intensity = i * 20 + 20;
                    *object_cloud += *tmp;
                    ObjectEntity oet(2);
                    oet.center.x = pt1.x + 0.5 * dx;
                    oet.center.y = pt1.y + 0.5 * dy;
                    oet.center.z = pt1.z + 0.5 * dz;
                    oet.center.intensity = 2;
                    oet.range.x = dx;
                    oet.range.y = dy;
                    oet.range.z = dz;
                    *oet.points += *tmp;
                    objects_tmp->push_back(ObjectEntity(oet));
                    ROS_INFO("\033[1;31m---->\033[0m detect vehicle at %lf, %lf, %lf", oet.center.x, oet.center.y, oet.center.z);
                }
                // else
                // {
                //     for(int j = 0; j < tmp->size(); j++)
                //         tmp->points[j].intensity = 0;
                //     *object_cloud += *tmp;
                // }
            }
        }

        // for person detection
        pcl::PointCloud<PointType>::Ptr NoGround;
        NoGround.reset(new pcl::PointCloud<PointType>());
        NoGround = revemoLowestCloudFilterForPerson(Non_Planar_cloud, *objects_tmp);

        if(NoGround->size() > 3)
        {

            vector<pcl::PointIndices> ece_inlier;
            ece.setInputCloud(NoGround);
            ece.setClusterTolerance(DetectorPerson_ECE_Tolerance);
            ece.setMinClusterSize(DetectorPerson_ECE_Min);
            ece.setMaxClusterSize(1000);
            ece.setSearchMethod(tree);
            ece.extract(ece_inlier);

            if(ece_inlier.size() > 0)
                ROS_INFO("\033[1;32m ######## person detector ######## \033[0m");
            for(int i = 0; i < ece_inlier.size(); i++)
            {
                pcl::PointCloud<PointType>::Ptr tmp(new pcl::PointCloud<PointType>);
                pcl::copyPointCloud(*NoGround, ece_inlier[i].indices, *tmp);
                PointType pt1, pt2;
                pcl::getMinMax3D(*tmp, pt1, pt2);
                double dx = pt2.x - pt1.x;
                double dy = pt2.y - pt1.y;
                double dz = pt2.z - pt1.z;
                ROS_INFO("\033[1;32m---->\033[0m dx:%lf, dy:%lf, dz:%lf, minz:%lf", dx, dy, dz, pt1.z);
                
                if( IsBetweenTwoValue(dx/dy, 0.33, 3.0)
                && IsBetweenTwoValue(dx, DetectorPerson_MinXY, DetectorPerson_MaxXY) 
                && IsBetweenTwoValue(dy, DetectorPerson_MinXY, DetectorPerson_MaxXY) 
                && IsBetweenTwoValue(dz, 0.7, 1.8)
                && pt1.z < 2.5)
                {
                    for(int j = 0; j < tmp->size(); j++)
                        tmp->points[j].intensity = i * 20 + 20;
                    *object_cloud += *tmp;
                    ObjectEntity oet(1);
                    oet.center.x = pt1.x + 0.5 * dx;
                    oet.center.y = pt1.y + 0.5 * dy;
                    oet.center.z = pt1.z + 0.5 * dz;
                    oet.center.intensity = 1;
                    oet.range.x = dx;
                    oet.range.y = dy;
                    oet.range.z = dz;
                    *oet.points += *tmp;
                    objects_tmp->push_back(ObjectEntity(oet));
                    ROS_INFO("\033[1;31m---->\033[0m detect person at %lf, %lf, %lf", oet.center.x, oet.center.y, oet.center.z);
                }
                else if( IsBetweenTwoValue(dx/dy, 0.33, 3.0)
                && IsBetweenTwoValue(dx, DetectorPerson_MinXY - 0.05, DetectorPerson_MaxXY) 
                && IsBetweenTwoValue(dy, DetectorPerson_MinXY - 0.05, DetectorPerson_MaxXY) 
                && IsBetweenTwoValue(dz, 0.7, 1.8)
                && IsBetweenTwoValue(pt1.x, 254.3, 265.8)
                && IsBetweenTwoValue(pt1.y, 26.1, 32.0)
                && pt1.z < 2.8)
                {
                    for(int j = 0; j < tmp->size(); j++)
                        tmp->points[j].intensity = i * 20 + 20;
                    *object_cloud += *tmp;
                    ObjectEntity oet(1);
                    oet.center.x = pt1.x + 0.5 * dx;
                    oet.center.y = pt1.y + 0.5 * dy;
                    oet.center.z = pt1.z + 0.5 * dz;
                    oet.center.intensity = 1;
                    oet.range.x = dx;
                    oet.range.y = dy;
                    oet.range.z = dz;
                    *oet.points += *tmp;
                    objects_tmp->push_back(ObjectEntity(oet));
                    ROS_INFO("\033[1;31m---->\033[0m detect person at %lf, %lf, %lf", oet.center.x, oet.center.y, oet.center.z);
                }
                // else
                // {
                //     for(int j = 0; j < tmp->size(); j++)
                //         tmp->points[j].intensity = 0;
                //     *object_cloud += *tmp;
                // }
            }
        }

        object_pose_mtx[flag].lock();
        delete objects[flag];
        objects[flag] = objects_tmp;
        object_pose_mtx[flag].unlock();

        PointType p;
        p.x = p.y = p.z = p.intensity = 0;
        object_cloud->push_back(p);
        sensor_msgs::PointCloud2 laserCloudTemp;
        pcl::toROSMsg(*object_cloud, laserCloudTemp);
        laserCloudTemp.header.stamp = ros::Time::now();
        laserCloudTemp.header.frame_id = "/map";
        pubSegCloud[flag].publish(laserCloudTemp);

        sensor_msgs::PointCloud2 laserCloudTemp2;
        pcl::toROSMsg(*NoGround, laserCloudTemp2);
        laserCloudTemp2.header.stamp = ros::Time::now();
        laserCloudTemp2.header.frame_id = "/map";
        pubNoGroundForPerson[flag].publish(laserCloudTemp2);

        // sensor_msgs::PointCloud2 laserCloudTemp3;
        // pcl::toROSMsg(*NoGroundForVehicle, laserCloudTemp3);
        // laserCloudTemp3.header.stamp = ros::Time::now();
        // laserCloudTemp3.header.frame_id = "/map";
        // pubNoGroundForVehicle[flag].publish(laserCloudTemp3);

        

    }

    void publishThread()
    {
        ros::Rate rate(10);
        while(ros::ok())
        {
            rate.sleep();
        }
        
    }

     
pcl::PointCloud<PointType>::Ptr PointCloudBoundary(pcl::PointCloud<PointType>::Ptr cloud)
{
 
	// 1 计算法向量
	pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
	pcl::PointCloud<pcl::Normal>::Ptr  normals(new  pcl::PointCloud<pcl::Normal>);
	pcl::NormalEstimation<PointType, pcl::Normal> normalEstimation;
    pcl::PointCloud<PointType>::Ptr cloudOut(new pcl::PointCloud<PointType>);
	normalEstimation.setInputCloud(cloud);
	normalEstimation.setSearchMethod(tree);
	normalEstimation.setRadiusSearch(0.2);  // 法向量的半径
	normalEstimation.compute(*normals);
 
	/*pcl计算边界*/
	pcl::PointCloud<pcl::Boundary>::Ptr boundaries(new pcl::PointCloud<pcl::Boundary>); //声明一个boundary类指针，作为返回值
	boundaries->resize(cloud->size()); //初始化大小
	pcl::BoundaryEstimation<PointType, pcl::Normal, pcl::Boundary> boundary_estimation; //声明一个BoundaryEstimation类
	boundary_estimation.setInputCloud(cloud); //设置输入点云
	boundary_estimation.setInputNormals(normals); //设置输入法线
	pcl::search::KdTree<PointType>::Ptr kdtree_ptr(new pcl::search::KdTree<PointType>);
	boundary_estimation.setSearchMethod(kdtree_ptr); //设置搜寻k近邻的方式
	boundary_estimation.setKSearch(20); //设置k近邻数量
	boundary_estimation.setAngleThreshold(M_PI * 0.6); //设置角度阈值，大于阈值为边界
	boundary_estimation.compute(*boundaries); //计算点云边界，结果保存在boundaries中
 
	for(int i = 0; i < cloud->size(); i++)
    {
        if(boundaries->points[i].boundary_point != 0)
            cloudOut->push_back(PointType(cloud->points[i]));
    }
    return cloudOut;
}

pcl::PointCloud<PointType>::Ptr  PlaneExtraction(pcl::PointCloud<PointType>::Ptr cloud)
{
 
	// 0 计算法向量
	pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
	pcl::PointCloud<pcl::Normal>::Ptr  normals(new  pcl::PointCloud<pcl::Normal>);
	pcl::NormalEstimation<PointType, pcl::Normal> normalEstimation;
	normalEstimation.setInputCloud(cloud);
	normalEstimation.setSearchMethod(tree);
	normalEstimation.setRadiusSearch(0.2);  // 法向量的半径
	normalEstimation.compute(*normals);
 
 
	 // 1 提出出平面 
		// 提取平面点云的索引
	pcl::PointIndices::Ptr  index_plane(new pcl::PointIndices);
	pcl::SACSegmentationFromNormals<PointType, pcl::Normal> sacSegmentationFromNormals;
	pcl::ModelCoefficients::Ptr mdelCoefficients_plane(new pcl::ModelCoefficients);
	sacSegmentationFromNormals.setInputCloud(cloud);
	sacSegmentationFromNormals.setOptimizeCoefficients(true);//设置对估计的模型系数需要进行优化
	sacSegmentationFromNormals.setModelType(pcl::SACMODEL_NORMAL_PLANE); //设置分割模型
	sacSegmentationFromNormals.setNormalDistanceWeight(0.1);//设置表面法线权重系数
	sacSegmentationFromNormals.setMethodType(pcl::SAC_RANSAC);//设置采用RANSAC作为算法的参数估计方法
	sacSegmentationFromNormals.setMaxIterations(500); //设置迭代的最大次数
	sacSegmentationFromNormals.setDistanceThreshold(0.2); //设置内点到模型的距离允许最大值
	sacSegmentationFromNormals.setInputCloud(cloud);
  	sacSegmentationFromNormals.setInputNormals(normals);
	sacSegmentationFromNormals.segment(*index_plane, *mdelCoefficients_plane);
 
	 // 点云提取
	pcl::ExtractIndices<PointType> extractIndices;
	pcl::PointCloud<PointType>::Ptr  cloud_p(new pcl::PointCloud<PointType>);
	extractIndices.setInputCloud(cloud);
	extractIndices.setIndices(index_plane);
	extractIndices.setNegative(false);
	extractIndices.filter(*cloud_p);

    return cloud_p;
}


pcl::PointCloud<PointType>::Ptr  PlaneExtractionOut(pcl::PointCloud<PointType>::Ptr cloud)
{
 
	// 0 计算法向量
	pcl::search::KdTree<PointType>::Ptr tree(new pcl::search::KdTree<PointType>);
	pcl::PointCloud<pcl::Normal>::Ptr  normals(new  pcl::PointCloud<pcl::Normal>);
	pcl::NormalEstimation<PointType, pcl::Normal> normalEstimation;
	normalEstimation.setInputCloud(cloud);
	normalEstimation.setSearchMethod(tree);
	normalEstimation.setRadiusSearch(0.2);  // 法向量的半径
	normalEstimation.compute(*normals);
 
 
	 // 1 提出出平面 
		// 提取平面点云的索引
	pcl::PointIndices::Ptr  index_plane(new pcl::PointIndices);
	pcl::SACSegmentationFromNormals<PointType, pcl::Normal> sacSegmentationFromNormals;
	pcl::ModelCoefficients::Ptr mdelCoefficients_plane(new pcl::ModelCoefficients);
	sacSegmentationFromNormals.setInputCloud(cloud);
	sacSegmentationFromNormals.setOptimizeCoefficients(true);//设置对估计的模型系数需要进行优化
	sacSegmentationFromNormals.setModelType(pcl::SACMODEL_NORMAL_PLANE); //设置分割模型
	sacSegmentationFromNormals.setNormalDistanceWeight(0.1);//设置表面法线权重系数
	sacSegmentationFromNormals.setMethodType(pcl::SAC_RANSAC);//设置采用RANSAC作为算法的参数估计方法
	sacSegmentationFromNormals.setMaxIterations(100); //设置迭代的最大次数
	sacSegmentationFromNormals.setDistanceThreshold(0.2); //设置内点到模型的距离允许最大值
	sacSegmentationFromNormals.setInputCloud(cloud);
  	sacSegmentationFromNormals.setInputNormals(normals);
	sacSegmentationFromNormals.segment(*index_plane, *mdelCoefficients_plane);
 
	 // 点云提取
	pcl::ExtractIndices<PointType> extractIndices;
	pcl::PointCloud<PointType>::Ptr  cloud_p(new pcl::PointCloud<PointType>);
	extractIndices.setInputCloud(cloud);
	extractIndices.setIndices(index_plane);
	extractIndices.setNegative(true);
	extractIndices.filter(*cloud_p);

    return cloud_p;
}
    
    pcl::PointCloud<PointType>::Ptr revemoLowestCloudFilterForPerson(pcl::PointCloud<PointType>::Ptr cloudIn, vector<ObjectEntity> objects_tmp)
    {
        pcl::PointCloud<PointType>::Ptr cloudInZFiltered, cloudOutTemp, cloudOut;
        cloudInZFiltered.reset(new pcl::PointCloud<PointType>());
        cloudOutTemp.reset(new pcl::PointCloud<PointType>());
        cloudOut.reset(new pcl::PointCloud<PointType>());

        pcl::PassThrough<PointType> zFilter;
        zFilter.setFilterFieldName("z");
        zFilter.setFilterLimits(0, 6.0);
        zFilter.setInputCloud(cloudIn);
        zFilter.filter(*cloudInZFiltered);

        PointType pt1, pt2;
        pcl::getMinMax3D(*cloudInZFiltered, pt1, pt2);
        double res = DetectorPersonResolution;
        double minx = round(pt1.x / res) * res - res, maxx = round(pt2.x / res) * res + res;
        double miny = round(pt1.y / res) * res - res, maxy = round(pt2.y / res) * res + res;
        int xsize = round((maxx - minx) / res), ysize = round((maxy - miny) / res);
        vector<MyVoxel> voxels;
        for(double x = minx; x < maxx; x += res)
            for(double y = miny; y < maxy; y += res)
                voxels.push_back(MyVoxel(DetectorPersonThresholdZ));
            
        for(auto p:cloudInZFiltered->points)
        {
             bool IsDelete = false;
             for(auto oet:objects_tmp)
             {
                if(oet.IsPointNear(p))
                {
                    IsDelete = true;
                    break;
                }
             }
             if(IsDelete)
                continue;
             int x = round((p.x - minx) / res), y = round((p.y - miny) / res);
             voxels[x * ysize + y].insertPoint(p);
        }

        for(auto v:voxels)
        {
            pcl::PointCloud<PointType>::Ptr cloudOutV = v.getPointsBelowRelevantZ(2.0);
            if(cloudOutV->size() > 1)
                *cloudOut += *cloudOutV;
            // PointType ptmin, ptmax;
            // pcl::getMinMax3D(*cloudOutV, ptmin, ptmax);
            // double v_zmin = ptmin.z, v_zmax = ptmin.z + DetectorPersonThresholdZ * 2.0;
            // bool IsAddPoint = true;
            // pcl::PointCloud<PointType>::Ptr cloudOutVFiltered;
            // cloudOutVFiltered.reset(new pcl::PointCloud<PointType>());
            // while(IsAddPoint)
            // {
            //     IsAddPoint = false;
            //     for(int i = 0; i < cloudOutV->size(); i++)
            //     {
            //         PointType *p = &(cloudOutV->points[i]);
            //         if(p->z <= v_zmax)
            //         {
            //             cloudOutVFiltered->push_back(PointType(*p));
            //             IsAddPoint = true;
            //             p->z = 1000.0;
            //             continue;
            //         }
            //         else if(p->z - v_zmax < DetectorPersonThresholdZ * 2.0)
            //         {
            //             cloudOutVFiltered->push_back(PointType(*p));
            //             IsAddPoint = true;
            //             v_zmax = p->z + DetectorPersonThresholdZ * 2.0;
            //             p->z = 1000.0;
            //             continue;
            //         }
            //     }
            // }
            // pcl::getMinMax3D(*cloudOutVFiltered, ptmin, ptmax);
            // if(cloudOutVFiltered->size() > 3 && ptmax.z - ptmin.z > DetectorPersonThresholdZ)
            //     *cloudOut += *cloudOutVFiltered;
        }

        return cloudOut;
    }

    pcl::PointCloud<PointType>::Ptr revemoLowestCloudFilterForVehicle(pcl::PointCloud<PointType>::Ptr cloudIn)
    {
        pcl::PointCloud<PointType>::Ptr cloudInZFiltered, cloudOutTemp, cloudOut;
        cloudInZFiltered.reset(new pcl::PointCloud<PointType>());
        cloudOutTemp.reset(new pcl::PointCloud<PointType>());
        cloudOut.reset(new pcl::PointCloud<PointType>());

        pcl::PassThrough<PointType> zFilter;
        zFilter.setFilterFieldName("z");
        zFilter.setFilterLimits(0, 8.0);
        zFilter.setInputCloud(cloudIn);
        zFilter.filter(*cloudInZFiltered);

        PointType pt1, pt2;
        pcl::getMinMax3D(*cloudInZFiltered, pt1, pt2);
        double res = DetectorVehicleResolution;
        double minx = round(pt1.x / res) * res - res, maxx = round(pt2.x / res) * res + res;
        double miny = round(pt1.y / res) * res - res, maxy = round(pt2.y / res) * res + res;
        int xsize = round((maxx - minx) / res), ysize = round((maxy - miny) / res);
        vector<MyVoxel> voxels;
        for(double x = minx; x < maxx; x += res)
            for(double y = miny; y < maxy; y += res)
                voxels.push_back(MyVoxel(DetectorVehicleThresholdZ));
            
        for(auto p:cloudInZFiltered->points)
        {
             int x = round((p.x - minx) / res), y = round((p.y - miny) / res);
             voxels[x * ysize + y].insertPoint(p);
        }

        for(auto v:voxels)
        {
            pcl::PointCloud<PointType>::Ptr cloudOutV = v.getPointsBelowAbsoluteZ(5.0);
            if(cloudOutV->size() > 1)
                *cloudOut += *cloudOutV;
        }

        return cloudOut;
    }

    pcl::PointCloud<PointType>::Ptr revemoLowestCloudFilterForVehicle56(pcl::PointCloud<PointType>::Ptr cloudIn)
    {
        pcl::PointCloud<PointType>::Ptr cloudInZFiltered, cloudOutTemp, cloudOut;
        cloudInZFiltered.reset(new pcl::PointCloud<PointType>());
        cloudOutTemp.reset(new pcl::PointCloud<PointType>());
        cloudOut.reset(new pcl::PointCloud<PointType>());

        pcl::PassThrough<PointType> zFilter;
        zFilter.setFilterFieldName("z");
        zFilter.setFilterLimits(0, 8.0);
        zFilter.setInputCloud(cloudIn);
        zFilter.filter(*cloudInZFiltered);

        PointType pt1, pt2;
        pcl::getMinMax3D(*cloudInZFiltered, pt1, pt2);
        double res = DetectorVehicleResolution;
        double minx = round(pt1.x / res) * res - res, maxx = round(pt2.x / res) * res + res;
        double miny = round(pt1.y / res) * res - res, maxy = round(pt2.y / res) * res + res;
        int xsize = round((maxx - minx) / res), ysize = round((maxy - miny) / res);
        vector<MyVoxel> voxels;
        for(double x = minx; x < maxx; x += res)
            for(double y = miny; y < maxy; y += res)
                voxels.push_back(MyVoxel(DetectorVehicleThresholdZ));
            
        for(auto p:cloudInZFiltered->points)
        {
             int x = round((p.x - minx) / res), y = round((p.y - miny) / res);
             voxels[x * ysize + y].insertPoint(p);
        }

        for(auto v:voxels)
        {
            pcl::PointCloud<PointType>::Ptr cloudOutV = v.getPointsBelowAbsoluteZ(8.0);
            if(cloudOutV->size() > 1)
                *cloudOut += *cloudOutV;
        }

        return cloudOut;
    }

    pcl::PointCloud<PointType>::Ptr segPointCloudNearObstacles(pcl::PointCloud<PointType>::Ptr cloudIn)
    {
        
        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());

        int obstacles_size = obstacles.size();

        for(auto p:cloudIn->points)
        {
            bool IsIn = false;
            for(int i = 0; i < obstacles_size; i++)
            {
                IsIn = obstacles[i].IsPointNearBBox(p, Detector_To_Obstacle_Dist);
                if(IsIn)
                    break;
            }
            if(IsIn)
                continue;
            else
                cloudOut->points.push_back(PointType(p));
        }
        return cloudOut;
    }

    void sendUDPThread()
    {
        int sock_fd;
        sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
        if(sock_fd < 0)
        {
            ROS_INFO("SOCKET ERROR");
            return;
        }
        else
            ROS_INFO("SOCKET SUCCESS");
        fcntl(sock_fd, F_SETFL, fcntl(sock_fd, F_GETFL, 0) | O_NONBLOCK);

        vector<struct sockaddr_in> addr_serv_vec;
        int len;
        for(int i = 0; i < UDPNUM; i++)
        {
            string ip_port = UDPs[i];
            string ip = ip_port.substr(0, ip_port.find(":"));
            int port = atoi(ip_port.substr(ip_port.find(":")+1).c_str());
            ROS_INFO("ip: %s, port: %d", ip.c_str(), port);
            struct sockaddr_in addr_serv;
            memset(&addr_serv, 0, sizeof(addr_serv));
            addr_serv.sin_family = AF_INET;
            addr_serv.sin_addr.s_addr = inet_addr(ip.c_str());
            addr_serv.sin_port = htons(port);
            addr_serv_vec.push_back(addr_serv);
        }
        
        len = sizeof(addr_serv_vec[0]);

        pcl::PointCloud<PointType>::Ptr tmpElevationCloud;
        int flag = 0;
        ros::Rate rate(50);
        while(ros::ok())
        {
            flag = (flag + 1) % 5;
            if(flag > 1)
            {
                rate.sleep();
                continue;
            }
            
            vector<ObjectEntity>* objects_tmp = new vector<ObjectEntity>();
            object_pose_mtx[flag].lock();
            for(int i = 0; i < objects[flag]->size(); i++)
                objects_tmp->push_back(ObjectEntity((*objects[flag])[i]));
            object_pose_mtx[flag].unlock();

            machine_pose_mtx[flag].lock();
            double x_pos_cur_tmp = x_pos_cur[flag];
            machine_pose_mtx[flag].unlock();

            int send_num;
            size_t send_buf_size = 16 + 20 + 1 + objects_tmp->size() * 13;
            unsigned char bag_length = (unsigned char)(send_buf_size - 4);
            unsigned char *send_buf = (unsigned char *)malloc(send_buf_size);
            memset(send_buf, 0, send_buf_size);

            //ROS_INFO("send udp, %d objects", objects_tmp->size());

            send_buf[0] = 0xDE;
            send_buf[1] = (unsigned char)(bag_length / 256);
            send_buf[2] = (unsigned char)(bag_length % 256);
            send_buf[3] = 0x00;
            send_buf[4] = (unsigned char)(4-flag);
            send_buf[5] = 0x00;
            send_buf[6] = 0x00;
            send_buf[7] = 0x00;
            send_buf[8] = 0x00;
            send_buf[9] = 0x01;

            
            uint32_t x_pos = x_pos_cur_tmp == -1.0 ? 0 : (uint32_t)round((x_pos_cur_tmp) * 100);
            send_buf[10] = (unsigned char)(x_pos / 16777216);
            send_buf[11] = (unsigned char)(x_pos % 16777216 / 65536);
            send_buf[12] = (unsigned char)(x_pos % 65536 / 256);
            send_buf[13] = (unsigned char)(x_pos % 256);

            send_buf[14] = 0x01;
            send_buf[15] = objects_tmp->size();



            for(int i = 0; i < objects_tmp->size(); i++)
            {
                ObjectEntity oet = (*objects_tmp)[i];
                PointType p = oet.center;
                uint32_t x = (uint32_t)round(p.x * 100), y = (uint32_t)round(p.y * 100), z = (uint32_t)round(p.z * 100);
                uint32_t intensity = (uint32_t)round(oet.type);
                ROS_INFO("\033[1;31m---->\033[0m object %d, at %lf, %lf, %lf, type:%d", i, p.x, p.y, p.z, intensity);
                send_buf[16 + i * 13] = (unsigned char)intensity;
                send_buf[17 + i * 13] = (unsigned char)(x / 16777216);
                send_buf[18 + i * 13] = (unsigned char)(x % 16777216 / 65536);
                send_buf[19 + i * 13] = (unsigned char)(x % 65536 / 256);
                send_buf[20 + i * 13] = (unsigned char)(x % 256);
                send_buf[21 + i * 13] = (unsigned char)(y / 16777216);
                send_buf[22 + i * 13] = (unsigned char)(y % 16777216 / 65536);
                send_buf[23 + i * 13] = (unsigned char)(y % 65536 / 256);
                send_buf[24 + i * 13] = (unsigned char)(y % 256);
                send_buf[25 + i * 13] = (unsigned char)(z / 16777216);
                send_buf[26 + i * 13] = (unsigned char)(z % 16777216 / 65536);
                send_buf[27 + i * 13] = (unsigned char)(z % 65536 / 256);
                send_buf[28 + i * 13] = (unsigned char)(z % 256);
            }

            send_buf[send_buf_size - 1] = getCheckCode(send_buf, 0, send_buf_size - 1);

            // for(int i = 0; i < send_buf_size; i++)
            //     ROS_INFO("%d : %d", i, send_buf[i]);

            
            string udp_string;
            for(int i = 0; i < UDPNUM; i++)
            {
                unsigned char *send_buf_tmp = (unsigned char *)malloc(send_buf_size);
                memcpy(send_buf_tmp, send_buf, send_buf_size);
                send_num = sendto(sock_fd, send_buf_tmp, send_buf_size, 0, (struct sockaddr *)&addr_serv_vec[i], sizeof(addr_serv_vec[i]));
                free(send_buf_tmp);
            }
            free(send_buf);

            

            rate.sleep();
        }
    }

    unsigned char getCheckCode(unsigned char *sendBuf, int start_index, int end_index)
    {
        unsigned char checkCode = 0;
        for(int i = start_index; i < end_index; i++)
            checkCode += sendBuf[i];
        return checkCode;
    }


    

    



    

};

int main(int argc, char** argv){

    ros::init(argc, argv, "model_builder");
    
    ObjectDetect OD;

    ROS_INFO("\033[1;32m---->\033[0m Object Detect Started.");

    std::thread SendUDPThread(&ObjectDetect::sendUDPThread, &OD);


    ros::MultiThreadedSpinner spinner(4);

    spinner.spin();

    SendUDPThread.join();

    return 0;
}