#include <my_utility.h>


class SensorCollector{
    private:
    ros::NodeHandle nh, ng;
    string topic1;
    string topic2;
    double xdiff;
    ros::Subscriber subLaserCloud1, subLaserCloud2;
    pcl::PointCloud<PointType>::Ptr laserCloudIn1, laserCloudIn2;
    double time1, time2;
    int merge_fail_times;
    int IsCalibrating;

    double timeLaserCloudIn1, timeLaserCloudIn2, laserCloudInProcessedTimeLast1, laserCloudInProcessedTimeLast2;
    int laserCloudInPointerLast1, laserCloudInPointerLast2;
    double laserCloudInTimeArray1[imuQueLength / 20], laserCloudInTimeArray2[imuQueLength / 20];
    bool laserCloudInProcessed1[imuQueLength / 20], laserCloudInProcessed2[imuQueLength / 20];
    pcl::PointCloud<PointType>::Ptr laserCloudInArray1[imuQueLength / 20], laserCloudInArray2[imuQueLength / 20];

    pcl::PointCloud<PointType>::Ptr fullCloud;
    ros::Publisher pubFullCloud;
    PointType nanPoint; // fill in fullCloud at each iteration
    mutex mtx;

    double transform1[3][4] = 
        { 
            {0.232382029295, -0.008833367378, 0.972584486008, 7.798072814941 - 10.71},
            {-0.021410025656, 0.999670028687, 0.014194918796, 0.201781034470 + 15.56},
            {-0.972388923168, -0.024121701717, 0.232116222382, 7.447278976440 + 9.59}
            


        };

    double transform2[3][4] = 
        { 
            {0.182902872562, 0.019303975627, 0.982941448689, 7.834307670593 - 10.71},
            {-0.000332773139, 0.999808371067, -0.019573302940, -15.750258445740 + 15.56},
            {-0.983130931854, 0.003252916737, 0.182874247432, 7.458092689514 + 9.59}
            


        };
    double transform[3][4] = 
        { 
            {1.0, 0.0, 0.0, 0.0},
            {0.0, 1.0, 0.0, 0.0},
            {0.0, 0.0, 1.0, 0.0}
        };

    public:
    SensorCollector():
    nh("~"){
        ng.getParam("topic1", topic1);
        ng.getParam("topic2", topic2);
        ng.getParam("xdiff1", xdiff);
        ng.getParam("IsCalibrating", IsCalibrating);
        transform[0][3] += xdiff;

        nanPoint.x = std::numeric_limits<float>::quiet_NaN();
        nanPoint.y = std::numeric_limits<float>::quiet_NaN();
        nanPoint.z = std::numeric_limits<float>::quiet_NaN();
        nanPoint.intensity = -1;
        merge_fail_times = 0;

        laserCloudIn1.reset(new pcl::PointCloud<PointType>());
        laserCloudIn2.reset(new pcl::PointCloud<PointType>());
        time1 = time2 = -1.0;

        laserCloudInPointerLast1 = laserCloudInPointerLast2 = -1;
        laserCloudInProcessedTimeLast1 = 0.0;
        laserCloudInProcessedTimeLast2 = 0.0;
        for(int i = 0; i < imuQueLength / 20; i++)
        {
            laserCloudInArray1[i].reset(new pcl::PointCloud<PointType>());
            laserCloudInTimeArray1[i] = -1.0;
            laserCloudInProcessed1[i] = true;
            laserCloudInArray2[i].reset(new pcl::PointCloud<PointType>());
            laserCloudInTimeArray2[i] = -1.0;
            laserCloudInProcessed2[i] = true;
        }

        fullCloud.reset(new pcl::PointCloud<PointType>());


        subLaserCloud1 = nh.subscribe<sensor_msgs::PointCloud2>(topic1, 2, &SensorCollector::laserCloud1Handler, this);
        subLaserCloud2 = nh.subscribe<sensor_msgs::PointCloud2>(topic2, 2, &SensorCollector::laserCloud2Handler, this);
       
        pubFullCloud = nh.advertise<sensor_msgs::PointCloud2>("/full_cloud1", 2);
    }


    void laserCloud1Handler(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        timeLaserCloudIn1 = ros::Time::now().toSec();
        laserCloudIn1.reset(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(*laserCloudMsg, *laserCloudIn1);
        if(laserCloudIn1->points.size() < 100)
            return;
        laserCloudInPointerLast1 = (laserCloudInPointerLast1 + 1) % (imuQueLength / 20);
        laserCloudInArray1[laserCloudInPointerLast1].reset(new pcl::PointCloud<PointType>());
        *laserCloudInArray1[laserCloudInPointerLast1] += *laserCloudIn1;
        laserCloudInTimeArray1[laserCloudInPointerLast1] = timeLaserCloudIn1;
        laserCloudInProcessed1[laserCloudInPointerLast1] = false;
    }

    void laserCloud2Handler(const sensor_msgs::PointCloud2ConstPtr& laserCloudMsg){
        timeLaserCloudIn2 = ros::Time::now().toSec();
        laserCloudIn2.reset(new pcl::PointCloud<PointType>());
        pcl::fromROSMsg(*laserCloudMsg, *laserCloudIn2);
        if(laserCloudIn2->points.size() < 100)
            return;
        laserCloudInPointerLast2 = (laserCloudInPointerLast2 + 1) % (imuQueLength / 20);
        laserCloudInArray2[laserCloudInPointerLast2].reset(new pcl::PointCloud<PointType>());
        *laserCloudInArray2[laserCloudInPointerLast2] += *laserCloudIn2;
        laserCloudInTimeArray2[laserCloudInPointerLast2] = timeLaserCloudIn2;
        laserCloudInProcessed2[laserCloudInPointerLast2] = false;
    }

    void mergeCloudThread()
    {
        ros::Rate rate(20);
        while (ros::ok())
        {
            mergeCloud_thread();
            rate.sleep();
        }
    }

    void mergeCloud_thread()
    {
        if(laserCloudInPointerLast1 == -1 || laserCloudInPointerLast2 == -1 || merge_fail_times > 60)
            return;

        double time_begin = ros::Time::now().toSec();

        double Time1[imuQueLength / 20], Time2[imuQueLength / 20];
        for(int i = 0; i < imuQueLength / 20; i++)
        {
            Time1[i] = laserCloudInTimeArray1[i];
            Time2[i] = laserCloudInTimeArray2[i];
        }
        
        
        int laserCloudInProcessingPointer1 = -1, laserCloudInProcessingPointer2 = -1;
        double minTimeDiff = 0.06;
        for(int i = 0; i < imuQueLength / 20; i++)
        {
            bool laserCloudInProcessedTemp1 = laserCloudInProcessed1[i];
            double laserCloudInTimeTemp1 = Time1[i];

            if(laserCloudInProcessedTemp1 == true || laserCloudInTimeTemp1 == -1.0 || laserCloudInTimeTemp1 < laserCloudInProcessedTimeLast1)
                continue;
            
            
            for(int j = 0; j < imuQueLength / 20; j++)
            {
                bool laserCloudInProcessedTemp2 = laserCloudInProcessed2[j];
                double laserCloudInTimeTemp2 = Time2[j];

                if(laserCloudInProcessedTemp2 == true || laserCloudInTimeTemp2 == -1.0 || laserCloudInTimeTemp2 < laserCloudInProcessedTimeLast2)
                    continue;

                double timeDiff = fabs(laserCloudInTimeTemp1 - laserCloudInTimeTemp2);
                if(timeDiff < minTimeDiff)
                {
                    minTimeDiff = timeDiff;
                    laserCloudInProcessingPointer1 = i;
                    laserCloudInProcessingPointer2 = j;
                }
            }
        }

        if(laserCloudInProcessingPointer1 == -1 || laserCloudInProcessingPointer2 == -1)
        {
            merge_fail_times++;
            if(merge_fail_times > 60)
            {
                ROS_WARN("livox driver error, relaunch this driver...");
                system("bash /home/kcst/shell/kill_livox_driver.sh");
                ROS_INFO("restart lidar driver...");
                ros::Duration duration(10);
                duration.sleep();
                ROS_INFO("restart lidar sleep over...");
                merge_fail_times = 0;
            }
            return;
        }
        else
            merge_fail_times = 0;

        laserCloudInProcessed1[laserCloudInProcessingPointer1] = true;
        laserCloudInProcessedTimeLast1 = laserCloudInTimeArray1[laserCloudInProcessingPointer1];
        laserCloudInProcessed2[laserCloudInProcessingPointer2] = true;
        laserCloudInProcessedTimeLast2 = laserCloudInTimeArray2[laserCloudInProcessingPointer2];
        double avgTime = (laserCloudInProcessedTimeLast1 + laserCloudInProcessedTimeLast2) / 2.0;
        
        
        pcl::PointCloud<PointType>::Ptr tmp_laserCloudIn1, tmp_laserCloudIn2;
        tmp_laserCloudIn1.reset(new pcl::PointCloud<PointType>());
        tmp_laserCloudIn2.reset(new pcl::PointCloud<PointType>());
        *tmp_laserCloudIn1 += *laserCloudInArray1[laserCloudInProcessingPointer1];
        *tmp_laserCloudIn2 += *laserCloudInArray2[laserCloudInProcessingPointer2];

        pcl::PointCloud<PointType>::Ptr laserCloudIn1Trans, laserCloudIn2Trans;

        laserCloudIn1Trans.reset(new pcl::PointCloud<PointType>());
        laserCloudIn2Trans.reset(new pcl::PointCloud<PointType>());

        int laserCloudIn1Size = tmp_laserCloudIn1->size();
        for(int i = 0; i < laserCloudIn1Size; i++)
        {
            PointType *p = &(tmp_laserCloudIn1->points[i]);
            PointType pt = PointType(*p);
            p->x = pt.x * transform1[0][0] + pt.y * transform1[0][1] + pt.z * transform1[0][2] + 1.0 * transform1[0][3];
            p->y = pt.x * transform1[1][0] + pt.y * transform1[1][1] + pt.z * transform1[1][2] + 1.0 * transform1[1][3];
            p->z = pt.x * transform1[2][0] + pt.y * transform1[2][1] + pt.z * transform1[2][2] + 1.0 * transform1[2][3];
        }
        *laserCloudIn1Trans += *tmp_laserCloudIn1;
        
        int laserCloudIn2Size = tmp_laserCloudIn2->size();
        for(int i = 0; i < laserCloudIn2Size; i++)
        {
            PointType *p = &(tmp_laserCloudIn2->points[i]);
            PointType pt = PointType(*p);
            p->x = pt.x * transform2[0][0] + pt.y * transform2[0][1] + pt.z * transform2[0][2] + 1.0 * transform2[0][3];
            p->y = pt.x * transform2[1][0] + pt.y * transform2[1][1] + pt.z * transform2[1][2] + 1.0 * transform2[1][3];
            p->z = pt.x * transform2[2][0] + pt.y * transform2[2][1] + pt.z * transform2[2][2] + 1.0 * transform2[2][3];
        }
        *laserCloudIn2Trans += *tmp_laserCloudIn2;

        laserCloudIn2Size = laserCloudIn2Trans->size();
        for(int i = 0; i < laserCloudIn2Size; i++)
        {
            PointType *p = &(laserCloudIn2Trans->points[i]);
            PointType pt = PointType(*p);
            p->x = pt.x * transform[0][0] + pt.y * transform[0][1] + pt.z * transform[0][2] + 1.0 * transform[0][3];
            p->y = pt.x * transform[1][0] + pt.y * transform[1][1] + pt.z * transform[1][2] + 1.0 * transform[1][3];
            p->z = pt.x * transform[2][0] + pt.y * transform[2][1] + pt.z * transform[2][2] + 1.0 * transform[2][3];
        }

        fullCloud.reset(new pcl::PointCloud<PointType>());
        if(IsCalibrating == 1)
            *fullCloud += *laserCloudIn1Trans;
        else if (IsCalibrating == 2)
            *fullCloud += *laserCloudIn2Trans;
        else
        {
            *fullCloud += *laserCloudIn1Trans;
            *fullCloud += *laserCloudIn2Trans;
        }

        sensor_msgs::PointCloud2 laserCloudTemp;
        pcl::toROSMsg(*fullCloud, laserCloudTemp);
        laserCloudTemp.header.stamp = ros::Time().fromSec(avgTime);
        laserCloudTemp.header.frame_id = "hesai_frame1";
        pubFullCloud.publish(laserCloudTemp);

        double time_finish = ros::Time::now().toSec();
        //ROS_INFO("Merge Cloud at:%lf, publish %lf", avgTime, time_finish);
    }

    pcl::PointCloud<PointType>::Ptr transformPointCloud(pcl::PointCloud<PointType>::Ptr cloudIn, PointTypePose* transformIn){

        pcl::PointCloud<PointType>::Ptr cloudOut(new pcl::PointCloud<PointType>());

        PointType *pointFrom;
        PointType pointTo;

        int cloudSize = cloudIn->points.size();
        cloudOut->resize(cloudSize);
        
        for (int i = 0; i < cloudSize; ++i){

            pointFrom = &cloudIn->points[i];
            float x1 = cos(transformIn->yaw) * pointFrom->x - sin(transformIn->yaw) * pointFrom->y;
            float y1 = sin(transformIn->yaw) * pointFrom->x + cos(transformIn->yaw)* pointFrom->y;
            float z1 = pointFrom->z;

            float x2 = x1;
            float y2 = cos(transformIn->roll) * y1 - sin(transformIn->roll) * z1;
            float z2 = sin(transformIn->roll) * y1 + cos(transformIn->roll)* z1;

            pointTo.x = cos(transformIn->pitch) * x2 + sin(transformIn->pitch) * z2 + transformIn->x;
            pointTo.y = y2 + transformIn->y;
            pointTo.z = -sin(transformIn->pitch) * x2 + cos(transformIn->pitch) * z2 + transformIn->z;
            pointTo.intensity = pointFrom->intensity;

            cloudOut->points[i] = pointTo;
        }
        return cloudOut;
    }

    



};

int main(int argc, char** argv){

    ros::init(argc, argv, "sensor_collector");
    
    SensorCollector SC;

    ROS_INFO("\033[1;32m---->\033[0m Sensor Collector Started.");

    std::thread MergeCloudThread(&SensorCollector::mergeCloudThread, &SC);

    ros::MultiThreadedSpinner spinner(4);

    spinner.spin();

    MergeCloudThread.join();


    return 0;
}