
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <memory>
#include <cmath>
#include <algorithm>
#include <thread>

#include <ros/ros.h>

#include <sensor_msgs/PointCloud2.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/common.h>
 

#define PI 3.14159265
using namespace std;

class Point
{
    public:
    float x;
    float y;
    float z;

    Point()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Point(float _x, float _y, float _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    ~Point() { }
};

typedef vector<Point> Cloud;

class LineExtraction{
    private:
    ros::NodeHandle nh, ng;
    vector<string> lines;
    vector<double> times;
    vector<Cloud*> clouds;
    string filename;
    double distance_threshold, wall_distance, min_distance, insert_dist_threshold, insert_gap, filter_gap;
    int max_iterations;
    string input;
    ros::Publisher pubScanCloud, pubLeftLine, pubRightLine, pubLeftObs, pubRightObs;
    

    public:
    LineExtraction():
    nh("~"){
        ng.getParam("filename", filename);
        ng.getParam("distance_threshold", distance_threshold);
        ng.getParam("max_iterations", max_iterations);
        ng.getParam("wall_distance", wall_distance);
        ng.getParam("min_distance", min_distance);
        ng.getParam("insert_dist_threshold", insert_dist_threshold);
        ng.getParam("insert_gap", insert_gap);
        ng.getParam("filter_gap", filter_gap);
        
        
        input = "input";

        pubScanCloud = nh.advertise<sensor_msgs::PointCloud2>("/scan_cloud", 2);
        pubLeftLine = nh.advertise<sensor_msgs::PointCloud2>("/left", 2);
        pubRightLine = nh.advertise<sensor_msgs::PointCloud2>("/right", 2);
        pubLeftObs = nh.advertise<sensor_msgs::PointCloud2>("/left_obs", 2);
        pubRightObs = nh.advertise<sensor_msgs::PointCloud2>("/right_obs", 2);
    }

    bool IsScanBegin(int _pos)
    {
        string::size_type idx;
        idx = lines[_pos].find(input);
        if(idx == string::npos)
            return false;
        else
        {
            int size = lines.size();
            for(int i = 1; i < 7; i++)
            {
                if(_pos + i >= size)
                    return false;
                idx = lines[_pos + i].find(input);
                if(idx != string::npos)
                    return false;
            }
            return true;
        }
    }

    vector<string> stringSplit(const string& str, char delim) {
        stringstream ss(str);
        string item;
        vector<string> elems;
        while (getline(ss, item, delim)) {
            if (!item.empty()) {
                elems.push_back(item);
            }
        }
        return elems;
    }

    vector<Point> getInsertPoints(Point pt1, Point pt2, double dist)
    {
        vector<Point> res;

        // 插值的间隔为gap = insert_gap / dist, 其中insert_gap参数可以调整
        double gap = insert_gap / dist;
        for(double m = 0; m < 1; m += gap)
        {
            Point pt;
            pt.x = pt1.x * m + pt2.x * (1-m);
            pt.y = pt1.y * m + pt2.y * (1-m);
            res.push_back(pt);
        }
        return res;
    }

    // 根据主方向分左右两边
    void getLeftRightClouds(Cloud* cloud, Cloud* left, Cloud* right)
    {
        int size = cloud->size();
        double min_sum = 540, direction = 0.0;
        for(double th = -PI / 9; th < PI / 9; th += PI / 180)
        {
            double covar = 0;
            double sum = 0;
            for(int i = 0; i < size; i++)
            {
                double x = cloud->at(i).x * cos(th) + cloud->at(i).y * sin(th);
                double y = -cloud->at(i).x * sin(th) + cloud->at(i).y * cos(th);
                if(abs(x) < min_distance) sum ++;
            }
            // for(int i = 0; i < size; i++)
            //     covar += (yt[i] - avg) * (yt[i] - avg) / size;
            //ROS_INFO("th: %f, sum: %f", th, sum);
            if(sum < min_sum)
            {
                min_sum = sum;
                direction = th;
            }
            else if(sum == min_sum && abs(th) < abs(direction))
                direction = th;
            
            
        }
        //ROS_INFO("direction: %f", direction);
        for(int i = 0; i < size; i++)
        {
            double x = cloud->at(i).x * cos(direction) + cloud->at(i).y * sin(direction);
            double y = -cloud->at(i).x * sin(direction) + cloud->at(i).y * cos(direction);
            // y坐标阈值, 20米
            if(abs(y) > 20.0)
                continue;
            // x坐标阈值 左侧是-wall_distance到-2,右侧是2到wall_distance, wall_distance参数可以改
            if(x <= -min_distance && x >= -wall_distance)
                left->push_back(cloud->at(i));
            else if(x > min_distance && x <= wall_distance)
                right->push_back(cloud->at(i));
        }
    }






    void run()
    {
        // read log file
        ifstream infile(filename.c_str(), ios::in);
        if(!infile)
        {
            ROS_INFO("filename error");
            return;
        }

        
        string textline;
        while(getline(infile, textline, '\n'))
        {
            lines.push_back(textline);
        }

        int num = lines.size();
        int pos = 0;
        while(pos < num)
        {
            if(IsScanBegin(pos) == false)
            {
                pos ++;
                continue;
            }


            string line = lines[pos];
            int m, s, ms;
            m = atoi(line.substr(0,1).c_str()), s = atoi(line.substr(2,2).c_str()), ms = atoi(line.substr(5,3).c_str());
            double time = m * 60 + s + ms * 0.001;
            Cloud *scan = new Cloud();
            vector<int> scan_data;
            for(int i = 0; i < 6; i++)
            {
                pos ++;
                line = lines[pos];
                vector<string> data = stringSplit(line, ' ');
                for(int i = 1; i < data.size(); i++)
                {
                    int d = atoi(data[i].c_str());
                    scan_data.push_back(d);
                }
            }
            if(scan_data.size() != 542)
                continue;

            ROS_INFO("time stamp: %lf, points: %d", time, scan_data.size());

            for(int i = 0; i < scan_data.size(); i++)
            {
                int d = scan_data[i];
                Point pt;
                pt.x = round(d * cos((-45+i*0.5) * PI / 180)) * 0.001;
                pt.y = round(d * sin((-45+i*0.5) * PI / 180)) * 0.001;
                pt.z = 0;

                // 距离过近的点不要
                if(d < min_distance * 1000)
                    continue;
                scan->push_back(pt);
                
            }
            clouds.push_back(scan);
        }

        ROS_INFO("Read Log File Done");

        ros::Rate rate(5);
        int count = 0;
        while(ros::ok() && count < clouds.size())
        {
            double time_begin = ros::Time::now().toSec();

            Cloud *scanLeft, *scanRight, *scan;
            scanLeft = new Cloud();
            scanRight = new Cloud();
            scan = clouds[count];
            //ROS_INFO("scan size: %d", scan->size());
            getLeftRightClouds(scan, scanLeft, scanRight);
            //ROS_INFO("scanLeft size: %d, scanRight size: %d", scanLeft->size(), scanRight->size());
            double time_step1 = ros::Time::now().toSec();
            //ROS_INFO("PCA time cost: %lf", time_step1 - time_begin);

            Cloud *lineLeft, *lineRight;
            lineLeft= new Cloud();
            lineRight= new Cloud();

            // 对左侧的任意两个点，计算出插值点，插值点的距离与插值点的距离差不能超过insert_dist_threshold, 见getInsertPoints函数
            for(int i = 0; i < scanLeft->size() - 1; i++)
            {
                Point pt1 = (*scanLeft)[i];
                for(int j = i + 1; j < scanLeft->size(); j++)
                {
                    Point pt2 = (*scanLeft)[j];
                    double dist = sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y));
                    if(dist < insert_dist_threshold)
                        continue;
                    vector<Point> insertPts = getInsertPoints(pt1, pt2, dist);
                    lineLeft->insert(lineLeft->end(), insertPts.begin(), insertPts.end());
                }
            }

            // 对右侧的任意两个点，计算出插值点，插值点的距离与插值点的距离差不能超过insert_dist_threshold, 见getInsertPoints函数
            for(int i = 0; i < scanRight->size() - 1; i++)
            {
                Point pt1 = (*scanRight)[i];
                for(int j = i + 1; j < scanRight->size(); j++)
                {
                    Point pt2 = (*scanRight)[j];
                    double dist = sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y));
                    if(dist < insert_dist_threshold)
                        continue;
                    vector<Point> insertPts = getInsertPoints(pt1, pt2, dist);
                    lineRight->insert(lineRight->end(), insertPts.begin(), insertPts.end());
                }
            }

            double time_step2 = ros::Time::now().toSec();
            //ROS_INFO("Insert time cost: %lf", time_step2 - time_step1);
            // ###########################################################

            // 对左右两侧的点云进行滤波, 去除大部分冗余的插值点
            vector<pair<double, double>> obsGridLeft, obsGridRight;
            for(double i = -20.0; i <= 20.0; i+= filter_gap)
            {
                obsGridLeft.push_back(make_pair(i, -100.0));
                obsGridRight.push_back(make_pair(i, 100.0));
            }

            for(auto p:*lineLeft)
            {
                int index = round((p.y + 20.0) / filter_gap);
                if(obsGridLeft[index].second < p.x)
                    obsGridLeft[index].second = p.x;
            }
            for(auto p:*lineRight)
            {
                int index = round((p.y + 20.0) / filter_gap);
                if(obsGridRight[index].second > p.x)
                    obsGridRight[index].second = p.x;
            }
            for(auto p:*scanLeft)
			{
			    int index = round((p.y + 20.0) / filter_gap);
			    if(obsGridLeft[index].second < p.x)
			        obsGridLeft[index].second = p.x;
			}
			for(auto p:*scanRight)
			{
			    int index = round((p.y + 20.0) / filter_gap);
			    if(obsGridRight[index].second > p.x)
			        obsGridRight[index].second = p.x;
			}

            double time_step3 = ros::Time::now().toSec();
            //ROS_INFO("filter time cost: %lf", time_step3 - time_step2);

            Cloud *obsLeft, *obsRight;
            obsLeft= new Cloud();
            obsRight= new Cloud();
#if 1
			for(auto p:obsGridLeft)
			{
			    if(abs(p.second) < 100)
			        obsLeft->push_back(Point(p.second, p.first, 0.0));
			}
			for(auto p:obsGridRight)
			{
			    if(abs(p.second) < 100)
			        obsRight->push_back(Point(p.second, p.first, 0.0));
			}
#else
			for(auto p:obsGridLeft)
			{
			    if(abs(p.second) < 100)
				{
					bool IsInsert = true;
					Point pt1(p.second, p.first,0);
					for(auto pt2:*scanLeft)
					{
						if(sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y)) < insert_gap)
						{	
							IsInsert = false;
							break;
						}
					}
					if(IsInsert)
						obsLeft->push_back(Point(p.second, p.first, 0.0));
				}
			        
			}
			for(auto p:obsGridRight)
			{
			    if(abs(p.second) < 100)
			    {
					bool IsInsert = true;
					Point pt1(p.second, p.first,0);
					for(auto pt2:*scanRight)
					{
						if(sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y)) < insert_gap)
						{
							IsInsert = false;
							break;
						}
					}
					if(IsInsert)
						obsRight->push_back(Point(p.second, p.first, 0.0));
				}
			}
#endif
            double time_finish = ros::Time::now().toSec();

            //ROS_INFO("time cost: %lf", time_finish - time_begin);
            count ++;
            // ###########################################################               

            //pcl可视化, 忽略
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_scan, cloud_scan_left, cloud_scan_right, cloud_obs_left, cloud_obs_right;
            cloud_scan.reset(new pcl::PointCloud<pcl::PointXYZ>);
            cloud_scan_left.reset(new pcl::PointCloud<pcl::PointXYZ>);
            cloud_scan_right.reset(new pcl::PointCloud<pcl::PointXYZ>);
            cloud_obs_left.reset(new pcl::PointCloud<pcl::PointXYZ>);
            cloud_obs_right.reset(new pcl::PointCloud<pcl::PointXYZ>);
            for(auto p:*obsLeft)
                cloud_obs_left->push_back(pcl::PointXYZ(p.x, p.y, 0.0));
            for(auto p:*obsRight)
                cloud_obs_right->push_back(pcl::PointXYZ(p.x, p.y, 0.0));
            for(auto p:*scan)
                cloud_scan->push_back(pcl::PointXYZ(p.x, p.y, 0.0));
            for(auto p:*scanLeft)
                cloud_scan_left->push_back(pcl::PointXYZ(p.x, p.y, 0.0));
            for(auto p:*scanRight)
                cloud_scan_right->push_back(pcl::PointXYZ(p.x, p.y, 0.0));
            
            
            Cloud().swap(*scan);
            Cloud().swap(*scanLeft);
            Cloud().swap(*scanRight);
            Cloud().swap(*lineLeft);
            Cloud().swap(*lineRight);
            Cloud().swap(*obsLeft);
            Cloud().swap(*obsRight);

            //delete scan, scanLeft, scanRight, lineLeft, lineRight, obsLeft, obsRight;
            
            sensor_msgs::PointCloud2 laserCloudTemp;
            pcl::toROSMsg(*cloud_scan, laserCloudTemp);
            laserCloudTemp.header.stamp = ros::Time::now();
            laserCloudTemp.header.frame_id = "scan";
            pubScanCloud.publish(laserCloudTemp);

            sensor_msgs::PointCloud2 laserCloudTempL;
            pcl::toROSMsg(*cloud_obs_left, laserCloudTempL);
            laserCloudTempL.header.stamp = ros::Time::now();
            laserCloudTempL.header.frame_id = "scan";
            pubLeftObs.publish(laserCloudTempL);

            sensor_msgs::PointCloud2 laserCloudTempR;
            pcl::toROSMsg(*cloud_obs_right, laserCloudTempR);
            laserCloudTempR.header.stamp = ros::Time::now();
            laserCloudTempR.header.frame_id = "scan";
            pubRightObs.publish(laserCloudTempR);

            sensor_msgs::PointCloud2 laserCloudTempLS;
            pcl::toROSMsg(*cloud_scan_left, laserCloudTempLS);
            laserCloudTempLS.header.stamp = ros::Time::now();
            laserCloudTempLS.header.frame_id = "scan";
            pubLeftLine.publish(laserCloudTempLS);

            sensor_msgs::PointCloud2 laserCloudTempRS;
            pcl::toROSMsg(*cloud_scan_right, laserCloudTempRS);
            laserCloudTempRS.header.stamp = ros::Time::now();
            laserCloudTempRS.header.frame_id = "scan";
            pubRightLine.publish(laserCloudTempRS);




            rate.sleep();
        }
    }
};

int main(int argc, char** argv){

    ros::init(argc, argv, "line extraction");
    
    LineExtraction LE;

    ROS_INFO("\033[1;32m---->\033[0m Line Extraction Started.");

    LE.run();

    ROS_INFO("Line Extraction run over");


    return 0;
}