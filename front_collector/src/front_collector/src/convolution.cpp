#include <iostream>
#include <math.h>
#include <numeric>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include <memory>
#include <cmath>
#include <thread>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>

#include "convolution.h"
#include "config.h"
#include "comm.h"
#include "Base.h"
#include <android/log.h>
#include "KKLog.h"

//#include <ros/ros.h>

//#include <sensor_msgs/PointCloud2.h>

//#include <pcl/point_cloud.h>
//#include <pcl/point_types.h>
//#include <pcl_ros/point_cloud.h>
//#include <pcl_conversions/pcl_conversions.h>
//#include <pcl/filters/filter.h>
//#include <pcl/filters/passthrough.h>
//#include <pcl/common/common.h>

using namespace std;

#define PI 3.1415926
extern int g_bBackMode;
class Point
{
    public:
    float x;
    float y;
    float z;

    Point()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Point(float _x, float _y, float _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }
};

typedef vector<Point> Cloud;

class LineExtraction
{
	private:


	//vector<Cloud*> clouds;
   // vector<Cloud*> cloudsLeft;
   // vector<Cloud*> cloudsRight;
    
	//double distance_threshold = 0.3;
	double wall_distance = 5.0;
	double min_distance = 1.5;
	double insert_dist_threshold = 7.0;
	double insert_gap = 0.5;
	double filter_gap = 0.5;
	//int max_iterations = 1000;

	public:
	vector<Point> getInsertPoints(Point pt1, Point pt2)
    {
        vector<Point> res;
        double dist = sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y));
        // 两个点的距离小于插入阈值，不插入
        if(dist < insert_dist_threshold)
        {
			return res;
        }
         
        // 插值的间隔为gap = insert_gap / dist, 其中insert_gap参数可以调整
        double gap = insert_gap / dist;
        for(double m = 0; m < 1; m += gap)
        {
            Point pt;
            pt.x = pt1.x * m + pt2.x * (1-m);
            pt.y = pt1.y * m + pt2.y * (1-m);
            res.push_back(pt);
        }
        return res;
    }

	// 根据主方向分左右两边
    void getLeftRightClouds(Cloud* cloud, Cloud* left, Cloud* right)
    {
        int size = cloud->size();
        double min_sum = 540, direction = 0.0;
        for(double th = -PI / 9; th < PI / 9; th += PI / 180)
        {
            //double covar = 0;
            double sum = 0;
            for(int i = 0; i < size; i++)
            {
                double x = cloud->at(i).x * cos(th) + cloud->at(i).y * sin(th);
               // double y = -cloud->at(i).x * sin(th) + cloud->at(i).y * cos(th);
                if(abs(x) < min_distance) sum ++;
            }
            // for(int i = 0; i < size; i++)
            //     covar += (yt[i] - avg) * (yt[i] - avg) / size;
            //ROS_INFO("th: %f, sum: %f", th, sum);
            if(sum < min_sum)
            {
                min_sum = sum;
                direction = th;
            }
            
        }
        //ROS_INFO("direction: %f", direction);
        for(int i = 0; i < size; i++)
        {
            double x = cloud->at(i).x * cos(direction) + cloud->at(i).y * sin(direction);
            double y = -cloud->at(i).x * sin(direction) + cloud->at(i).y * cos(direction);
            // y坐标阈值, 20米
            if(abs(y) > 20.0)
                continue;
            // x坐标阈值 左侧是-wall_distance到-2,右侧是2到wall_distance, wall_distance参数可以改
            if(x <= -min_distance && x >= -wall_distance)
                left->push_back(cloud->at(i));
            else if(x > min_distance && x <= wall_distance)
                right->push_back(cloud->at(i));
        }
    }

	LineModel Roadway_Completion(uint16* input)
	{
		//vector<Cloud*> cloudsLeft;
    	//vector<Cloud*> cloudsRight;
		Cloud *scan, *scanLeft, *scanRight;
		scan = new Cloud();
		scanLeft = new Cloud();
		scanRight = new Cloud();
	
		vector<int> scan_data;
		   
		for(int j=0;j<code_num;j++)
		{

			if(input[j] == 0xFFFF) 
			{
				scan_data.push_back(input[j]);
				continue;
			}
			else if(input[j] == 0) 
			{
				scan_data.push_back(input[j]);
				continue;
			}
			else 
			{
				scan_data.push_back(input[j]);
			}
		}
	 
		  // 把单帧点云分成左侧和右侧
		for(unsigned long i = 0; i < scan_data.size(); i++)
		{
			int d = scan_data[i];
			
			Point pt;
			
			pt.x = round(d * cos((-45+i*0.5) * PI / 180)) * 0.001;
			pt.y = round(d * sin((-45+i*0.5) * PI / 180)) * 0.001;
			pt.z = 0;
			
			// 距离过近的点不要
			if(d < min_distance * 1000)
				continue;
			scan->push_back(pt);
		}

		
		getLeftRightClouds(scan, scanLeft, scanRight);
	
		LineModel point;
		if((scanLeft->size()>= 20)&&(scanRight->size()>= 20))
		{
			
			Cloud *lineLeft, *lineRight;
			lineLeft= new Cloud();
			lineRight= new Cloud();

			// 对左侧的任意两个点，计算出插值点，插值点的距离与插值点的距离差不能超过insert_dist_threshold, 见getInsertPoints函数
			for(unsigned long i = 0; i < scanLeft->size() - 1; i++)
			{
			    Point pt1 = (*scanLeft)[i];
			    for(unsigned long j = i + 1; j < scanLeft->size(); j++)
			    {
			        Point pt2 = (*scanLeft)[j];
			        vector<Point> insertPts = getInsertPoints(pt1, pt2);
			        if(insertPts.empty())
			            continue;
			        lineLeft->insert(lineLeft->end(), insertPts.begin(), insertPts.end());
			    }
			}

			// 对右侧的任意两个点，计算出插值点，插值点的距离与插值点的距离差不能超过insert_dist_threshold, 见getInsertPoints函数
			for(unsigned long i = 0; i < scanRight->size() - 1; i++)
			{
			    Point pt1 = (*scanRight)[i];
			    for(unsigned long j = i + 1; j < scanRight->size(); j++)
			    {
			        Point pt2 = (*scanRight)[j];
			        vector<Point> insertPts = getInsertPoints(pt1, pt2);
			        if(insertPts.empty())
			            continue;
			        lineRight->insert(lineRight->end(), insertPts.begin(), insertPts.end());
			    }

			  
			}
			// ###########################################################

			// 对左右两侧的点云进行滤波, 去除大部分冗余的插值点
			vector<pair<double, double>> obsGridLeft, obsGridRight;
			for(double i = -20.0; i <= 20.0; i+= filter_gap)
			{
			    obsGridLeft.push_back(make_pair(i, -100.0));
			    obsGridRight.push_back(make_pair(i, 100.0));
			}

			for(auto p:*lineLeft)
			{
			    int index = round((p.y + 20.0) / filter_gap);
			    if(obsGridLeft[index].second < p.x)
			        obsGridLeft[index].second = p.x;
			}
			for(auto p:*lineRight)
			{
			    int index = round((p.y + 20.0) / filter_gap);
			    if(obsGridRight[index].second > p.x)
			        obsGridRight[index].second = p.x;
			}

			for(auto p:*scanLeft)
			{
			    int index = round((p.y + 20.0) / filter_gap);
			    if(obsGridLeft[index].second < p.x)
			        obsGridLeft[index].second = p.x;
			}
			for(auto p:*scanRight)
			{
			    int index = round((p.y + 20.0) / filter_gap);
			    if(obsGridRight[index].second > p.x)
			        obsGridRight[index].second = p.x;
			}
			
			Cloud *obsLeft, *obsRight;
			obsLeft= new Cloud();
			obsRight= new Cloud();
#if 1
			for(auto p:obsGridLeft)
			{
			    if(abs(p.second) < 100)
			        obsLeft->push_back(Point(p.second, p.first, 0.0));
			}
			for(auto p:obsGridRight)
			{
			    if(abs(p.second) < 100)
			        obsRight->push_back(Point(p.second, p.first, 0.0));
			}
#else
			for(auto p:obsGridLeft)
			{
			    if(abs(p.second) < 100)
				{
					bool IsInsert = true;
					Point pt1(p.second, p.first,0);
					for(auto pt2:*scanLeft)
					{
						if(sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y)) < insert_gap)
						{	
							IsInsert = false;
							break;
						}
					}
					if(IsInsert)
						obsLeft->push_back(Point(p.second, p.first, 0.0));
				}
			        
			}
			for(auto p:obsGridRight)
			{
			    if(abs(p.second) < 100)
			    {
					bool IsInsert = true;
					Point pt1(p.second, p.first,0);
					for(auto pt2:*scanRight)
					{
						if(sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y)) < insert_gap)
						{
							IsInsert = false;
							break;
						}
					}
					if(IsInsert)
						obsRight->push_back(Point(p.second, p.first, 0.0));
				}
			}
#endif
		//	LOGMAP("obsRight = %lu obsLeft = %lu ",obsLeft->size(),obsRight->size());

	
			point.left_point = obsLeft->size();
			for(unsigned long i= 0;i<obsLeft->size();i++)
			{

				point.line_left_x[i] = (obsLeft->at(i).x)*1000/x_k;
				point.line_left_y[i] = (obsLeft->at(i).y)*1000/x_k;

			}
			point.right_point = obsRight->size();
			for(unsigned long i= 0;i<obsRight->size();i++)
			{

				point.line_right_x[i] = (obsRight->at(i).x)*1000/x_k;
				point.line_right_y[i] = (obsRight->at(i).y)*1000/x_k;

			}

#if 1 
			unsigned char * Ocdata = (unsigned char*)malloc(sizeof(unsigned char)*(4096));
			int temp = 0;
			memset(Ocdata, 0, sizeof(unsigned char)*4096);
			Ocdata[temp++]= 0x99;
			Ocdata[temp++]= 0;
			Ocdata[temp++]= 0;
			for(unsigned long n= 0;n<obsLeft->size();n++)
			{
				Ocdata[temp++]=*((unsigned char*)(&(obsLeft->at(n).x))+3);
				Ocdata[temp++]=*((unsigned char*)(&(obsLeft->at(n).x))+2);
				Ocdata[temp++]=*((unsigned char*)(&(obsLeft->at(n).x))+1);
				Ocdata[temp++]=*((unsigned char*)(&(obsLeft->at(n).x)));
				Ocdata[temp++]=*((unsigned char*)(&(obsLeft->at(n).y))+3);
				Ocdata[temp++]=*((unsigned char*)(&(obsLeft->at(n).y))+2);
				Ocdata[temp++]=*((unsigned char*)(&(obsLeft->at(n).y))+1);
				Ocdata[temp++]=*((unsigned char*)(&(obsLeft->at(n).y)));
				
				//LOGMAP("obsLeft[%lu] = %f | %f ",n,obsLeft->at(n).x,obsLeft->at(n).y);
			
			}
			for(unsigned long n= 0;n<obsRight->size();n++)
			{
				Ocdata[temp++]=*((unsigned char*)(&(obsRight->at(n).x))+3);
				Ocdata[temp++]=*((unsigned char*)(&(obsRight->at(n).x))+2);
				Ocdata[temp++]=*((unsigned char*)(&(obsRight->at(n).x))+1);
				Ocdata[temp++]=*((unsigned char*)(&(obsRight->at(n).x)));
				Ocdata[temp++]=*((unsigned char*)(&(obsRight->at(n).y))+3);
				Ocdata[temp++]=*((unsigned char*)(&(obsRight->at(n).y))+2);
				Ocdata[temp++]=*((unsigned char*)(&(obsRight->at(n).y))+1);
				Ocdata[temp++]=*((unsigned char*)(&(obsRight->at(n).y)));
				
				//LOGMAP("obsLeft[%lu] = %f | %f ",n,obsLeft->at(n).x,obsLeft->at(n).y);
			
			}
			Ocdata[1] = temp>>8;
			Ocdata[2] = temp;

#if 0 
			for(int i = 0;i<temp;i++)
			{
				LOSEND("Ocdata[%d] = %02x",i,Ocdata[i]);

			}
#endif
			SendDataToOC1ByUDP(Ocdata, temp, 4006);
			free(Ocdata);
#endif
			Cloud().swap(*scan);
			Cloud().swap(*scanLeft);
			Cloud().swap(*scanRight);
			Cloud().swap(*lineLeft);
			Cloud().swap(*lineRight);
			Cloud().swap(*obsLeft);
			Cloud().swap(*obsRight);


			point.erro_flag = FALSE;
	
			return point;
		
		
		}
		else
		{
			Cloud().swap(*scan);
			Cloud().swap(*scanLeft);
			Cloud().swap(*scanRight);
			
			point.erro_flag = TRUE;
			
			return point;

		}
		
	}


	

};




LineModel Add_empty_point(uint16* input)
{
  	LineModel point;
    LineExtraction LE;
    point = LE.Roadway_Completion(input);

    return point;
}












