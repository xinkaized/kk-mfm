#include <unistd.h>
#include <pwd.h>
#include <ros/ros.h>

#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/Odometry.h>
#include <opencv2/opencv.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/range_image/range_image.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/local_maximum.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/common/common.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/segmentation/supervoxel_clustering.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/features/boundary.h>
#include <pcl/common/pca.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
 
#include <vector>
#include <cmath>
#include <algorithm>
#include <queue>
#include <deque>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cfloat>
#include <iterator>
#include <sstream>
#include <iostream>
#include <string>
#include <limits>
#include <iomanip>
#include <array>
#include <thread>
#include <mutex>
#include <boost/thread/thread.hpp>

#define PI 3.14159265

using namespace std;

typedef pcl::PointXYZI  PointType;
struct PointXYZIRPYT
{
    PCL_ADD_POINT4D
    PCL_ADD_INTENSITY;
    float roll;
    float pitch;
    float yaw;
    double time;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
} EIGEN_ALIGN16;

POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIRPYT,
                                   (float, x, x) (float, y, y)
                                   (float, z, z) (float, intensity, intensity)
                                   (float, roll, roll) (float, pitch, pitch) (float, yaw, yaw)
                                   (double, time, time)
)

typedef PointXYZIRPYT  PointTypePose;


extern const string imuTopic = "/imu/data";

// Save pcd
string fileDirectory;

extern const float scanPeriod = 0.1;
extern const int imuQueLength = 200;
extern const int N_SCAN = 16;
extern const int Horizon_SCAN = 1800;
extern const float ang_res_x = 0.2;
extern const float ang_res_y = 2.0;
extern const float ang_bottom = 15.0+0.1;

string UserName;
string foldername;


void getUserName()
{
    uid_t userid;
    struct passwd *pwd;
    userid = getuid();
    pwd = getpwuid(userid);
    UserName = string(pwd->pw_name);
    //UserName = string("nvidia");

}

class BoundingBox
{
    private:
    PointType pt1, pt2;
    public:
    BoundingBox(PointType _pt1, PointType _pt2)
    {
        pt1 = _pt1;
        pt2 = _pt2;
    };
    BoundingBox(double x1, double y1, double z1, double x2, double y2, double z2)
    {
        pt1.x = x1, pt1.y = y1, pt1.z = z1;
        pt2.x = x2, pt2.y = y2, pt2.z = z2;
    };
    
    bool IsPointInBBox(PointType pt)
    {
        Eigen::Vector3d diff1(pt.x - pt1.x, pt.y - pt1.y, pt.z - pt1.z);
        Eigen::Vector3d diff2(pt.x - pt2.x, pt.y - pt2.y, pt.z - pt2.z);
        if(diff1[0] > 0 && diff1[1] > 0 && diff1[2] > 0 && diff2[0] < 0 && diff2[1] < 0 && diff2[2] < 0)
            return true;
        else
            return false;
    };

    bool IsPointNearBBox(PointType pt, double dist)
    {
        Eigen::Vector3d diff1(pt.x - pt1.x, pt.y - pt1.y, pt.z - pt1.z);
        Eigen::Vector3d diff2(pt.x - pt2.x, pt.y - pt2.y, pt.z - pt2.z);
        if(diff1[0] > -dist && diff1[1] > -dist && diff1[2] > -dist && diff2[0] < dist && diff2[1] < dist && diff2[2] < dist)
            return true;
        else
            return false;
    };
};

class MyVoxel
{
    public:
    pcl::PointCloud<PointType>::Ptr cloud;
    double minz, maxz, height, threshold;

    MyVoxel(double thr)
    {
        minz = 17.0;
        maxz = -1.0;
        height = -1.0;
        threshold = thr;
        cloud.reset(new pcl::PointCloud<PointType>());
    };
    
    bool insertPoint(PointType pt)
    {
        cloud->push_back(PointType(pt));
        minz = minz < pt.z ? minz : pt.z;
        maxz = maxz > pt.z ? maxz : pt.z;
        height = maxz - minz;
    };

    pcl::PointCloud<PointType>::Ptr getLowestPoints()
    {
        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());
        for(auto p:cloud->points)
            if(abs(p.z - minz) < threshold)
            {
                p.intensity = (p.z - minz) / threshold * 100;
                cloudOut->push_back(PointType(p));
            }
        return cloudOut;
    };

    pcl::PointCloud<PointType>::Ptr getPointsBelowAbsoluteZ(double zz)
    {
        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());
        if(height > threshold)
        {
            for(auto p:cloud->points)
                if(abs(p.z - minz) >= 0.01 && p.z <= zz)
                    cloudOut->push_back(PointType(p));
        }
        return cloudOut;
    };

    pcl::PointCloud<PointType>::Ptr getPointsBelowRelevantZ(double zz)
    {
        pcl::PointCloud<PointType>::Ptr cloudOut;
        cloudOut.reset(new pcl::PointCloud<PointType>());
        if(height > threshold)
        {
            for(auto p:cloud->points)
                if(p.z - minz >= 0.01 && p.z - minz <= zz)
                    cloudOut->push_back(PointType(p));
        }
        return cloudOut;
    };
};